\documentclass[aspectratio=169, compress]{beamer}

% This file is a solution template for:

% - Giving a talk on some subject.
% - The talk is between 15min and 45min long.
% - Style is ornate.



% Copyright 2004 by Till Tantau <tantau@users.sourceforge.net>.
%
% In principle, this file can be redistributed and/or modified under
% the terms of the GNU Public License, version 2.
%
% However, this file is supposed to be a template to be modified
% for your own needs. For this reason, if you use this file as a
% template and not specifically distribute it as part of a another
% package/program, I grant the extra permission to freely copy and
% modify this file as you see fit and even to delete this copyright
% notice. 


\mode<presentation>
{
  \usetheme{Warsaw}
  % or ...

  \setbeamercovered{transparent}
  % or whatever (possibly just delete it)
}

\usepackage[english]{babel}
% or whatever

\usepackage[latin1]{inputenc}
% or whatever

\usepackage{times}
\usepackage[T1]{fontenc}
% Or whatever. Note that the encoding and the font should match. If T1
% does not look nice, try deleting the line with the fontenc.

\usepackage{standalone}
\standaloneconfig{mode=buildnew}

\usepackage{pgfplots}
\pgfplotsset{compat=1.18}
\usetikzlibrary{pgfplots.statistics}

\usepackage{siunitx}
\usepackage{booktabs}
\usepackage{spreadtab}
\usepackage{multirow}
\usepackage{graphicx}

\usepackage{biblatex}

% Add boxplot style to ignore outliers
\makeatletter
\pgfplotsset{
    boxplot/hide outliers/.code={
        \def\pgfplotsplothandlerboxplot@outlier{}%
    }
}
\makeatother

\addbibresource{../report/sources.bib}

\usepackage{graphicx}

\title[RTX Tubes] % (optional, use only with long paper titles)
{RTX Accelerated Drawing of Tube Splines}

%\subtitle
%{Presentation Subtitle} % (optional)

%\author[Author, Another] % (optional, use only with lots of authors)
%{F.~Author\inst{1} \and S.~Another\inst{2}}
% - Use the \inst{?} command only if the authors have different
%   affiliation.
\author{M.~Henze}

\institute[Institute of software and multimedia technology] % (optional, but mostly needed)
{
  Department of Computer Science\\
  University of Technology Dresden
}
% - Use the \inst command only if there are several affiliations.
% - Keep it simple, no one is interested in your street address.

%\subject{Talks}
% This is only inserted into the PDF information catalog. Can be left
% out. 



% If you have a file called "university-logo-filename.xxx", where xxx
% is a graphic format that can be processed by latex or pdflatex,
% resp., then you can add a logo as follows:

% \pgfdeclareimage[height=0.5cm]{university-logo}{university-logo-filename}
% \logo{\pgfuseimage{university-logo}}



% Delete this, if you do not want the table of contents to pop up at
% the beginning of each subsection:
%\AtBeginSubsection[]
%{
%  \begin{frame}<beamer>{Outline}
%    \tableofcontents[currentsection,currentsubsection]
%  \end{frame}
%}


% If you wish to uncover everything in a step-wise fashion, uncomment
% the following command: 

%\beamerdefaultoverlayspecification{<+->}


\begin{document}

\begin{frame}
  \titlepage{}
\end{frame}

\begin{frame}{Outline}
\centering
  \tableofcontents[pausesections]
  % You might wish to add the option [pausesections]
\end{frame}


% Since this a solution template for a generic talk, very little can
% be said about how it should be structured. However, the talk length
% of between 15min and 45min and the theme suggest that you stick to
% the following rules:  

% - Exactly two or three sections (other than the summary).
% - At *most* three subsections per section.
% - Talk about 30s to 2min per frame. So there should be between about
%   15 and 30 frames, all told.

\section{Motivation}

\subsection{Task Formulation}

\begin{frame}{Task Description}
  \begin{itemize}
  \item
    Investigate image-order rendering of Hermite Spline Tubes\pause{}
  \item
    Use NVIDIA's RTX feature set for hardware raytracing\pause{}
  \item
    Implement raycasting algorithm for comparison in same API\pause{}
  \item
    Study performance characteristics of both approaches\pause{}
  \end{itemize}
\end{frame}


\subsection{Image Order Techniques}

\begin{frame}{Image Order Techniques}
\begin{columns}
  \begin{column}{.5\textwidth}
    \begin{itemize}[<+->]
      \item Classical approach called object order rendering
      \item Every object flows through rasterization pipeline
      \item Scientific visualization display several million objects
      \item ``Direct'' pixel shading more straightforward
    \end{itemize}
  \end{column}
  \begin{column}{.5\textwidth}
    \begin{itemize}[<+->]
      \item Given reference algorithm~\cite{russig2020gpu} implements an image order rendering of generalized spline tubes using raycasting
      \item Inspired by~\cite{han2019ray} which presents raytracing against cone stumps
      \item~\cite{kanzler2018voxel} employ voxel grid and piecewise linear approximation combined with raycasting
    \end{itemize}
  \end{column}
\end{columns}
\end{frame}


\section{Implementation}

\subsection{Raycasting}

\begin{frame}{Raycasting Pipeline}
  \begin{columns}
    \begin{column}{.35\textwidth}
      \begin{itemize}[<+->]
        \item Implementation following~\cite{russig2020gpu}
        \item Avoid explicit tesselation
        \item Memory conservation through GS data expansion
        \item Implicit surface formulation lends to per fragment silhouette \& shading
      \end{itemize}
    \end{column}
    \begin{column}{.35\textwidth}
      \begin{itemize}[<+->]
        \item Spline nodes as VBO
        \item Drawn as line primitive
        \item Vertex shader only passthrough
        \item Geometry shader emits Bounding Box
        \item Fragment shader tests intersection along bounding box surface
      \end{itemize}
    \end{column}
    \begin{column}{.3\textwidth}
      \centering
      \includegraphics<-7>[height=.8\textheight]{../images/RenderingPipeline}%
      \includegraphics<8>[width=\textwidth]{../images/spline_coord_sys}%
      \includegraphics<9>[width=\textwidth]{../images/ray_coord_sys}%
      \nocite{khronos:ogl_pipe}
    \end{column}
  \end{columns}
\end{frame}

\subsection{Raytracing}

\begin{frame}{Raytracing ``Pipeline'' in general}
  \begin{columns}
    \begin{column}{.5\textwidth}
      \begin{itemize}[<+->]
        \item Prepare scene traversal with acceleration structure
        \item Setup shader binding table
        \item Generate one ray per image coordinate
        \item Rays recursively traverse acceleration structure according to SBT
        \item Second pass blits image into framebuffer
      \end{itemize}
    \end{column}
    \begin{column}{.5\textwidth}
      \centering
      \includegraphics<1>[width=\textwidth]{../images/2020-blog-ray-tracing-in-vulkan-figure-3}
      \nocite{khronos:blog_rtx}%
      \includegraphics<2->[height=.7\textheight]{../images/2020-blog-raytracing-img-08_1}
      \nocite{khronos:blog_rtx_2}
    \end{column}
  \end{columns}
\end{frame}

\begin{frame}{Raytracing ``Pipeline'' for Splines}
  \begin{columns}
    \begin{column}{.5\textwidth}
      \begin{itemize}[<+->]
        \item Prepare scene as AABB acceleration structure
        \item ``Trivial'' SBT for primary rays against one material
        \item Custom intersection shader tests implicit surface formula
        \item Original spline data accessible through SSBO
        \item First intersection finishes ray traversal
      \end{itemize}
    \end{column}
    \begin{column}{.5\textwidth}
      \centering
      \includegraphics<1>[width=\textwidth]{../images/2020-blog-ray-tracing-in-vulkan-figure-3}
      \nocite{khronos:blog_rtx}%
      \includegraphics<2->[height=.7\textheight]{../images/2020-blog-raytracing-img-08_1}
      \nocite{khronos:blog_rtx_2}
    \end{column}
  \end{columns}
\end{frame}

\section{Evaluation}

\subsection{Frametimes}

\begin{frame}{Fibers}
  \begin{columns}
    \begin{column}{.5\textwidth}
      \includegraphics[width=\textwidth]{../images/fibers_close_fb}
    \end{column}
    \begin{column}{.5\textwidth}
      \includegraphics[width=\textwidth]{../images/fibers_far_fb}
    \end{column}
  \end{columns}
\end{frame}

\begin{frame}{Fibers Frametimes}
  \begin{columns}
    \begin{column}{.6\textwidth}
      \centering
      \includestandalone[width=\textwidth]{../figures/frametimeBoxplotFibers}
    \end{column}
    \begin{column}{.4\textwidth}
      \centering
      \includegraphics[keepaspectratio,width=\textwidth,height=.4\textheight]{../images/fibers_close_fb}
      \includegraphics[keepaspectratio,width=\textwidth,height=.4\textheight]{../images/fibers_far_fb}
    \end{column}
  \end{columns}
\end{frame}

\begin{frame}{Brain}
  \begin{columns}
    \begin{column}{.5\textwidth}
      \includegraphics[width=\textwidth]{../images/brain_close_fb}
    \end{column}
    \begin{column}{.5\textwidth}
      \includegraphics[width=\textwidth]{../images/brain_far_fb}
    \end{column}
  \end{columns}
\end{frame}

\begin{frame}{Brain Frametimes}
  \begin{columns}
    \begin{column}{.6\textwidth}
      \centering
      \includestandalone[width=\textwidth]{../figures/frametimeBoxplotBrain}
    \end{column}
    \begin{column}{.4\textwidth}
      \centering
      \includegraphics[keepaspectratio,width=\textwidth,height=.4\textheight]{../images/brain_close_fb}
      \includegraphics[keepaspectratio,width=\textwidth,height=.4\textheight]{../images/brain_far_fb}
    \end{column}
  \end{columns}
\end{frame}

\begin{frame}{HotRoom}
  \begin{columns}
    \begin{column}{.5\textwidth}
      \includegraphics[width=\textwidth]{../images/hotroom_close_fb}
    \end{column}
    \begin{column}{.5\textwidth}
      \includegraphics[width=\textwidth]{../images/hotroom_far_fb}
    \end{column}
  \end{columns}
\end{frame}

\begin{frame}{HotRoom Frametimes}
  \begin{columns}
    \begin{column}{.6\textwidth}
      \centering
      \includestandalone[width=\textwidth]{../figures/frametimeBoxplotHotRoom}
    \end{column}
    \begin{column}{.4\textwidth}
      \centering
      \includegraphics[keepaspectratio,width=\textwidth,height=.4\textheight]{../images/hotroom_close_fb}
      \includegraphics[keepaspectratio,width=\textwidth,height=.4\textheight]{../images/hotroom_far_fb}
    \end{column}
  \end{columns}
\end{frame}

\begin{frame}{Huge}
  \begin{columns}
    \begin{column}{.5\textwidth}
      \includegraphics[width=\textwidth]{../images/huge_close_fb}
    \end{column}
    \begin{column}{.5\textwidth}
      \includegraphics[width=\textwidth]{../images/huge_far_fb}
    \end{column}
  \end{columns}
\end{frame}

\begin{frame}{Huge Frametimes}
  \begin{columns}
    \begin{column}{.6\textwidth}
      \centering
      \includestandalone[width=\textwidth]{../figures/frametimeBoxplotHuge}
    \end{column}
    \begin{column}{.4\textwidth}
      \centering
      \includegraphics[keepaspectratio,width=\textwidth,height=.4\textheight]{../images/huge_close_fb}
      \includegraphics[keepaspectratio,width=\textwidth,height=.4\textheight]{../images/huge_far_fb}
    \end{column}
  \end{columns}
\end{frame}

\begin{frame}{Massive}
  \begin{columns}
    \begin{column}{.5\textwidth}
      \includegraphics[width=\textwidth]{../images/massive_close_fb}
    \end{column}
    \begin{column}{.5\textwidth}
      \includegraphics[width=\textwidth]{../images/massive_far_fb}
    \end{column}
  \end{columns}
\end{frame}

\begin{frame}{Massive Frametimes}
  \begin{columns}
    \begin{column}{.6\textwidth}
      \centering
      \includestandalone[width=\textwidth]{../figures/frametimeBoxplotMassive}
    \end{column}
    \begin{column}{.4\textwidth}
      \centering
      \includegraphics[keepaspectratio,width=\textwidth,height=.4\textheight]{../images/massive_close_fb}
      \includegraphics[keepaspectratio,width=\textwidth,height=.4\textheight]{../images/massive_far_fb}
    \end{column}
  \end{columns}
\end{frame}

\begin{frame}{Frametime Composition}
\begin{columns}
  \begin{column}{.5\textwidth}
    \centering
    \sisetup{round-mode=places}
    \resizebox{\textwidth}{!}{% resizebox
    \begin{spreadtab}{{tabular}{r c r S[round-precision=2] S[round-precision=2] S[round-precision=2]}}
    \toprule
    Dataset @& Variant @& \#Splines @& {RC} @& {RT} @& {Speedup} @\\
    & & & {[\unit{\ms}]} @& {[\unit{\ms}]} @& {[1]} @\\
    \midrule
    \multirow{2}{*}{Fibers} @& close @& \multirow{2}{*}{7060} @& 2.3793 & 3.8282 & [-2,0]/[-1,0] \\
    @& far @& & 0.8235 & 2.5046 & [-2,0]/[-1,0] \\
    \midrule
    \multirow{2}{*}{Brain} @& close @& \multirow{2}{*}{228978} @& 4.8856 & 3.6345 & [-2,0]/[-1,0] \\
    @& far @& & 5.1772 & 04.648 & [-2,0]/[-1,0] \\
    \midrule
    \multirow{2}{*}{HotRoom} @& close @& \multirow{2}{*}{61896} @& 11.8953 & 7.6561 & [-2,0]/[-1,0] \\
    @& far @& & 5.7878 & 6.4105 & [-2,0]/[-1,0] \\
    \midrule
    \multirow{2}{*}{Huge} @& close @& \multirow{2}{*}{1574654} @& 27.7645 & 8.7165 & [-2,0]/[-1,0] \\
    @& far @& & 24.4131 & 4.4989 & [-2,0]/[-1,0] \\
    \midrule
    \multirow{2}{*}{Massive} @& close @& \multirow{2}{*}{3631046} @& 56.3658 & 6.7558 & [-2,0]/[-1,0] \\
    @& far @& & 54.6008 & 7.7127 & [-2,0]/[-1,0] \\
    \bottomrule
    \end{spreadtab}% resizebox
    }
  \end{column}
  \begin{column}{.5\textwidth}
    \centering
    \includestandalone[height=.85\textheight]{../figures/frametimeComposition}
  \end{column}
\end{columns}
\end{frame}

\subsection{Bottlenecks}

\begin{frame}{Fibers Overdraw}
  \begin{columns}
    \begin{column}{.5\textwidth}
      \includegraphics<1>[width=\textwidth]{../images/fibers_close_fb}%
      \includegraphics<2>[width=\textwidth]{../images/fibers_close_overdraw}
    \end{column}
    \begin{column}{.5\textwidth}
      \includegraphics<1>[width=\textwidth]{../images/fibers_far_fb}%
      \includegraphics<2>[width=\textwidth]{../images/fibers_far_overdraw}
    \end{column}
  \end{columns}
\end{frame}

\begin{frame}{Brain Overdraw}
  \begin{columns}
    \begin{column}{.5\textwidth}
      \includegraphics<1>[width=\textwidth]{../images/brain_close_fb}%
      \includegraphics<2>[width=\textwidth]{../images/brain_close_overdraw}
    \end{column}
    \begin{column}{.5\textwidth}
      \includegraphics<1>[width=\textwidth]{../images/brain_far_fb}%
      \includegraphics<2>[width=\textwidth]{../images/brain_far_overdraw}
    \end{column}
  \end{columns}
\end{frame}

\begin{frame}{HotRoom Overdraw}
  \begin{columns}
    \begin{column}{.5\textwidth}
      \includegraphics<1>[width=\textwidth]{../images/hotroom_close_fb}%
      \includegraphics<2>[width=\textwidth]{../images/hotroom_close_overdraw}
    \end{column}
    \begin{column}{.5\textwidth}
      \includegraphics<1>[width=\textwidth]{../images/hotroom_far_fb}%
      \includegraphics<2>[width=\textwidth]{../images/hotroom_far_overdraw}
    \end{column}
  \end{columns}
\end{frame}

\begin{frame}{Huge Overdraw}
  \begin{columns}
    \begin{column}{.5\textwidth}
      \includegraphics<1>[width=\textwidth]{../images/huge_close_fb}%
      \includegraphics<2>[width=\textwidth]{../images/huge_close_overdraw}
    \end{column}
    \begin{column}{.5\textwidth}
      \includegraphics<1>[width=\textwidth]{../images/huge_far_fb}%
      \includegraphics<2>[width=\textwidth]{../images/huge_far_overdraw}
    \end{column}
  \end{columns}
\end{frame}

\begin{frame}{Massive Overdraw}
  \begin{columns}
    \begin{column}{.5\textwidth}
      \includegraphics<1>[width=\textwidth]{../images/massive_close_fb}%
      \includegraphics<2>[width=\textwidth]{../images/massive_close_overdraw}
    \end{column}
    \begin{column}{.5\textwidth}
      \includegraphics<1>[width=\textwidth]{../images/massive_far_fb}%
      \includegraphics<2>[width=\textwidth]{../images/massive_far_overdraw}
    \end{column}
  \end{columns}
\end{frame}

\begin{frame}{RT AABBs}
  \includegraphics[width=.9\textwidth]{../images/fibers_rt_aabb}
\end{frame}


\section*{Summary}

\begin{frame}{Summary}

  % Keep the summary *very short*.
  \begin{itemize}
  \item
    RTX accelerated drawing of spline tubes scales very well on huge datasets.
  \item
    Raycasting approach has significantly lower overhead on small to medium datasets.
  \item
    Raycasting performance is mostly lost on overdraw.
  \end{itemize}
  
  % The following outlook is optional.
  \vskip0pt plus.5fill
  \begin{itemize}
  \item
    Outlook
    \begin{itemize}
    \item
      Improve intersection context switching through OBB ``hack''
    \item
      Possible curve hardware primitives in future GPU generations 
    \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}[allowframebreaks]{References}
\printbibliography{}
\end{frame}

\end{document}
\documentclass[a4paper, twocolumn]{scrartcl}

\usepackage{standalone}
\standaloneconfig{mode=buildnew}

\usepackage[english]{babel}
\usepackage{csquotes}
\usepackage{biblatex}
\usepackage[acronym, automake]{glossaries}
\usepackage{caption}
\usepackage{subcaption}

\usepackage{cleveref}

\usepackage{multirow}
\usepackage{booktabs}

\usepackage{pgfplots}
\pgfplotsset{compat=1.18}
\usetikzlibrary{pgfplots.statistics}

\usepackage{siunitx}

% Add boxplot style to ignore outliers
\makeatletter
\pgfplotsset{
    boxplot/hide outliers/.code={
        \def\pgfplotsplothandlerboxplotoutlier{}%
    }
}
\makeatother

\addbibresource{sources.bib}

\makeglossaries{}
\newacronym{as}{AS}{Acceleration Structure}
\newacronym{blas}{BLAS}{Bottom Level Acceleration Structure}
\newacronym{tlas}{TLAS}{Top Level Acceleration Structure}
\newacronym{obb}{OBB}{Oriented Bounding Box}
\newacronym{aabb}{AABB}{Axis Aligned Bounding Box}
\newacronym{sbt}{SBT}{Shader Binding Table}

\author{Mario Henze}
\title{RTX Accelerated Drawing of Tube Splines}
\date{}

\begin{document}

\maketitle{}

\tableofcontents{}

\section{Task definition} % (fold)
\label{sec:taskdefinition}

Hermite Spline Tubes are a well-suited visualization primitive for curve data
when per sample derivatives are available in addition to values. When
rendering them using a rasterizer pipeline, raycasting scales much better to
larger dataset sizes than surface tessellation, but some form of silhouette
geometry is still required, incurring a performance penalty. Also, very complex
datasets can incur significant overdraw, wasting fragment processing time in a
method that is already highly dependent on fillrate.

The goal of this project is to investigate image-order rendering of Hermite
Spline Tubes as an alternative approach. Specifically, the image-order renderer
shall leverage NVIDIA’s RTX feature set for hardware raytracing. For
comparison, an implementation of the rasterization based raycasting algorithm
should be developed in the same API used to access RTX (e.g. Vulkan).

% section taskdefinition (end)

\section{Image Order Rendering} % (fold)
\label{sec:image_order_rendering}

The traditional way to draw any scene into a framebuffer employs a so called
object order approach. This means that every logical grouping of vertices,
color, etc.\ is thought of as an object in a scene. Each of these objects then
flow through the rendering pipeline, where the vertices are first transformed
into a view dependent coordinate system and then rasterized. As there are
usually much fewer objects to be drawn in the scene than pixels to fill in the
framebuffer this object centered approach delivers outstanding performance.
Furthermore, the technical advancements in graphics accelerators over the past
few decades really pushed the boundaries of what is possibly with object order
rendering.

The computer graphics subfield of scientific visualization, however, challenges
this assumed inequality. Oftentimes, huge datasets, without extensive
preprocessing, need to be displayed. The usual scenes contain several million
objects or more. Direct shading of each framebuffer pixel now becomes more
straightforward, as its performance scales significantly better with dataset
complexity.

The given task of trajectory visualization lends itself to image order rendering
techniques, as it exhibits the aforementioned difficulties. With million
distinct splines or more per dataset the capabilities of object order rendering
are exhausted quite fast.

\textcite{kanzler2018voxel} present a voxel grid approach for
trajectory visualization. Their method prepares piecewise linear approximations
for each spline subsection contained in a cell. Each of these grid cells then
gets raycasted with the contained spline data.

\textcite{han2019ray} model each trajectory as a series of spherical
nodes connected by cylinders with varying radii. For both geometric shapes
implicit surfaces were defined. A bounding volume hierarchy needs to be built
and is used for the raytracing.

The given reference algorithm from \textcite{russig2020gpu} uses the work
of \textcite{han2019ray} as inspiration and presents a generalized implicit
surface definition for spline segments. The following chapters will explain it
in detail.

% section image_order_rendering (end)


\section{Raycasting} % (fold)
\label{sec:raycasting}

\begin{figure}
    \centering
    \includegraphics[width=\columnwidth]{../images/spline_coord_sys}
    \caption{An \gls{obb} is found by calculating a transformation \(M\). The
     splines minima and maxima each scale the unit cube accordingly. Image
     courtesy of~\cite{russig2020gpu}}%
    \label{fig:obb}
\end{figure}

\begin{figure}
    \centering
    \includegraphics[width=\columnwidth]{../images/ray_coord_sys}
    \caption{The splines intersection with the view ray is calculated in ray
     space. The closest hit on the x-axis guides the fragment processing. Image
     courtesy of~\cite{russig2020gpu}}%
    \label{fig:rayspace_int}
\end{figure}
      
The first raycasting implementation is based on the reference work published
by \textcite{russig2020gpu}. It was done with the Vulkan API and cgvu
Framework. Classic methods for trajectory visualization often employ
tessellation strategies to prepare the data for visualization on rasterizing
GPUs. The authors show that with increasing data size the memory bandwidth
quickly becomes the bottleneck of this approach.

They try to counter this with an implicit surface formulation for a volume,
defined by the trace of a sphere moving along a hermitean spline trajectory.
Memory is conserved by the fact that only the node points attribute data of
each spline segment are stored in VRAM. An geometry shader invocation then
closely envelopes the implicit volume with an \gls{obb} (see 
\cref{fig:obb}). Compared to the explicit tesselation of the spline surfaces
this cuts down the number of processed vertices significantly. Furthermore, the
position attribute of the spline is transformed into the view dependent ray
space to simplify the formulation of the implicit surface formulas. The fragment
shader determines the actual intersection with the tube segment and evaluates 
the visualized properties of the spline (see \cref{fig:rayspace_int}).

It is then shown that for sufficiently large datasets this raycasting approach
still manages to render with interactive frametimes, while the tessellation
approach fails to do so. \textcite{russig2020gpu} note that their method puts
significant strain on the fillrate of the GPU\@. Overdraw issues with certain
datasets were non negligible as well.

% section raycasting (end)

\section{NVIDIAs RTX Pipeline} % (fold)
\label{sec:nvidias_rtx_pipeline}

While NVidias hardware accelerated ray tracing API RTX isn't the first of its
kind, it achieved notably fast adoption into the leading rendering APIs like
Microsofts DirectX and Vulkan. The structure of its components, therefore, will
guide our understanding of how raytracing will be hardware accelerated. The
following paragraphs will present the general steps necessary to use RTX
through respective Vulkan extensions.

\subsection{Acceleration Structure} % (fold)
\label{sub:acceleration_structure}

\begin{figure}
    \centering
    \includegraphics[width=\columnwidth]{../images/2020-blog-ray-tracing-in-vulkan-figure-3}
    \caption{Hierarchy of \glspl{as}. Image from~\cite{khronos:blog_rtx}}%
    \label{fig:rt_as_hierarchy}
\end{figure}

Rendering any scene via RTX raytracing requires a two phase approach. For
efficient ray scene traversal the hardware uses an \gls{as}. This structure
needs to be built before any raytracing can happen. If there are animated parts
in the scene an update of the \gls{as} is required for each frame. Separating
static from dynamic parts into a flat hierarchy of \glspl{as} makes these
updates much cheaper. This is achieved by building a \gls{blas} for a ``class''
of objects. The \gls{tlas} then aggregates the scene, or parts of it, by storing
instanced informations referring to a \gls{blas} (see \cref{fig:rt_as_hierarchy}).
The \glspl{blas} can contain a spatial hierarchy of either triangles or
\glspl{aabb}, which envelop custom geometry.

% subsection acceleration_structure (end)


\subsection{Programmatic Ray Traversal} % (fold)
\label{sub:programmatic_ray_traversal}

The actual ray tracing then can be programmed through several kinds of
shaders. The ray gen shader will be called up first and dictates how many rays
in which directions will be traced.

Intersection testing is implemented in a fixed manner for triangles or can be
defined with an intersection shader for custom geometry. This shader is invoked
whenever a potential hit is discovered, which means the ray has intersected
the \gls{aabb}. Given the ray origin and direction, the shader can decide for
which numeric value of the ray parameter \(t\) an actual intersection with the
geometry has happened.

For each of these the any hit shader can be used to further filter and process
reported intersections. A common use case would be alpha testing against a
texture and discarding the intersection based on a texel
value (described in~\cite{subtil_2018}).

\begin{figure}
    \centering
    \includegraphics[width=\columnwidth]{../images/2020-blog-raytracing-img-08_1}
    %\nocite{khronos:blog_rtx_2}
    \caption{The control flow in the RTX pipeline. Image from~\cite{khronos:blog_rtx_2}}%
    \label{fig:rt_pipeline}
\end{figure}

The recursive ray traversal will be finished after the first hit was found or an
implementation defined recursion limit is reached (see \cref{fig:rt_pipeline}.)
Each ray with an confirmed intersection will result in an
invocation of the closest hit shader. The main lighting and material
computations will happen here. Further raytracing with secondary rays for
effects like reflection will be implemented in here as well. If no
intersections have been found the miss shader will be called. This shader is
suitable for rendering skyboxes.

The communication between all these shader stages is conceptually divided into
built in and user definable variables. The general parameters common to all
raytracing applications, like the ray parameter \(t\) or the rays position and
direction, are all built into the formal specification of the RTX extension.
GLSL for example exposes them via the usual \texttt{gl\_} prefix. Complex
techniques such as physically correct subsurface scattering of light usually
demand a versatile data exchange. Therefore, a custom variable or structure
definition can be annotated with a ``ray-payload'' or ``hit-attribute''
modifier. The ray payload is shared between the ray generation, closest hit,
and miss stages. It is commonly used to communicate values like the final
output color.

With an hit attribute the intersection-, any hit-, and closest hit stages can
transfer information about the encountered intersection. The built in triangle
intersection processor for example delivers the barycentric weights of the
triangle intersection point (as defined in~\cite{glsl_ext_rt}).

% subsection programmatic_ray_traversal (end)

\subsection{Type Dispatch} % (fold)
\label{sub:type_dispatch}

Realistic scenes often contain a multitude of different materials with distinct
physical properties. In order to model their behaviour it is only feasible to
create different sets of shaders for each material. The hardware implementation,
therefore, needs a way to choose the shader sets according to material. This
dispatching information is collected into a \gls{sbt}.

The \gls{sbt} is basically a long array with records of equal size. Within each
record several ``pointers'' are stored, which define a distinct material. As
each set of shaders model a material, this means each record stores a pointer
to the respective shader set. Additional shader parameters like binding
respective buffers can be stored aswell.

With these different combinations all laid out in a table, the hardware can then
dispatch with simple offset and stride calculations. Each ray- and geometry
type can be assigned an integer independently. With the \gls{sbt} record length
known as stride these integers naturally work as offsets. And with all offsets
applied recursively into the \gls{sbt} record (see~\cite{usher_2019}), all the
needed combinations of ray types with material types can be described for the
hardware.

% subsection type_dispatch (end)

\subsection{Raytracing ``Pipeline'' for Splines} % (fold)
\label{sub:raytracing_pipeline_for_splines}

The reference algorithm of raytracing against the implicit surface of the
splines was mapped onto the RTX pipeline as follows. The choice of acceleration
structure was straightforward. Only a hierarchy of \glspl{aabb} allows the use
of a custom intersection shader which in turn is necessary for the implicit
surface test.

As the visualized material properties of the splines are quite simple, the
\gls{sbt} becomes equally trivial. For this task only visibility testing is
needed. Therefore the only ray type is for these primary rays. The material
used for all splines evaluates the spline color and adds lambertian shading.
This results in a \gls{sbt} with one record, one shader set, and without any
offsets.

The traversal of the \gls{aabb} hierarchy in turn invokes the custom
intersection shader. This shader is virtually identical to the fragment shader
as described in \cref{sec:raycasting}. It tests for and reports the first
intersection of the implicit surface formulation. Any further traversal will be
aborted.

In contrast to the described use cases of each shader type
(see \cref{sub:programmatic_ray_traversal}) the actual color and shading 
calculations for the ray are computed in the intersection shader as well. As the
spline segments data is accessed via index into an SSBO, the color attribute is 
already present as variable from the type definition. Instead of evaluating the 
spline again at the found intersection in the closest hit shader the color will 
be communicated as hit attribute.

% subsection raytracing_pipeline_for_splines (end)

% section nvidias_rtx_pipeline (end)


\section{Performance evaluation} % (fold)
\label{sec:performance_evaluation}

All shown framebuffers were rendered and capture with Full HD resolution. The
frametimes were recorded over an interval of about \qty{10}{\s}.

\subsection{Fibers} % (fold)
\label{sub:fibers}

\begin{figure}
\begin{subfigure}{\columnwidth}
    \centering
    \includegraphics[width=\linewidth]{../images/fibers_close_fb}
    \caption{close}
\end{subfigure}
\begin{subfigure}{\columnwidth}
    \centering
    \includegraphics[width=\linewidth]{../images/fibers_far_fb}
    \caption{far}
\end{subfigure}
\caption{Benchmark perspectives of ``Fibers'' Dataset}%
\label{fig:view_fibers}
\end{figure}

\begin{figure}
\centering
\includestandalone[width=\columnwidth]{../figures/frametimeBoxplotFibers}
\caption{Frametimes for ``Fibers'' Dataset}%
\label{fig:comp_frametimes_fibers}
\end{figure}

The ``Fibers'' dataset (\cref{fig:view_fibers}) was mainly used for development.
With around seven thousand splines it has barely significance for
generalization. But the measured frametimes in \cref{fig:comp_frametimes_fibers}
highlight the initial overhead of the raytracing engine pretty well.

The close perspective with the raycasting approach really taxes the fragment
processing capabilities of the GPU\@. Here it achieves around \qty{2.5}{\ms} 
while the Raytracing needs close to \qty{4}{\ms}. The far perspective, as
the more common data visualization, really highlights this disparity, where
raycasting easily achieves frametimes below \qty{1}{\ms}, while raytracing needs
more than double the time with \qty{2.5}{\ms}. Nonetheless, both implementations
deliver perfect framerates for interactivity in any perspective.

% subsection fibers (end)


\subsection{Brain} % (fold)
\label{sub:brain}

\begin{figure}
\begin{subfigure}{\columnwidth}
    \centering
    \includegraphics[width=\linewidth]{../images/brain_close_fb}
    \caption{close}
\end{subfigure}
\begin{subfigure}{\columnwidth}
    \centering
    \includegraphics[width=\linewidth]{../images/brain_far_fb}
    \caption{far}
\end{subfigure}
\caption{Benchmark perspectives of ``Brain'' dataset}%
\label{fig:view_brain}
\end{figure}

\begin{figure}
\centering
\includestandalone[width=\columnwidth]{../figures/frametimeBoxplotBrain}
\caption{Frametimes for ``Brain'' Dataset}%
\label{fig:comp_frametimes_brain}
\end{figure}

The ``Brain'' dataset (\cref{fig:view_brain}) is another real world scan used
mainly for development. With 230 thousand splines it is still fairly small and
therefore also limited in generality. But here one can see the first indication
of a turning point in performance characteristics (see \cref{fig:comp_frametimes_brain}).

Raycasting in close perspective achieves a little under \qty{5}{\ms} while
Raytracing on the other hand beats it with around \qty{3.7}{\ms}. The far
perspective unusually costs both implementations additional frametime.
Raycasting now uses around \qty{5.2}{\ms} and Raytracing \qty{4.7}{\ms}. But
the overall framerates are still great for interactivity.

% subsection brain (end)


\subsection{HotRoom} % (fold)
\label{sub:hotroom}

\begin{figure}
\begin{subfigure}{\columnwidth}
    \centering
    \includegraphics[width=\linewidth]{../images/hotroom_close_fb}
    \caption{close}
\end{subfigure}
\begin{subfigure}{\columnwidth}
    \centering
    \includegraphics[width=\linewidth]{../images/hotroom_far_fb}
    \caption{far}
\end{subfigure}
\caption{Benchmark perspectives of ``HotRoom'' dataset}%
\label{fig:view_hotroom}
\end{figure}

\begin{figure}
\centering
\includestandalone[width=\columnwidth]{../figures/frametimeBoxplotHotRoom}
\caption{Frametimes for ``HotRoom'' Dataset}%
\label{fig:comp_frametimes_hotroom}
\end{figure}

The ``HotRoom'' dataset (\cref{fig:view_hotroom}) seems to show the flow of
heated air in an enclosed space. With only 61 thousand splines it is another
small one, but chosen for the edge case of highly overlapping bounding boxes.

Raycasting seems quite loaded with \qty{12}{\ms} in the close perspective, while
raytracing can keep under \qty{8}{\ms}. The far perspective greatly eases up on
fragment processing for raycasting, which now achieves under \qty{6}{\ms} and
beats raytracing with \qty{6.2}{\ms}. Still all framerates remain solid
for \qty{60}{\hertz} refresh rates but the headroom has dwindled (see 
\cref{fig:comp_frametimes_hotroom}).

% subsection hotroom (end)


\subsection{Huge} % (fold)
\label{sub:huge}

\begin{figure}
\begin{subfigure}{\columnwidth}
    \centering
    \includegraphics[width=\linewidth]{../images/huge_close_fb}
    \caption{close}
\end{subfigure}
\begin{subfigure}{\columnwidth}
    \centering
    \includegraphics[width=\linewidth]{../images/huge_far_fb}
    \caption{far}
\end{subfigure}
\caption{Benchmark perspectives of ``Huge'' dataset}%
\label{fig:view_huge}
\end{figure}

\begin{figure}
\centering
\includestandalone[width=\columnwidth]{../figures/frametimeBoxplotHuge}
\caption{Frametimes for ``Huge'' Dataset}%
\label{fig:comp_frametimes_huge}
\end{figure}

``Huge'' (\cref{fig:view_huge}) is the first medium large dataset
with 1.5 million generated splines tracing random path. It shows the first
reliable performance generalization (see \cref{fig:comp_frametimes_huge}).

Closely zoomed in raycasting manages only around \qty{27}{\ms} while raytracing
uses \qty{8}{\ms}. Zooming out emphasizes this speedup further. Raycasting
gains little with frametimes still around \qty{24}{\ms}, while raytracing
nearly halves to little over \qty{4}{\ms}. Raytracing therefore still is easily
capable of 60 FPS interactive rates while RC begins to feel stuttery.

% subsection huge (end)


\subsection{Massive} % (fold)
\label{sub:massive}

\begin{figure}
\begin{subfigure}{\columnwidth}
    \centering
    \includegraphics[width=\linewidth]{../images/massive_close_fb}
    \caption{close}
\end{subfigure}
\begin{subfigure}{\columnwidth}
    \centering
    \includegraphics[width=\linewidth]{../images/massive_far_fb}
    \caption{far}
\end{subfigure}
\caption{Benchmark perspectives of ``Massive'' dataset}%
\label{fig:view_massive}
\end{figure}

\begin{figure}
\centering
\includestandalone[width=\columnwidth]{../figures/frametimeBoxplotMassive}
\caption{Frametimes for ``Massive'' Dataset}%
\label{fig:comp_frametimes_massive}
\end{figure}

``Massive'' (\cref{fig:view_massive}) is the second and finally large dataset
with 3.6 million splines, which further highlights the found performance
characteristics.

Raycasting achieves \qty{56}{\ms} while raytracing keeps under \qty{7}{\ms}
closely zoomed in. Trying to frame the traces from a farther perspective
marginally improves raycasting frametimes to \qty{54}{\ms}. Raytracing on the
other hand needs with \qty{7.7}{\ms} even more time. But overall raytracing
still manages very good framerates, well suited for interactivity, while
raycasting displays stuttery (see \cref{fig:comp_frametimes_massive}).

% subsection massive (end)


\subsection{Characteristics} % (fold)
\label{sub:characteristics}

\begin{figure*}
    \centering
    \includestandalone{../figures/frametimeComposition}
    \caption{Frametime composition}%
    \label{fig:frametime_composition}
\end{figure*}

\Cref{fig:frametime_composition} on \cpageref{fig:frametime_composition} 
displays all previous findings in a
general context. The frametimes are further divided into a NoOp part and the
full algorithm runtime. For raycasting this measures the geometry processing
overhead up until the fragment shader, which in turn just discards all
fragments. The rest, therefore, shows the actual time spent intersecting and
shading the spline.

Unfortunately this semantic is not directly applicable to raytracing. Here NoOp
means the time for shooting rays according to the \gls{sbt} into the \gls{as}.
The intersection shader always reports an intersection. The rest
therefore tries to capture the actual time spent in the intersection shader.

\begin{table*}
\sisetup{round-mode=places}
\centering
\begin{tabular}{r c r S[round-precision=2] S[round-precision=2] S[round-precision=2]}
\toprule{}
\textbf{Dataset} & \textbf{Variant} & \textbf{\#Splines} & {\textbf{RC}} & {\textbf{RT}} & {\textbf{Speedup}} \\
& & & {[\unit{\ms}]} & {[\unit{\ms}]} & {[1]} \\
\midrule{}
\multirow{2}{*}{Fibers}
& close & \multirow{2}{*}{7060} & 2.3793 & 3.8282 & 0.6215 \\
& far & & 0.8235 & 2.5046 & 0.3288 \\
\midrule{}
\multirow{2}{*}{Brain}
& close & \multirow{2}{*}{228978} & 4.8856 & 3.6345 & 1.3442 \\
& far & & 5.1772 & 4.648 & 1.1139 \\
\midrule{}
\multirow{2}{*}{HotRoom}
& close & \multirow{2}{*}{61896} & 11.8953 & 7.6561 & 1.5537 \\
& far & & 5.7878 & 6.4105 & 0.9029 \\
\midrule{}
\multirow{2}{*}{Huge}
& close & \multirow{2}{*}{1574654} & 27.7645 & 8.7165 & 3.1853 \\
& far & & 24.4131 & 4.4989 & 5.4265 \\
\midrule{}
\multirow{2}{*}{Massive}
& close & \multirow{2}{*}{3631046} & 56.3658 & 6.7558 & 8.3433 \\
& far & & 54.6008 & 7.7127 & 7.0793 \\
\bottomrule{}
\end{tabular}
\caption{Dataset and performance statistics}%
\label{tab:data_and_frame_stats}
\end{table*}

According to the measurement the following characteristics can be noticed.
Raytracing exhibits notable initial overhead. Raycasting performance
degradation scales linearly with dataset size, while raytracing demonstrates an
performance cost which seems to approach a constant factor. This is clearly
noticeable in \cref{tab:data_and_frame_stats} on \cpageref{tab:data_and_frame_stats}.
Raytracing frametimes keep in the same ballpark, while the speedup factor keeps
increasing.

% subsection characteristics (end)

\subsection{Overdraw} % (fold)
\label{sub:overdraw}

\begin{figure*}
    \begin{subfigure}{\textwidth}
        \includegraphics[width=.5\textwidth]{../images/fibers_close_fb}%
        \includegraphics[width=.5\textwidth]{../images/fibers_close_overdraw}
    \end{subfigure}\\[1ex]
    \begin{subfigure}{\textwidth}
        \includegraphics[width=.5\textwidth]{../images/fibers_far_fb}%
        \includegraphics[width=.5\textwidth]{../images/fibers_far_overdraw}
    \end{subfigure}
    \caption{``Fibers'' Overdraw}%
    \label{fig:fibers_overdraw}
\end{figure*}

\begin{figure*}
    \begin{subfigure}{\textwidth}
        \includegraphics[width=.5\textwidth]{../images/brain_close_fb}%
        \includegraphics[width=.5\textwidth]{../images/brain_close_overdraw}
    \end{subfigure}\\[1ex]
    \begin{subfigure}{\textwidth}
        \includegraphics[width=.5\textwidth]{../images/brain_far_fb}%
        \includegraphics[width=.5\textwidth]{../images/brain_far_overdraw}
    \end{subfigure}
    \caption{``Brain'' Overdraw}%
    \label{fig:brain_overdraw}
\end{figure*}

\begin{figure*}
    \begin{subfigure}{\textwidth}
        \includegraphics[width=.5\textwidth]{../images/hotroom_close_fb}%
        \includegraphics[width=.5\textwidth]{../images/hotroom_close_overdraw}
    \end{subfigure}\\[1ex]
    \begin{subfigure}{\textwidth}
        \includegraphics[width=.5\textwidth]{../images/hotroom_far_fb}%
        \includegraphics[width=.5\textwidth]{../images/hotroom_far_overdraw}
    \end{subfigure}
    \caption{``HotRoom'' Overdraw}%
    \label{fig:hotroom_overdraw}
\end{figure*}

\begin{figure*}
    \begin{subfigure}{\textwidth}
        \includegraphics[width=.5\textwidth]{../images/huge_close_fb}%
        \includegraphics[width=.5\textwidth]{../images/huge_close_overdraw}
    \end{subfigure}\\[1ex]
    \begin{subfigure}{\textwidth}
        \includegraphics[width=.5\textwidth]{../images/huge_far_fb}%
        \includegraphics[width=.5\textwidth]{../images/huge_far_overdraw}
    \end{subfigure}
    \caption{``Huge'' Overdraw}%
    \label{fig:huge_overdraw}
\end{figure*}

\begin{figure*}
    \begin{subfigure}{\textwidth}
        \includegraphics[width=.5\textwidth]{../images/massive_close_fb}%
        \includegraphics[width=.5\textwidth]{../images/massive_close_overdraw}
    \end{subfigure}\\[1ex]
    \begin{subfigure}{\textwidth}
        \includegraphics[width=.5\textwidth]{../images/massive_far_fb}%
        \includegraphics[width=.5\textwidth]{../images/massive_far_overdraw}
    \end{subfigure}
    \caption{``Massive'' Overdraw}%
    \label{fig:massive_overdraw}
\end{figure*}

The main bottleneck for the raycasting approach seems to stem from massive
overdraw issues. Even conservative depth extension cannot fully counteract this
phenomenon.

\Cref{fig:fibers_overdraw,fig:brain_overdraw,fig:hotroom_overdraw,fig:huge_overdraw,fig:massive_overdraw}
on \cpagerefrange{fig:fibers_overdraw}{fig:massive_overdraw}
display an color mapped grayscale image, where each pixel accumulates the amount
of times an overdraw has happened. The blue spectrum here emphasizes overdraw 
around 1 to 10 times and the rest shows the hotspots. Light yellow regions
are clipped which means more than 255 overdraws have happened.

\begin{figure*}
    \includegraphics[width=\textwidth]{../images/fibers_rt_aabb}
    \caption{The \gls{aabb} structure enveloping all splines of the ``Fibers''
     dataset.}%
    \label{fig:rt_aabb_overlay}
\end{figure*}

\Cref{fig:rt_aabb_overlay} on \cpageref{fig:rt_aabb_overlay} shows an overlay of
 the \gls{aabb} structure enclosing all splines. Each of the pixels colored in
 the primary colors highlight, where raytracing loses its performance. For
 every invocation of the custom intersection shader the hardware needs to do an
 expensive context switch. This means the hardware accelerated ray traversal
 switches from RT cores to general purpose cores. Therefore, all AABBs hits
 which result in no actual intersection should be avoided.

% subsection overdraw (end)

% section performance_evaluation (end)


\section{Summary} % (fold)
\label{sec:summary}

The presented work clearly shows that the RTX accelerated implementation scales
very well on huge datasets. Nonetheless, raycasting exhibits significantly lower
overhead for small to medium datasets. Furthermore, it is shown that raycasting
suffers from extreme overdraw issues which are resolved by the correct early
abort on ray traversal in the RTX framework.

As raytracing handles huge amounts of splines easily, further improvements could
be achieved by reducing the empty volume in the \glspl{aabb} through generous
subdivisions, where each \gls{aabb} approaches a cubic shape.

% section summary (end)

\clearpage{}

\printglossaries{}
\printbibliography{}

\end{document}
#ifndef __VK_MACROS_H__
#define __VK_MACROS_H__

#include <vulkan/vulkan.h>

#include <cgvu/vk/vulkan.h>

void chkd(VkResult result);
void createBuffer(cgvu::Context &ctx, VkDeviceSize size, VkBufferUsageFlags usageFlags,
                  VkMemoryPropertyFlags propertyFlags, VkBuffer &buffer,
                  VkDeviceMemory &bufferMemory, VkMemoryAllocateFlagsInfo *allocateFlags = nullptr);
void copyBuffer(cgvu::Context &ctx, VkBuffer &srcBuffer, VkBuffer &dstBuffer, VkDeviceSize size);
template <typename InIt>
void uploadData(cgvu::Context &ctx, InIt begin, InIt end, VkBuffer &dstBuffer,
                VkDeviceMemory &bufferMemory, VkBufferUsageFlags usageFlags)
{
    VkDevice dev = ctx.handle();

    const VkDeviceSize bufferSize{std::distance(begin, end) * sizeof(InIt::value_type)};

    VkBuffer stagingBuffer;
    VkDeviceMemory stagingDeviceMemory;
    createBuffer(ctx, bufferSize, VK_BUFFER_USAGE_TRANSFER_SRC_BIT,
                 VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT,
                 stagingBuffer, stagingDeviceMemory);

    void *data{nullptr};
    vkMapMemory(dev, stagingDeviceMemory, 0, bufferSize, 0, &data);
    std::copy(begin, end, reinterpret_cast<InIt::value_type *>(data));
    vkUnmapMemory(dev, stagingDeviceMemory);

    // Ensure that the usage flag includes device locality
    VkMemoryAllocateFlagsInfo allocInfo = {};
    allocInfo.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_FLAGS_INFO;
    allocInfo.flags = VK_MEMORY_ALLOCATE_DEVICE_ADDRESS_BIT;

    createBuffer(ctx, bufferSize, usageFlags | VK_BUFFER_USAGE_TRANSFER_DST_BIT,
                 VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT, dstBuffer,
                 bufferMemory, &allocInfo);
    copyBuffer(ctx, stagingBuffer, dstBuffer, bufferSize);
    vkDestroyBuffer(dev, stagingBuffer, nullptr);
    vkFreeMemory(dev, stagingDeviceMemory, nullptr);
}

void createImage(cgvu::Context &ctx, uint32_t width, uint32_t height, VkFormat format,
                 VkImageTiling tiling, VkImageUsageFlags usageFlags,
                 VkMemoryPropertyFlags propertyFlags, VkImage &image, VkDeviceMemory &imageMemory,
                 VkMemoryAllocateFlagsInfo *allocateFlags = nullptr);

#endif // !__VK_MACROS_H__

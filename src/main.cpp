
//////
//
// Includes
//

// C++ STL
#include <array>
#include <cstddef>
#include <exception>
#include <fstream>
#include <iostream>
#include <sstream>
#include <stdexcept>
#include <string>
#include <vector>

// GLFW library (including Vulkan)
#define GLFW_INCLUDE_VULKAN
#include <GLFW/glfw3.h>

// GLM library
#define GLM_FORCE_SWIZZLE 1
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

// ImGUI library
// - ToDo: abstract away behind framework functionality to eliminate this include
#include <imgui.h>

// CGVu library
#include <cgvu/application.h>
#include <cgvu/util/utils.h>
#include <cgvu/vk/buffer.h>
#include <cgvu/vk/shaderprog.h>
#include <cgvu/vk/texture.h>
#include <cgvu/vk/vulkan_internals.h>

// Local includes
#include "bezdat.h"
#include "curveutils.h"
#include "hspline.h"
#include "rtx_structs.h"
#include "transformation.h"
#include "vk_macros.h"

//////
//
// Setup constants
//

int viewportWidth = 1280;
int viewportHeight = 800;

//////
//
// Module-private structs
//

// Encapsulates the Vulkan RTX entry points
struct RTXfuncs
{
    // Convenience type for map of Context -> function table
    typedef std::map<const cgvu::Context *, RTXfuncs> Map;

    // List of RTX functions used by the app
    PFN_vkCreateAccelerationStructureKHR vkCreateAccelerationStructureKHR;
    PFN_vkGetAccelerationStructureBuildSizesKHR vkGetAccelerationStructureBuildSizesKHR;
    PFN_vkGetAccelerationStructureDeviceAddressKHR vkGetAccelerationStructureDeviceAddressKHR;
    PFN_vkCmdBuildAccelerationStructuresKHR vkCmdBuildAccelerationStructuresKHR;
    PFN_vkCreateRayTracingPipelinesKHR vkCreateRayTracingPipelinesKHR;
    PFN_vkGetRayTracingShaderGroupHandlesKHR vkGetRayTracingShaderGroupHandlesKHR;
    PFN_vkCmdTraceRaysKHR vkCmdTraceRaysKHR;

    // Default constuctor for use with STL containers
    RTXfuncs() {}

    // Actual-use constructor
    RTXfuncs(const cgvu::Context &ctx)
    {
        vkCreateAccelerationStructureKHR =
            (PFN_vkCreateAccelerationStructureKHR)vkGetDeviceProcAddr(
                ctx.handle(), "vkCreateAccelerationStructureKHR");
        vkGetAccelerationStructureBuildSizesKHR =
            (PFN_vkGetAccelerationStructureBuildSizesKHR)vkGetDeviceProcAddr(
                ctx.handle(), "vkGetAccelerationStructureBuildSizesKHR");
        vkGetAccelerationStructureDeviceAddressKHR =
            (PFN_vkGetAccelerationStructureDeviceAddressKHR)vkGetDeviceProcAddr(
                ctx.handle(), "vkGetAccelerationStructureDeviceAddressKHR");
        vkCmdBuildAccelerationStructuresKHR =
            (PFN_vkCmdBuildAccelerationStructuresKHR)vkGetDeviceProcAddr(
                ctx.handle(), "vkCmdBuildAccelerationStructuresKHR");
        vkCreateRayTracingPipelinesKHR =
            (PFN_vkCreateRayTracingPipelinesKHR)vkGetDeviceProcAddr(
                ctx.handle(), "vkCreateRayTracingPipelinesKHR");
        vkGetRayTracingShaderGroupHandlesKHR =
            (PFN_vkGetRayTracingShaderGroupHandlesKHR)vkGetDeviceProcAddr(
                ctx.handle(), "vkGetRayTracingShaderGroupHandlesKHR");
        vkCmdTraceRaysKHR =
            (PFN_vkCmdTraceRaysKHR)vkGetDeviceProcAddr(ctx.handle(), "vkCmdTraceRaysKHR");
    }

    // Obtain an RTX function table for a given context
    static RTXfuncs &obtain(RTXfuncs::Map &map, const cgvu::Context &ctx)
    {
        auto it = map.find(&ctx);
        if (it != map.end())
            return it->second;
        return map.emplace(&ctx, ctx).first->second;
    }
};

//////
//
// Module-private classes
//

// Application
class RTXtubesApp : public cgvu::ApplicationStub<float>
{

  public:
    ////
    // Exported types

    // Hermite Node vertex type
    struct NodeVertex
    {
        Vec4 pos, tan, col, dcol;
        Vec2 rad;

        inline static cgvu::BufferBindingDescs &bindingDescs(void)
        {
            static cgvu::BufferBindingDescs bd = {{0, sizeof(NodeVertex), false}};
            return bd;
        }
        inline static cgvu::AttribDescs &attributeDescs(void)
        {
            static cgvu::AttribDescs ad = {
                {0, 0, offsetof(NodeVertex, pos), cgvu::AFmt::VEC4_FLT32},
                {1, 0, offsetof(NodeVertex, tan), cgvu::AFmt::VEC4_FLT32},
                {2, 0, offsetof(NodeVertex, col), cgvu::AFmt::VEC4_FLT32},
                {3, 0, offsetof(NodeVertex, dcol), cgvu::AFmt::VEC4_FLT32},
                {4, 0, offsetof(NodeVertex, rad), cgvu::AFmt::VEC2_FLT32}};
            return ad;
        }
    };

    // Quadratic Spline
    struct QSpline
    {
        mat3 pos_spline{};
        mat3x4 col_spline{};
        vec3 rad_spline{};

        QSpline(mat3 pos, mat3x4 col, vec3 rad)
            : pos_spline(pos), col_spline(col), rad_spline(rad){};
    };

    // Uniform structs
    struct CGVuTransforms
    {
        Mat4 modelview;
        Mat4 proj;
        Mat4 normalMat;
    };
    struct CGVuMaterial
    {
        Vec4 albedo;
        float specularity;
        float shininess;
    };
    struct CGVuGlobalLight
    {
        Vec3 pos; // in eye space
        alignas(16) Vec3 color;
    };

  protected:
    ////
    // Feature structs
    VkPhysicalDeviceAccelerationStructureFeaturesKHR pdasf;
    VkPhysicalDeviceRayTracingPipelineFeaturesKHR pdrtpf;

    ////
    // Data members

    // Low-level Vulkan objects
    // - function tables
    RTXfuncs::Map RTXfuncTable;
    
    // - raytracing memory
    VkBuffer aabbPositionsBuffer;
    VkDeviceMemory aabbPositionsDeviceMemory;

    VkAccelerationStructureKHR blas;
    VkBuffer blasBuffer;
    VkDeviceMemory blasDeviceMemory;
    
    VkAccelerationStructureKHR tlas;
    VkBuffer tlasBuffer;
    VkDeviceMemory tlasDeviceMemory;

    VkBuffer geometryInformationBuffer;
    VkDeviceMemory geometryInformationDeviceMemory;

    VkBuffer nodeBuffer;
    VkDeviceMemory nodeDeviceMemory;
    
    // - raytracing shader stuff
    VkDescriptorSetLayout rtDescriptorSetLayout;
    VkDescriptorPool descriptorPool;
    VkDescriptorSet rtDescriptorSet;

    VkImageView rayTraceImageView;
    VkImage rayTraceImage;
    VkDeviceMemory rayTraceImageMemory;

    // - raytracing pipeline
    VkPipelineLayout rtPipelineLayout;
    VkPipeline rtPipeline;

    VkBuffer rtShaderBindingTableBuffer;
    VkDeviceMemory rtShaderBindingTableDeviceMemory;

    // - raytracing command buffers
    std::vector<VkCommandBuffer> cmdRaytrace;

    // High-level Vulkan objects
    // - command buffers
    cgvu::CommandBuffer cmdRaycast;

    // Dataset
    // - geometry
    std::vector<NodeVertex> vertices;
    std::vector<unsigned> indices;
    std::vector<std::pair<Vec3, Vec3>> aabbVertices;
    std::vector<QSpline> quadSplines;
    // - buffers
    cgvu::StorageBuffer vertexBuf;
    cgvu::StorageBuffer indexBuf;

    // The test shader
    cgvu::ShaderProgram sh;
    cgvu::UniformBuffer shUniforms;
    cgvu::TypedUniform<CGVuTransforms> shTrans;
    cgvu::TypedUniform<CGVuMaterial> shMat;
    cgvu::TypedUniform<CGVuGlobalLight> shLight;

    // The test pipeline
    cgvu::Pipeline pl;

    // State
    Real angle = 0;

  public:
    ////
    // Object construction / destruction

    // Virtual base destructor. Causes vtable creation.
    RTXtubesApp() : sh(cgvu::SHADER_SRC_SPK, "shader/hsplinetube_rc.spk")
    {
        configureMainWindow(viewportWidth, viewportHeight);
#ifdef _DEBUG
        setVulkanLogLevel(cgvu::Dbg::LOADER_DRIVER, cgvu::log::INFO);
        setVulkanLogLevel(cgvu::Dbg::VALIDATION, cgvu::log::WARNING);
#else
        setVulkanLogLevel(cgvu::Dbg::LOADER_DRIVER, cgvu::log::WARNING);
#endif
    }

    ////
    // Interface: cgvu::Application

    // App name
    virtual std::string getName(void) { return "RTXtubes"; }

    // Instance extensions we require
    virtual void declareRequiredInstanceExtensions(std::set<std::string> *out) const override {}

    // Instance layers we require
    virtual void declareRequiredInstanceLayers(std::set<std::string> *out) const override {}

    virtual void declareRequiredDeviceExtensions(
        std::map<std::string, cgvu::Vulkan::DeviceExtensionInfo> *out) const override
    {
        static VkPhysicalDeviceAccelerationStructureFeaturesKHR acceleration_structure_features = {
            // sType
            VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_ACCELERATION_STRUCTURE_FEATURES_KHR,
            // pNext (ignored, will be set up by cgvu internally)
            nullptr,
            // accelerationStructure;
            VK_TRUE,
            // accelerationStructureCaptureReplay
            VK_FALSE,
            // accelerationStructureIndirectBuild
            VK_FALSE,
            // accelerationStructureHostCommands
            VK_FALSE,
            // descriptorBindingAccelerationStructureUpdateAfterBind
            VK_FALSE};
        static VkPhysicalDeviceRayTracingPipelineFeaturesKHR rt_pipeline_features = {
            // sType
            VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_RAY_TRACING_PIPELINE_FEATURES_KHR,
            // pNext (ignored, will be set up by cgvu internally)
            nullptr,
            // rayTracingPipeline
            VK_TRUE,
            // rayTracingPipelineShaderGroupHandleCaptureReplay
            VK_FALSE,
            // rayTracingPipelineShaderGroupHandleCaptureReplayMixed
            VK_FALSE,
            // rayTracingPipelineTraceRaysIndirect,
            VK_FALSE,
            // rayTraversalPrimitiveCulling
            VK_TRUE};
        static VkPhysicalDeviceBufferDeviceAddressFeatures buffer_device_address_features = {
            // sType
            VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_BUFFER_DEVICE_ADDRESS_FEATURES,
            // pNext
            nullptr,
            // bufferDeviceAddress
            VK_TRUE,
            // bufferDeviceAddressCaptureReplay;
            VK_FALSE,
            // bufferDeviceAddressMultiDevice;
            VK_FALSE};

        using DEI = cgvu::Vulkan::DeviceExtensionInfo;
        /* out->emplace("VK_KHR_acceleration_structure", DEI{0, &acceleration_structure_features});
        out->emplace("VK_KHR_ray_tracing_pipeline", DEI{0, &rt_pipeline_features});
        out->emplace("VK_KHR_buffer_device_address", DEI{0, &buffer_device_address_features});*/
    }

    // Post-constructor init
    virtual void mainInit(cgvu::Context &ctx) override
    {
        frametime_file = std::ofstream("frametime.log", std::ios::trunc);

        // Load dataset
        auto &bezdat = BezdatHandler<Real>::obtainRef();
        const std::string dataFilename(
            "../data/Fibers.bezdat"); // Large_seed128-n1250-t2000-v0.25 |
                                      // Huge_seed128-n7000-t6333-v0.015625
        std::ifstream dataFile(dataFilename);
        HermiteSpline<Real> splines = bezdat.read(dataFile);
        std::cout << "Dataset \"" << cgvu::util::nameWithoutPath(dataFilename) << "\":" << std::endl
                  << "  - tubes: " << splines.curveCount << std::endl
                  << "  - segments: " << splines.segments.size() << std::endl
                  << "  - nodes: " << splines.positions.size() << std::endl
                  << std::endl;

        buildRaycastShader(ctx);
        prepareRaycastDataset(splines);
        uploadRaycastDataset(ctx);
        createRaycastPipeline(ctx);
        buildRaycastCommandBuffer(ctx);

        // Attach light to camera (by specifying position in eye coordinates)
        shLight.initialize({/*pos*/ {0.125, 0.0625, 0}, /*color*/ {1, 1, 1}});

        /* prepareRaytracingDataset(splines);
        uploadRaytracingDataset(ctx);*/

        ////
        // RTX
        /* buildBLAS(ctx);
        buildTLAS(ctx);

        createImageView(ctx);
        createDescriptorSets(ctx);
        createRayTracePipeline(ctx);
        createShaderBindingTable(ctx);
        createRTCommandBuffer(ctx);*/
    }

    // GUI drawing
    void drawGUI(cgvu::Context &context)
    {
        // Create a window called "My First Tool", with a menu bar.
        ImGui::Begin("Options", &shouldShowOptions, ImGuiWindowFlags_None);

        ImGui::Checkbox("Autorotate Dataset", &shouldAnimate);
        ImGui::Checkbox("Use Raytracing", &shouldUseRaytracing);
        ImGui::Checkbox("Write Frametimes", &shouldWriteFrametimes);

        ImGui::End();
    }

    // Main loop body
    virtual bool mainLoop(cgvu::Context &ctx, const Stats &stats) override
    {
        // Update State
        angle = shouldAnimate ? glm::mod<Real>(angle + 5 * stats.frameTime, 360) : angle;

        // Handle input
        auto &io = ImGui::GetIO();

        // Apply node transformation
        auto &transf = transforms();
        transf.pushModelview();
        transf.modelview() = glm::rotate(transf.modelview(), (float)glm::radians(angle), {1, 1, 0});

        // Update uniforms
        auto &trans = shTrans.set();
        trans.modelview = transf.modelview();
        trans.proj = transf.projection();
        trans.normalMat = transf.getNormalMat();

        // Render
        if (shouldUseRaytracing)
        {
            drawRayTraceImage(ctx, ctx.getSwapImageID());
        }
        else
        {
            ctx.submitCommands(cmdRaycast);
        }

        if (shouldWriteFrametimes)
        {
            frametime_file << stats.frameTime << '\n';
        }

        // Reset our changes to the transformation stack
        transf.popModelview();

        // Shut down on ESC key
        return !(io.WantCaptureKeyboard) && ImGui::IsKeyPressed(GLFW_KEY_ESCAPE, false);
    }

  private:
    std::ofstream frametime_file;

    bool shouldAnimate{false};
    bool shouldShowOptions{true};
    bool shouldUseRaytracing{false};
    bool shouldWriteFrametimes{false};

    void buildRaycastShader(cgvu::Context &ctx)
    {
        sh.build(ctx, {
                          {0, cgvu::UFrm::INTEGRAL, 1, cgvu::SH::ALL_GFX},
                          {1, cgvu::UFrm::INTEGRAL, 1, cgvu::SH::FRAGMENT},
                          {2, cgvu::UFrm::INTEGRAL, 1, cgvu::SH::FRAGMENT},
                      });
        shUniforms = cgvu::UniformBuffer(ctx);
        shTrans = shUniforms.insert<CGVuTransforms>();
        shMat = shUniforms.insert<CGVuMaterial>();
        shLight = shUniforms.insert<CGVuGlobalLight>();
        shUniforms.build();
        sh.updateDescriptorSets({cgvu::UniformUpdate(0, shTrans), cgvu::UniformUpdate(1, shMat),
                                 cgvu::UniformUpdate(2, shLight)});
    }

    void prepareRaycastDataset(HermiteSpline<Real> const &splines)
    {
        // Setup dataset for rendering
        // - material
        shMat.initialize({/*albedo*/ {1, 1, 1, 1}, /*specularity*/ 0.5, /*shininess*/ 96});
        // - nodes
        vertices.reserve(splines.positions.size());
        for (size_t i = 0; i < vertices.capacity(); i++)
            vertices.emplace_back(NodeVertex{splines.positions[i].val, splines.positions[i].der,
                                             splines.colors[i].val, splines.colors[i].der,
                                             Vec2{splines.radii[i].val, splines.radii[i].der}});
        // - segments
        indices.reserve(splines.segments.size() * 2);
        for (const auto &seg : splines.segments)
        {
            indices.push_back(seg.n0);
            indices.push_back(seg.n1);
        }
    }

    void prepareRaytracingDataset(HermiteSpline<Real> const &splines)
    {
        for (auto &&segment : splines.segments)
        {
            auto &&sp = splines.positions;
            auto &&sc = splines.colors;
            auto &&sr = splines.radii;

            // Convert the given Hermite spline into a Bezier spline
            const auto pos = toBezier(vec3(sp[segment.n0].val.xyz), vec3(sp[segment.n0].der.xyz),
                                      vec3(sp[segment.n1].val.xyz), vec3(sp[segment.n1].der.xyz));
            const auto col = toBezier(vec4(sc[segment.n0].val), vec4(sc[segment.n0].der),
                                      vec4(sc[segment.n1].val), vec4(sc[segment.n1].der));
            const auto rad = toBezier(sr[segment.n0].val, sr[segment.n0].der, sr[segment.n1].val,
                                      sr[segment.n1].der);
            const auto qpos =
                quadraticSubdivide<CubicCurve3<float>, QuadraticCurve3<float>, Vec3>(pos);
            const auto qcol =
                quadraticSubdivide<CubicCurve4<float>, QuadraticCurve4<float>, Vec4>(col);
            const auto qrad =
                quadraticSubdivide<CubicCurve1<float>, QuadraticCurve1<float>, float>(rad);

            // Every sub spline has an quadratic position and quadratic radius spline
            for (auto &&[pos, col, rad] : {std::make_tuple(qpos.first, qcol.first, qrad.first),
                                           std::make_tuple(qpos.second, qcol.second, qrad.second)})
            {
                const auto basis = findBasis(pos);
                const auto R = glm::transpose(basis);
                const mat2x3 extremas = findExtremas(R * pos, rad);
                aabbVertices.push_back(std::make_pair(extremas[0], extremas[1]));

                quadSplines.emplace_back(pos, col, rad);
            }
        }
    }

    void uploadRaycastDataset(cgvu::Context &ctx)
    {
        vertexBuf = cgvu::StorageBuffer(ctx, cgvu::SBT::VERTEX);
        vertexBuf.upload(vertices);
        indexBuf = cgvu::StorageBuffer(ctx, cgvu::SBT::INDEX);
        indexBuf.upload(indices);
    }

    void uploadRaytracingDataset(cgvu::Context &ctx)
    {
        uploadData(ctx, std::cbegin(quadSplines), std::cend(quadSplines), nodeBuffer,
                   nodeDeviceMemory,
                   VK_BUFFER_USAGE_STORAGE_BUFFER_BIT | VK_BUFFER_USAGE_SHADER_DEVICE_ADDRESS_BIT);
    }

    void createRaycastPipeline(cgvu::Context &ctx)
    {
        pl = ctx.createPipeline(sh);
        pl.setInputBindings<NodeVertex>(cgvu::PT::LINE_LIST);
        pl.attachDepthStencilBuffer(mainDepthStencil());
        pl.build();
    }

    void buildRaycastCommandBuffer(cgvu::Context &ctx)
    {
        cmdRaycast = ctx.createCommandBuffer(cgvu::TC::RENDER, false);
        cmdRaycast.startRecording();
        cmdRaycast.bindPipeline(pl);
        cmdRaycast.bindBuffer(NodeVertex::bindingDescs()[0].binding, vertexBuf);
        cmdRaycast.bindIndexBuffer(indexBuf);
        cmdRaycast.drawIndexed(indices.size(), 1, 0);
        cmdRaycast.build();
    }

    void buildAS(cgvu::Context& ctx, VkAccelerationStructureBuildGeometryInfoKHR &asbgInfo, VkAccelerationStructureBuildRangeInfoKHR *const asbriPtr)
    {
        // Vulkan shortcuts
        VkDevice dev = ctx.handle();
        auto &devInfo = *(VulkanDeviceInfo *)ctx.getDeviceInfo();

        // Load functions entry points
        auto &rtx = RTXfuncs::obtain(RTXfuncTable, ctx);

        cgvu::CommandBuffer cmdBuffer = ctx.createCommandBuffer(cgvu::TaskClass::RENDER, true);
        VkCommandBuffer cmd = cmdBuffer.handle(0);

        VkCommandBufferBeginInfo bi = {};
        bi.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
        bi.flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT;
        bi.pInheritanceInfo = nullptr;

        chkd(vkBeginCommandBuffer(cmd, &bi));
        rtx.vkCmdBuildAccelerationStructuresKHR(cmdBuffer.handle(0), 1, &asbgInfo, &asbriPtr);
        chkd(vkEndCommandBuffer(cmd));

        VkSubmitInfo si = {};
        si.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
        si.commandBufferCount = 1;
        si.pCommandBuffers = &cmd;

        VkFenceCreateInfo fci = {};
        fci.sType = VK_STRUCTURE_TYPE_FENCE_CREATE_INFO;
        VkFence f;
        chkd(vkCreateFence(dev, &fci, nullptr, &f));

        chkd(vkQueueSubmit(devInfo.getGraphicsQueue(), 1, &si, f));
        chkd(vkWaitForFences(dev, 1, &f, VK_FALSE, 60'000'000'000));
    }

    void buildBLAS(cgvu::Context &ctx)
    {
        // Prepare AABB vertices
        std::vector<VkAabbPositionsKHR> aabbPositions;
        for (const auto &[pmin, pmax] : aabbVertices)
        {
            auto &pos = aabbPositions.emplace_back();
            pos.minX = pmin.x;
            pos.minY = pmin.y;
            pos.minZ = pmin.z;
            pos.maxX = pmax.x;
            pos.maxY = pmax.y;
            pos.maxZ = pmax.z;
        }

        // Vulkan shortcuts
        VkDevice dev = ctx.handle();
        auto &devInfo = *(VulkanDeviceInfo *)ctx.getDeviceInfo();

        // Load functions entry points
        auto &rtx = RTXfuncs::obtain(RTXfuncTable, ctx);

        uploadData(ctx, std::cbegin(aabbPositions), std::cend(aabbPositions), aabbPositionsBuffer,
                   aabbPositionsDeviceMemory,
                   VK_BUFFER_USAGE_STORAGE_BUFFER_BIT | VK_BUFFER_USAGE_SHADER_DEVICE_ADDRESS_BIT);

        VkBufferDeviceAddressInfo bdaInfo = {};
        bdaInfo.sType = VK_STRUCTURE_TYPE_BUFFER_DEVICE_ADDRESS_INFO;
        bdaInfo.buffer = aabbPositionsBuffer;

        VkAccelerationStructureGeometryKHR asGeometry;
        asGeometry.sType = VK_STRUCTURE_TYPE_ACCELERATION_STRUCTURE_GEOMETRY_KHR;
        asGeometry.pNext = nullptr;
        asGeometry.geometryType = VK_GEOMETRY_TYPE_AABBS_KHR;
        asGeometry.geometry.aabbs.sType =
            VK_STRUCTURE_TYPE_ACCELERATION_STRUCTURE_GEOMETRY_AABBS_DATA_KHR;
        asGeometry.geometry.aabbs.pNext = nullptr;
        asGeometry.geometry.aabbs.data.deviceAddress = vkGetBufferDeviceAddress(dev, &bdaInfo);
        asGeometry.geometry.aabbs.stride = sizeof(VkAabbPositionsKHR);
        asGeometry.flags = VK_GEOMETRY_OPAQUE_BIT_KHR;

        VkAccelerationStructureBuildGeometryInfoKHR asbgInfo = {};
        asbgInfo.sType = VK_STRUCTURE_TYPE_ACCELERATION_STRUCTURE_BUILD_GEOMETRY_INFO_KHR;
        asbgInfo.type = VK_ACCELERATION_STRUCTURE_TYPE_BOTTOM_LEVEL_KHR;
        asbgInfo.flags = VK_BUILD_ACCELERATION_STRUCTURE_PREFER_FAST_TRACE_BIT_KHR |
                         VK_BUILD_ACCELERATION_STRUCTURE_ALLOW_COMPACTION_BIT_KHR;
        asbgInfo.mode = VK_BUILD_ACCELERATION_STRUCTURE_MODE_BUILD_KHR;
        asbgInfo.geometryCount = 1;
        asbgInfo.pGeometries = &asGeometry;
        asbgInfo.ppGeometries = nullptr;

        // Determine acceleration structure and scratch buffer sizes
        VkAccelerationStructureBuildSizesInfoKHR asbsInfo = {};
        asbsInfo.sType = VK_STRUCTURE_TYPE_ACCELERATION_STRUCTURE_BUILD_SIZES_INFO_KHR;

        rtx.vkGetAccelerationStructureBuildSizesKHR(
            dev,
            VkAccelerationStructureBuildTypeKHR::VK_ACCELERATION_STRUCTURE_BUILD_TYPE_DEVICE_KHR,
            &asbgInfo, &asbgInfo.geometryCount, &asbsInfo);

        // Ensure that the usage flag includes device locality
        VkMemoryAllocateFlagsInfo allocInfo = {};
        allocInfo.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_FLAGS_INFO;
        allocInfo.flags = VK_MEMORY_ALLOCATE_DEVICE_ADDRESS_BIT;

        // Create the necessary buffers
        createBuffer(ctx, asbsInfo.accelerationStructureSize,
                     VK_BUFFER_USAGE_ACCELERATION_STRUCTURE_STORAGE_BIT_KHR |
                         VK_BUFFER_USAGE_SHADER_DEVICE_ADDRESS_BIT,
                     VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT, blasBuffer, blasDeviceMemory, &allocInfo);

        VkBuffer scratchBuffer;
        VkDeviceMemory scratchDeviceMemory;
        createBuffer(ctx, asbsInfo.buildScratchSize,
                     VK_BUFFER_USAGE_ACCELERATION_STRUCTURE_BUILD_INPUT_READ_ONLY_BIT_KHR |
                         VK_BUFFER_USAGE_SHADER_DEVICE_ADDRESS_BIT,
                     VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT, scratchBuffer, scratchDeviceMemory,
                     &allocInfo);

        bdaInfo.buffer = scratchBuffer;
        asbgInfo.scratchData.deviceAddress = vkGetBufferDeviceAddress(dev, &bdaInfo);

        VkAccelerationStructureCreateInfoKHR asci = {};
        asci.sType = VK_STRUCTURE_TYPE_ACCELERATION_STRUCTURE_CREATE_INFO_KHR;
        asci.createFlags = 0;
        asci.buffer = blasBuffer;
        asci.offset = 0;
        asci.size = asbsInfo.accelerationStructureSize;
        asci.type = VK_ACCELERATION_STRUCTURE_TYPE_BOTTOM_LEVEL_KHR;
        //asci.deviceAddress = VK_NULL_HANDLE;

        chkd(rtx.vkCreateAccelerationStructureKHR(dev, &asci, nullptr, &blas));

        asbgInfo.dstAccelerationStructure = blas;

        const auto asbrInfo = std::make_unique<VkAccelerationStructureBuildRangeInfoKHR>();
        asbrInfo->primitiveCount = aabbPositions.size();
        asbrInfo->primitiveOffset = 0;
        asbrInfo->firstVertex = 0;
        asbrInfo->transformOffset = 0;

		buildAS(ctx, asbgInfo, asbrInfo.get());

        vkDestroyBuffer(dev, scratchBuffer, nullptr);
        vkFreeMemory(dev, scratchDeviceMemory, nullptr);
    }

    void buildTLAS(cgvu::Context &ctx)
    {
        // Vulkan shortcuts
        VkDevice dev = ctx.handle();
        auto &devInfo = *(VulkanDeviceInfo *)ctx.getDeviceInfo();

        // Load functions entry points
        auto &rtx = RTXfuncs::obtain(RTXfuncTable, ctx);

		// Brace initialization ensures all entries are 0.f
		VkTransformMatrixKHR identityTransform = {};
        identityTransform.matrix[0][0] = 1.f;
        identityTransform.matrix[1][1] = 1.f;
        identityTransform.matrix[2][2] = 1.f;

        VkAccelerationStructureDeviceAddressInfoKHR accelerationStructureDeviceAddressInfo = {};
        accelerationStructureDeviceAddressInfo.sType =
            VK_STRUCTURE_TYPE_ACCELERATION_STRUCTURE_DEVICE_ADDRESS_INFO_KHR;
        accelerationStructureDeviceAddressInfo.accelerationStructure = blas;

        VkDeviceAddress accelerationStructureDeviceAddress =
            rtx.vkGetAccelerationStructureDeviceAddressKHR(dev,
                                                           &accelerationStructureDeviceAddressInfo);

		VkAccelerationStructureInstanceKHR asInstance{};
        asInstance.transform = identityTransform;
        asInstance.mask = 0xFF;
        asInstance.accelerationStructureReference = accelerationStructureDeviceAddress;

        // upload staging data
        VkDeviceSize giBufferSize = sizeof(VkAccelerationStructureInstanceKHR);
        
        {
            VkBuffer giStagingBuffer;
            VkDeviceMemory giStagingBufferMemory;
            createBuffer(ctx, giBufferSize, VK_BUFFER_USAGE_TRANSFER_SRC_BIT,
                         VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT,
                         giStagingBuffer, giStagingBufferMemory);
            void *giData{};
            vkMapMemory(dev, giStagingBufferMemory, 0, giBufferSize, 0, &giData);
            std::copy(&asInstance, &asInstance + 1,
                      reinterpret_cast<VkAccelerationStructureInstanceKHR *>(giData));
            vkUnmapMemory(dev, giStagingBufferMemory);

            // Ensure that the usage flag includes device locality
            VkMemoryAllocateFlagsInfo allocInfo = {};
            allocInfo.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_FLAGS_INFO;
            allocInfo.flags = VK_MEMORY_ALLOCATE_DEVICE_ADDRESS_BIT;

            createBuffer(ctx, giBufferSize,
                         VK_BUFFER_USAGE_TRANSFER_DST_BIT |
                             VK_BUFFER_USAGE_SHADER_DEVICE_ADDRESS_BIT,
                         VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT, geometryInformationBuffer, geometryInformationDeviceMemory, &allocInfo);
            copyBuffer(ctx, giStagingBuffer, geometryInformationBuffer, giBufferSize);
            vkDestroyBuffer(dev, giStagingBuffer, nullptr);
            vkFreeMemory(dev, giStagingBufferMemory, nullptr);
        }

        // Get the instance buffers address
        VkBufferDeviceAddressInfo gibdaInfo = {};
        gibdaInfo.sType = VK_STRUCTURE_TYPE_BUFFER_DEVICE_ADDRESS_INFO;
        gibdaInfo.buffer = geometryInformationBuffer;

        VkDeviceAddress gibAddress = vkGetBufferDeviceAddress(dev, &gibdaInfo);

        VkAccelerationStructureGeometryKHR asGeometry;
        asGeometry.sType = VK_STRUCTURE_TYPE_ACCELERATION_STRUCTURE_GEOMETRY_KHR;
        asGeometry.pNext = nullptr;
        asGeometry.geometryType = VK_GEOMETRY_TYPE_INSTANCES_KHR;
        asGeometry.geometry.instances.sType =
            VK_STRUCTURE_TYPE_ACCELERATION_STRUCTURE_GEOMETRY_INSTANCES_DATA_KHR;
        asGeometry.geometry.instances.pNext = nullptr;
        asGeometry.geometry.instances.arrayOfPointers = VK_FALSE;
        asGeometry.geometry.instances.data.deviceAddress = gibAddress;
        asGeometry.flags = VK_GEOMETRY_OPAQUE_BIT_KHR;

        VkAccelerationStructureBuildGeometryInfoKHR asbgInfo;
        asbgInfo.sType = VK_STRUCTURE_TYPE_ACCELERATION_STRUCTURE_BUILD_GEOMETRY_INFO_KHR;
        asbgInfo.pNext = nullptr;
        asbgInfo.type = VK_ACCELERATION_STRUCTURE_TYPE_TOP_LEVEL_KHR;
        asbgInfo.flags = VK_BUILD_ACCELERATION_STRUCTURE_PREFER_FAST_TRACE_BIT_KHR |
                         VK_BUILD_ACCELERATION_STRUCTURE_ALLOW_COMPACTION_BIT_KHR;
        asbgInfo.mode = VK_BUILD_ACCELERATION_STRUCTURE_MODE_BUILD_KHR;
        asbgInfo.srcAccelerationStructure = VK_NULL_HANDLE;
        asbgInfo.dstAccelerationStructure = VK_NULL_HANDLE;
        asbgInfo.geometryCount = 1;
        asbgInfo.pGeometries = &asGeometry;
        asbgInfo.ppGeometries = nullptr;
        asbgInfo.scratchData = {};

        // Determine acceleration structure and scratch buffer sizes
        VkAccelerationStructureBuildSizesInfoKHR asbsInfo = {};
        asbsInfo.sType = VK_STRUCTURE_TYPE_ACCELERATION_STRUCTURE_BUILD_SIZES_INFO_KHR;

        rtx.vkGetAccelerationStructureBuildSizesKHR(
            dev,
            VkAccelerationStructureBuildTypeKHR::VK_ACCELERATION_STRUCTURE_BUILD_TYPE_DEVICE_KHR,
            &asbgInfo, &asbgInfo.geometryCount, &asbsInfo);

        // Create the necessary buffers
        // Ensure that the usage flag includes device locality
        VkMemoryAllocateFlagsInfo allocInfo = {};
        allocInfo.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_FLAGS_INFO;
        allocInfo.flags = VK_MEMORY_ALLOCATE_DEVICE_ADDRESS_BIT;

        createBuffer(ctx, asbsInfo.accelerationStructureSize,
                     VK_BUFFER_USAGE_ACCELERATION_STRUCTURE_STORAGE_BIT_KHR |
                         VK_BUFFER_USAGE_SHADER_DEVICE_ADDRESS_BIT,
                     VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT, tlasBuffer, tlasDeviceMemory, &allocInfo);

        VkBuffer scratchBuffer;
        VkDeviceMemory scratchDeviceMemory;
        createBuffer(ctx, asbsInfo.buildScratchSize,
                     VK_BUFFER_USAGE_ACCELERATION_STRUCTURE_BUILD_INPUT_READ_ONLY_BIT_KHR |
                         VK_BUFFER_USAGE_SHADER_DEVICE_ADDRESS_BIT,
                     VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT, scratchBuffer, scratchDeviceMemory,
                     &allocInfo);

        VkBufferDeviceAddressInfo scratchbdaInfo = {};
        scratchbdaInfo.sType = VK_STRUCTURE_TYPE_BUFFER_DEVICE_ADDRESS_INFO;
        scratchbdaInfo.buffer = scratchBuffer;

        asbgInfo.scratchData.deviceAddress = vkGetBufferDeviceAddress(dev, &scratchbdaInfo);

        VkAccelerationStructureCreateInfoKHR asci = {};
        asci.sType = VK_STRUCTURE_TYPE_ACCELERATION_STRUCTURE_CREATE_INFO_KHR;
        asci.buffer = tlasBuffer;
        asci.size = asbsInfo.accelerationStructureSize;
        asci.type = VK_ACCELERATION_STRUCTURE_TYPE_TOP_LEVEL_KHR;
        //asci.deviceAddress = VK_NULL_HANDLE;

        chkd(rtx.vkCreateAccelerationStructureKHR(dev, &asci, nullptr, &tlas));

        asbgInfo.dstAccelerationStructure = tlas;

        const auto asbrInfo = std::make_unique<VkAccelerationStructureBuildRangeInfoKHR>();
        asbrInfo->primitiveCount = 1;
        asbrInfo->primitiveOffset = 0;
        asbrInfo->firstVertex = 0;
        asbrInfo->transformOffset = 0;

        buildAS(ctx, asbgInfo, asbrInfo.get());

        vkDestroyBuffer(dev, scratchBuffer, nullptr);
        vkFreeMemory(dev, scratchDeviceMemory, nullptr);
    }

    void createImageView(cgvu::Context& ctx) {
        // Vulkan shortcuts
        VkDevice dev = ctx.handle();
        auto &devInfo = *(VulkanDeviceInfo *)ctx.getDeviceInfo();

        // Load functions entry points
        auto &rtx = RTXfuncs::obtain(RTXfuncTable, ctx);

        VkMemoryAllocateFlagsInfo afi{};
        afi.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_FLAGS_INFO;
        afi.flags = VK_MEMORY_ALLOCATE_DEVICE_ADDRESS_BIT;

        createImage(ctx, viewportWidth, viewportHeight, VK_FORMAT_B8G8R8A8_UNORM,
                    VK_IMAGE_TILING_OPTIMAL,
                    VK_IMAGE_USAGE_TRANSFER_SRC_BIT | VK_IMAGE_USAGE_STORAGE_BIT,
                    VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT, rayTraceImage, rayTraceImageMemory,
                    &afi);

        VkImageSubresourceRange imageSubresourceRange{};
        imageSubresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
        imageSubresourceRange.levelCount = 1;
        imageSubresourceRange.layerCount = 1;

        VkImageViewCreateInfo ivcInfo{};
        ivcInfo.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
        ivcInfo.viewType = VK_IMAGE_VIEW_TYPE_2D;
        ivcInfo.format = VK_FORMAT_B8G8R8A8_UNORM;
        ivcInfo.subresourceRange = imageSubresourceRange;
        ivcInfo.image = rayTraceImage;

        chkd(vkCreateImageView(dev, &ivcInfo, nullptr, &rayTraceImageView));

        VkImageMemoryBarrier imageMemoryBarrier = {};
        imageMemoryBarrier.sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER;
        imageMemoryBarrier.pNext = NULL;
        imageMemoryBarrier.oldLayout = VK_IMAGE_LAYOUT_UNDEFINED;
        imageMemoryBarrier.newLayout = VK_IMAGE_LAYOUT_GENERAL;
        imageMemoryBarrier.image = rayTraceImage;
        imageMemoryBarrier.subresourceRange = imageSubresourceRange;
        imageMemoryBarrier.srcAccessMask = 0;
        imageMemoryBarrier.dstAccessMask = 0;

        VkCommandBufferAllocateInfo bufferAllocateInfo = {};
        bufferAllocateInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
        bufferAllocateInfo.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
        bufferAllocateInfo.commandPool = devInfo.primaryComputeQF().pool_static;
        bufferAllocateInfo.commandBufferCount = 1;

        VkCommandBuffer commandBuffer;
        vkAllocateCommandBuffers(dev, &bufferAllocateInfo, &commandBuffer);

        VkCommandBufferBeginInfo commandBufferBeginInfo = {};
        commandBufferBeginInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
        commandBufferBeginInfo.flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT;

        vkBeginCommandBuffer(commandBuffer, &commandBufferBeginInfo);
        vkCmdPipelineBarrier(commandBuffer, VK_PIPELINE_STAGE_ALL_COMMANDS_BIT,
                             VK_PIPELINE_STAGE_ALL_COMMANDS_BIT, 0, 0, NULL, 0, NULL, 1,
                             &imageMemoryBarrier);
        vkEndCommandBuffer(commandBuffer);

        VkSubmitInfo submitInfo = {};
        submitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
        submitInfo.commandBufferCount = 1;
        submitInfo.pCommandBuffers = &commandBuffer;

        vkQueueSubmit(devInfo.getComputeQueue(), 1, &submitInfo, VK_NULL_HANDLE);
        vkQueueWaitIdle(devInfo.getComputeQueue());

        vkFreeCommandBuffers(dev, devInfo.primaryComputeQF().pool_static, 1, &commandBuffer);
    }

    void createDescriptorSets(cgvu::Context &ctx)
    {
        // Vulkan shortcuts
        VkDevice dev = ctx.handle();
        auto &devInfo = *(VulkanDeviceInfo *)ctx.getDeviceInfo();

        // Load functions entry points
        auto &rtx = RTXfuncs::obtain(RTXfuncTable, ctx);

        VkDescriptorPoolSize dpSizes[4] = {};
        dpSizes[0].type = VK_DESCRIPTOR_TYPE_ACCELERATION_STRUCTURE_KHR;
        dpSizes[0].descriptorCount = 1;

        // The raytraced image buffer
        dpSizes[1].type = VK_DESCRIPTOR_TYPE_STORAGE_IMAGE;
        dpSizes[1].descriptorCount = 1;

        // Camera
        dpSizes[2].type = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
        dpSizes[2].descriptorCount = 1;

        // The buffers for position, tangent, color, derivativeColor, and radius+derivative
        dpSizes[3].type = VK_DESCRIPTOR_TYPE_STORAGE_BUFFER;
        dpSizes[3].descriptorCount = 1;
        
        VkDescriptorPoolCreateInfo dpcInfo = {};
        dpcInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO;
        dpcInfo.poolSizeCount = 4;
        dpcInfo.pPoolSizes = dpSizes;
        dpcInfo.maxSets = 1;

        chkd(vkCreateDescriptorPool(dev, &dpcInfo, nullptr, &descriptorPool));

        {
            std::array<VkDescriptorSetLayoutBinding, 4> descriptorSetLayoutBindings{};
            descriptorSetLayoutBindings[0].binding = 0;
            descriptorSetLayoutBindings[0].descriptorCount = 1;
            descriptorSetLayoutBindings[0].descriptorType =
                VK_DESCRIPTOR_TYPE_ACCELERATION_STRUCTURE_KHR;
            descriptorSetLayoutBindings[0].pImmutableSamplers = nullptr;
            descriptorSetLayoutBindings[0].stageFlags = VK_SHADER_STAGE_RAYGEN_BIT_KHR |
                                                        VK_SHADER_STAGE_CLOSEST_HIT_BIT_KHR |
                                                        VK_SHADER_STAGE_INTERSECTION_BIT_KHR;

            descriptorSetLayoutBindings[1].binding = 1;
            descriptorSetLayoutBindings[1].descriptorCount = 1;
            descriptorSetLayoutBindings[1].descriptorType = VK_DESCRIPTOR_TYPE_STORAGE_IMAGE;
            descriptorSetLayoutBindings[1].pImmutableSamplers = nullptr;
            descriptorSetLayoutBindings[1].stageFlags =
                VK_SHADER_STAGE_RAYGEN_BIT_KHR | VK_SHADER_STAGE_CLOSEST_HIT_BIT_KHR;

            descriptorSetLayoutBindings[2].binding = 2;
            descriptorSetLayoutBindings[2].descriptorCount = 1;
            descriptorSetLayoutBindings[2].descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
            descriptorSetLayoutBindings[2].pImmutableSamplers = nullptr;
            descriptorSetLayoutBindings[2].stageFlags =
                VK_SHADER_STAGE_RAYGEN_BIT_KHR | VK_SHADER_STAGE_CLOSEST_HIT_BIT_KHR;

            // Node Data
            descriptorSetLayoutBindings[3].binding = 3;
            descriptorSetLayoutBindings[3].descriptorCount = 1;
            descriptorSetLayoutBindings[3].descriptorType = VK_DESCRIPTOR_TYPE_STORAGE_BUFFER;
            descriptorSetLayoutBindings[3].pImmutableSamplers = nullptr;
            descriptorSetLayoutBindings[3].stageFlags = VK_SHADER_STAGE_RAYGEN_BIT_KHR |
                                                        VK_SHADER_STAGE_CLOSEST_HIT_BIT_KHR |
                                                        VK_SHADER_STAGE_INTERSECTION_BIT_KHR;

            VkDescriptorSetLayoutCreateInfo descriptorSetLayoutCreateInfo = {};
            descriptorSetLayoutCreateInfo.sType =
                VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO;
            descriptorSetLayoutCreateInfo.bindingCount = descriptorSetLayoutBindings.size();
            descriptorSetLayoutCreateInfo.pBindings = descriptorSetLayoutBindings.data();

            chkd(vkCreateDescriptorSetLayout(dev, &descriptorSetLayoutCreateInfo, nullptr,
                                             &rtDescriptorSetLayout));

            VkDescriptorSetAllocateInfo descriptorSetAllocateInfo = {};
            descriptorSetAllocateInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO;
            descriptorSetAllocateInfo.descriptorPool = descriptorPool;
            descriptorSetAllocateInfo.descriptorSetCount = 1;
            descriptorSetAllocateInfo.pSetLayouts = &rtDescriptorSetLayout;

            chkd(vkAllocateDescriptorSets(dev, &descriptorSetAllocateInfo, &rtDescriptorSet));

            std::array<VkWriteDescriptorSet, 4> writeDescriptorSets;

            VkWriteDescriptorSetAccelerationStructureKHR descriptorSetAccelerationStructure = {};
            descriptorSetAccelerationStructure.sType =
                VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET_ACCELERATION_STRUCTURE_KHR;
            descriptorSetAccelerationStructure.pNext = nullptr;
            descriptorSetAccelerationStructure.accelerationStructureCount = 1;
            descriptorSetAccelerationStructure.pAccelerationStructures = &tlas;

            writeDescriptorSets[0].sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
            writeDescriptorSets[0].pNext = &descriptorSetAccelerationStructure;
            writeDescriptorSets[0].dstSet = rtDescriptorSet;
            writeDescriptorSets[0].dstBinding = 0;
            writeDescriptorSets[0].dstArrayElement = 0;
            writeDescriptorSets[0].descriptorCount = 1;
            writeDescriptorSets[0].descriptorType = VK_DESCRIPTOR_TYPE_ACCELERATION_STRUCTURE_KHR;
            writeDescriptorSets[0].pImageInfo = nullptr;
            writeDescriptorSets[0].pBufferInfo = nullptr;
            writeDescriptorSets[0].pTexelBufferView = nullptr;
           
            VkDescriptorImageInfo imageInfo{};
            imageInfo.sampler = VK_NULL_HANDLE; // TODO?
            imageInfo.imageView = rayTraceImageView;
            imageInfo.imageLayout = VK_IMAGE_LAYOUT_GENERAL;

            writeDescriptorSets[1].sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
            writeDescriptorSets[1].pNext = nullptr;
            writeDescriptorSets[1].dstSet = rtDescriptorSet;
            writeDescriptorSets[1].dstBinding = 1;
            writeDescriptorSets[1].dstArrayElement = 0;
            writeDescriptorSets[1].descriptorCount = 1;
            writeDescriptorSets[1].descriptorType = VK_DESCRIPTOR_TYPE_STORAGE_IMAGE;
            writeDescriptorSets[1].pImageInfo = &imageInfo;
            writeDescriptorSets[1].pBufferInfo = nullptr;
            writeDescriptorSets[1].pTexelBufferView = nullptr;

            VkDescriptorBufferInfo uniformBufferInfo{};
            uniformBufferInfo.buffer = shUniforms.handle();
            uniformBufferInfo.offset = 0;
            uniformBufferInfo.range = VK_WHOLE_SIZE;

            writeDescriptorSets[2].sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
            writeDescriptorSets[2].pNext = nullptr;
            writeDescriptorSets[2].dstSet = rtDescriptorSet;
            writeDescriptorSets[2].dstBinding = 2;
            writeDescriptorSets[2].dstArrayElement = 0;
            writeDescriptorSets[2].descriptorCount = 1;
            writeDescriptorSets[2].descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
            writeDescriptorSets[2].pImageInfo = nullptr;
            writeDescriptorSets[2].pBufferInfo = &uniformBufferInfo;
            writeDescriptorSets[2].pTexelBufferView = nullptr;

            VkDescriptorBufferInfo nodeBufferInfo{};
            nodeBufferInfo.buffer = nodeBuffer;
            nodeBufferInfo.offset = 0;
            nodeBufferInfo.range = VK_WHOLE_SIZE;

            writeDescriptorSets[3].sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
            writeDescriptorSets[3].pNext = nullptr;
            writeDescriptorSets[3].dstSet = rtDescriptorSet;
            writeDescriptorSets[3].dstBinding = 3;
            writeDescriptorSets[3].dstArrayElement = 0;
            writeDescriptorSets[3].descriptorCount = 1;
            writeDescriptorSets[3].descriptorType = VK_DESCRIPTOR_TYPE_STORAGE_BUFFER;
            writeDescriptorSets[3].pImageInfo = nullptr;
            writeDescriptorSets[3].pBufferInfo = &nodeBufferInfo;
            writeDescriptorSets[3].pTexelBufferView = nullptr;

            vkUpdateDescriptorSets(dev, writeDescriptorSets.size(), writeDescriptorSets.data(), 0,
                                   nullptr);
        }
    }

    void createRayTracePipeline(cgvu::Context &ctx)
    {
        // Vulkan shortcuts
        VkDevice dev = ctx.handle();
        auto &devInfo = *(VulkanDeviceInfo *)ctx.getDeviceInfo();

        // Load functions entry points
        auto &rtx = RTXfuncs::obtain(RTXfuncTable, ctx);

        std::ifstream rgen_ifs("./shader/hsplinetube_rt.rgen.spv", std::ios::binary);
        std::vector<char> rgen_file(std::istreambuf_iterator<char>(rgen_ifs), {});

        std::ifstream rint_ifs("./shader/hsplinetube_rt.rint.spv", std::ios::binary);
        std::vector<char> rint_file(std::istreambuf_iterator<char>(rint_ifs), {});

        std::ifstream rmiss_ifs("./shader/hsplinetube_rt.rmiss.spv", std::ios::binary);
        std::vector<char> rmiss_file(std::istreambuf_iterator<char>(rmiss_ifs), {});

        std::ifstream rchit_ifs("./shader/hsplinetube_rt.rchit.spv", std::ios::binary);
        std::vector<char> rchit_file(std::istreambuf_iterator<char>(rchit_ifs), {});

        VkShaderModuleCreateInfo smcInfo = {};
        smcInfo.sType = VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO;
        smcInfo.codeSize = rgen_file.size();
        smcInfo.pCode = reinterpret_cast<uint32_t *>(rgen_file.data());

        VkShaderModule rgenShaderModule;
        chkd(vkCreateShaderModule(dev, &smcInfo, nullptr, &rgenShaderModule));

        smcInfo.codeSize = rint_file.size();
        smcInfo.pCode = reinterpret_cast<uint32_t *>(rint_file.data());
        VkShaderModule rintShaderModule;
        chkd(vkCreateShaderModule(dev, &smcInfo, nullptr, &rintShaderModule));

        smcInfo.codeSize = rmiss_file.size();
        smcInfo.pCode = reinterpret_cast<uint32_t *>(rmiss_file.data());
        VkShaderModule rmissShaderModule;
        chkd(vkCreateShaderModule(dev, &smcInfo, nullptr, &rmissShaderModule));

        smcInfo.codeSize = rchit_file.size();
        smcInfo.pCode = reinterpret_cast<uint32_t *>(rchit_file.data());
        VkShaderModule rchitShaderModule;
        chkd(vkCreateShaderModule(dev, &smcInfo, nullptr, &rchitShaderModule));

        std::array<VkPipelineShaderStageCreateInfo, 4> shaderStages{};
        shaderStages[0].sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
        shaderStages[0].stage = VK_SHADER_STAGE_RAYGEN_BIT_KHR;
        shaderStages[0].module = rgenShaderModule;
        shaderStages[0].pName = "main";

        shaderStages[1].sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
        shaderStages[1].stage = VK_SHADER_STAGE_INTERSECTION_BIT_KHR;
        shaderStages[1].module = rintShaderModule;
        shaderStages[1].pName = "main";

        shaderStages[2].sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
        shaderStages[2].stage = VK_SHADER_STAGE_MISS_BIT_KHR;
        shaderStages[2].module = rmissShaderModule;
        shaderStages[2].pName = "main";

        shaderStages[3].sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
        shaderStages[3].stage = VK_SHADER_STAGE_CLOSEST_HIT_BIT_KHR;
        shaderStages[3].module = rchitShaderModule;
        shaderStages[3].pName = "main";

        // -1 because closest hit and intersection need to be in the same group
        // if they are procedural???
        std::array<VkRayTracingShaderGroupCreateInfoKHR, shaderStages.size() - 1> sgcInfos{};
        sgcInfos[0].sType = VK_STRUCTURE_TYPE_RAY_TRACING_SHADER_GROUP_CREATE_INFO_KHR;
        sgcInfos[0].type = VK_RAY_TRACING_SHADER_GROUP_TYPE_GENERAL_KHR;
        sgcInfos[0].generalShader = 0;
        sgcInfos[0].closestHitShader = VK_SHADER_UNUSED_KHR;
        sgcInfos[0].anyHitShader = VK_SHADER_UNUSED_KHR;
        sgcInfos[0].intersectionShader = VK_SHADER_UNUSED_KHR;

        sgcInfos[1].sType = VK_STRUCTURE_TYPE_RAY_TRACING_SHADER_GROUP_CREATE_INFO_KHR;
        sgcInfos[1].type = VK_RAY_TRACING_SHADER_GROUP_TYPE_PROCEDURAL_HIT_GROUP_KHR;
        sgcInfos[1].generalShader = VK_SHADER_UNUSED_KHR;
        sgcInfos[1].closestHitShader = 3;
        sgcInfos[1].anyHitShader = VK_SHADER_UNUSED_KHR;
        sgcInfos[1].intersectionShader = 1;

        sgcInfos[2].sType = VK_STRUCTURE_TYPE_RAY_TRACING_SHADER_GROUP_CREATE_INFO_KHR;
        sgcInfos[2].type = VK_RAY_TRACING_SHADER_GROUP_TYPE_GENERAL_KHR;
        sgcInfos[2].generalShader = 2;
        sgcInfos[2].closestHitShader = VK_SHADER_UNUSED_KHR;
        sgcInfos[2].anyHitShader = VK_SHADER_UNUSED_KHR;
        sgcInfos[2].intersectionShader = VK_SHADER_UNUSED_KHR;

        VkPipelineLayoutCreateInfo plcInfo = {};
        plcInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO;
        plcInfo.setLayoutCount = 1;
        plcInfo.pSetLayouts = &rtDescriptorSetLayout;
        plcInfo.pPushConstantRanges = 0;

        chkd(vkCreatePipelineLayout(dev, &plcInfo, nullptr, &rtPipelineLayout));

        VkPipelineLibraryCreateInfoKHR pllcInfo{};
        pllcInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_LIBRARY_CREATE_INFO_KHR;

        VkRayTracingPipelineCreateInfoKHR rtpcInfo{};
        rtpcInfo.sType = VK_STRUCTURE_TYPE_RAY_TRACING_PIPELINE_CREATE_INFO_KHR;
        rtpcInfo.stageCount = shaderStages.size();
        rtpcInfo.pStages = shaderStages.data();
        rtpcInfo.groupCount = sgcInfos.size();
        rtpcInfo.pGroups = sgcInfos.data();
        rtpcInfo.maxPipelineRayRecursionDepth = 1;
        rtpcInfo.pLibraryInfo = &pllcInfo;
        rtpcInfo.layout = rtPipelineLayout;
        rtpcInfo.basePipelineHandle = VK_NULL_HANDLE;
        rtpcInfo.basePipelineIndex = -1;

        chkd(rtx.vkCreateRayTracingPipelinesKHR(dev, VK_NULL_HANDLE, VK_NULL_HANDLE, 1, &rtpcInfo,
                                                nullptr, &rtPipeline));
    }

    void createShaderBindingTable(cgvu::Context &ctx) {
        // Vulkan shortcuts
        VkDevice dev = ctx.handle();
        auto &devInfo = *(VulkanDeviceInfo *)ctx.getDeviceInfo();

        // Load functions entry points
        auto &rtx = RTXfuncs::obtain(RTXfuncTable, ctx);

        VkPhysicalDeviceRayTracingPipelinePropertiesKHR rayTracingProperties = {};
        rayTracingProperties.sType =
            VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_RAY_TRACING_PIPELINE_PROPERTIES_KHR;

        VkPhysicalDeviceProperties2 properties = {};
        properties.sType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_PROPERTIES_2;
        properties.pNext = &rayTracingProperties;

        vkGetPhysicalDeviceProperties2(ctx.handlePhy(), &properties);

        constexpr int num_shaders = 3;
        VkDeviceSize shaderBindingTableSize = rayTracingProperties.shaderGroupHandleSize * num_shaders;

        VkMemoryAllocateFlagsInfo ai{};
        ai.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_FLAGS_INFO;
        ai.flags = VK_MEMORY_ALLOCATE_DEVICE_ADDRESS_BIT;

        createBuffer(ctx, shaderBindingTableSize,
                     VK_BUFFER_USAGE_SHADER_BINDING_TABLE_BIT_KHR |
                         VK_BUFFER_USAGE_SHADER_DEVICE_ADDRESS_BIT,
                     VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT, rtShaderBindingTableBuffer,
                     rtShaderBindingTableDeviceMemory, &ai);

        //void *shaderHandleStorage = (void *)malloc(sizeof(uint8_t) * shaderBindingTableSize);
        auto const shaderHandleStorage =
            std::make_unique<std::byte[]>(sizeof(std::byte) * shaderBindingTableSize);
        chkd(rtx.vkGetRayTracingShaderGroupHandlesKHR(dev, rtPipeline, 0, num_shaders,
                                                      shaderBindingTableSize,
                                                      shaderHandleStorage.get()));

        std::byte *data;
        vkMapMemory(dev, rtShaderBindingTableDeviceMemory, 0, shaderBindingTableSize, 0,
                    reinterpret_cast<void **>(&data));
        for (int x = 0; x < num_shaders; x++)
        {
            memcpy(data,
                   shaderHandleStorage.get() + x * rayTracingProperties.shaderGroupHandleSize,
                   rayTracingProperties.shaderGroupHandleSize);
            data += rayTracingProperties.shaderGroupBaseAlignment;
        }
        vkUnmapMemory(dev, rtShaderBindingTableDeviceMemory);
    }

    void createRTCommandBuffer(cgvu::Context& ctx) {
        // Vulkan shortcuts
        VkDevice dev = ctx.handle();
        auto &devInfo = *(VulkanDeviceInfo *)ctx.getDeviceInfo();
        auto commandPool = devInfo.primaryGraphicsQF().pool_static;

        // Load functions entry points
        auto &rtx = RTXfuncs::obtain(RTXfuncTable, ctx);

        VkPhysicalDeviceRayTracingPipelinePropertiesKHR rayTracingProperties = {};
        rayTracingProperties.sType =
            VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_RAY_TRACING_PIPELINE_PROPERTIES_KHR;

        VkPhysicalDeviceProperties2 properties = {};
        properties.sType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_PROPERTIES_2;
        properties.pNext = &rayTracingProperties;

        vkGetPhysicalDeviceProperties2(ctx.handlePhy(), &properties);

        cmdRaytrace.resize(ctx.getSwapchainLength(), {});

        VkCommandBufferAllocateInfo commandBufferAllocateInfo = {};
        commandBufferAllocateInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
        commandBufferAllocateInfo.commandPool = commandPool;
        commandBufferAllocateInfo.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
        commandBufferAllocateInfo.commandBufferCount = ctx.getSwapchainLength();

        chkd(vkAllocateCommandBuffers(dev, &commandBufferAllocateInfo, cmdRaytrace.data()));

        for (int x = 0; x < ctx.getSwapchainLength(); x++)
        {
            VkCommandBufferBeginInfo commandBufferBeginCreateInfo = {};
            commandBufferBeginCreateInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;

            VkDeviceSize progSize = rayTracingProperties.shaderGroupBaseAlignment;
            VkDeviceSize sbtSize = progSize * (VkDeviceSize)3;
            VkDeviceSize rayGenOffset = 0u * progSize;
            VkDeviceSize hitGroupOffset = 1u * progSize;
            VkDeviceSize missOffset = 2u * progSize;

            VkBufferDeviceAddressInfo shaderBindingTableBufferDeviceAddressInfo = {};
            shaderBindingTableBufferDeviceAddressInfo.sType =
                VK_STRUCTURE_TYPE_BUFFER_DEVICE_ADDRESS_INFO;
            shaderBindingTableBufferDeviceAddressInfo.buffer = rtShaderBindingTableBuffer;

            VkDeviceAddress shaderBindingTableBufferDeviceAddress =
                vkGetBufferDeviceAddress(dev, &shaderBindingTableBufferDeviceAddressInfo);

            VkStridedDeviceAddressRegionKHR rgenShaderBindingTable{};
            rgenShaderBindingTable.deviceAddress =
                shaderBindingTableBufferDeviceAddress + 0u * progSize;
            rgenShaderBindingTable.stride = sbtSize;
            rgenShaderBindingTable.size = sbtSize * 1;

            VkStridedDeviceAddressRegionKHR rmissShaderBindingTable{};
            rmissShaderBindingTable.deviceAddress =
                shaderBindingTableBufferDeviceAddress + 1u * progSize;
            rmissShaderBindingTable.stride = progSize;
            rmissShaderBindingTable.size = sbtSize * 2;

            VkStridedDeviceAddressRegionKHR rchitShaderBindingTable{};
            rchitShaderBindingTable.deviceAddress =
                shaderBindingTableBufferDeviceAddress + 3u * progSize;
            rchitShaderBindingTable.stride = progSize;
            rchitShaderBindingTable.size = sbtSize * 1;

            const VkStridedDeviceAddressRegionKHR callableShaderBindingTable = {};

            VkImageSubresourceRange subresourceRange = {};
            subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
            subresourceRange.baseMipLevel = 0;
            subresourceRange.levelCount = 1;
            subresourceRange.baseArrayLayer = 0;
            subresourceRange.layerCount = 1;

            chkd(vkBeginCommandBuffer(cmdRaytrace[x], &commandBufferBeginCreateInfo));

            vkCmdBindPipeline(cmdRaytrace[x], VK_PIPELINE_BIND_POINT_RAY_TRACING_KHR, rtPipeline);
            vkCmdBindDescriptorSets(cmdRaytrace[x], VK_PIPELINE_BIND_POINT_RAY_TRACING_KHR,
                                    rtPipelineLayout, 0, 1, &rtDescriptorSet, 0, 0);

            rtx.vkCmdTraceRaysKHR(cmdRaytrace[x], &rgenShaderBindingTable, &rmissShaderBindingTable,
                                  &rchitShaderBindingTable, &callableShaderBindingTable, 800, 600,
                                  1);

            {
                VkImageMemoryBarrier imageMemoryBarrier = {};
                imageMemoryBarrier.sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER;
                imageMemoryBarrier.pNext = NULL;
                imageMemoryBarrier.oldLayout = VK_IMAGE_LAYOUT_GENERAL;
                imageMemoryBarrier.newLayout = VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL;
                imageMemoryBarrier.image = rayTraceImage;
                imageMemoryBarrier.subresourceRange = subresourceRange;
                imageMemoryBarrier.srcAccessMask = 0;
                imageMemoryBarrier.dstAccessMask = VK_ACCESS_TRANSFER_READ_BIT;

                vkCmdPipelineBarrier(cmdRaytrace[x], VK_PIPELINE_STAGE_ALL_COMMANDS_BIT,
                                     VK_PIPELINE_STAGE_ALL_COMMANDS_BIT, 0, 0, NULL, 0, NULL, 1,
                                     &imageMemoryBarrier);
            }

            {
                VkImageMemoryBarrier imageMemoryBarrier = {};
                imageMemoryBarrier.sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER;
                imageMemoryBarrier.pNext = NULL;
                imageMemoryBarrier.oldLayout = VK_IMAGE_LAYOUT_UNDEFINED;
                imageMemoryBarrier.newLayout = VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL;
                imageMemoryBarrier.image = ctx.getSwapImage(x);
                imageMemoryBarrier.subresourceRange = subresourceRange;
                imageMemoryBarrier.srcAccessMask = 0;
                imageMemoryBarrier.dstAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
                imageMemoryBarrier.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
                imageMemoryBarrier.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;

                vkCmdPipelineBarrier(cmdRaytrace[x], VK_PIPELINE_STAGE_ALL_COMMANDS_BIT,
                                     VK_PIPELINE_STAGE_ALL_COMMANDS_BIT, 0, 0, NULL, 0, NULL, 1,
                                     &imageMemoryBarrier);
            }

            {
                VkImageSubresourceLayers subresourceLayers = {};
                subresourceLayers.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
                subresourceLayers.mipLevel = 0;
                subresourceLayers.baseArrayLayer = 0;
                subresourceLayers.layerCount = 1;

                VkOffset3D offset = {};
                offset.x = 0;
                offset.y = 0;
                offset.z = 0;

                // TODO This should be resize independent ...
                VkExtent3D extent = {};
                extent.width = viewportWidth;
                extent.height = viewportHeight;
                extent.depth = 1;

                VkImageCopy imageCopy = {};
                imageCopy.srcSubresource = subresourceLayers;
                imageCopy.srcOffset = offset;
                imageCopy.dstSubresource = subresourceLayers;
                imageCopy.dstOffset = offset;
                imageCopy.extent = extent;

                vkCmdCopyImage(cmdRaytrace[x], rayTraceImage, VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL,
                               ctx.getSwapImage(x), VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, 1,
                               &imageCopy);
            }

            {
                VkImageSubresourceRange subresourceRange = {};
                subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
                subresourceRange.baseMipLevel = 0;
                subresourceRange.levelCount = 1;
                subresourceRange.baseArrayLayer = 0;
                subresourceRange.layerCount = 1;

                VkImageMemoryBarrier imageMemoryBarrier = {};
                imageMemoryBarrier.sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER;
                imageMemoryBarrier.pNext = NULL;
                imageMemoryBarrier.oldLayout = VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL;
                imageMemoryBarrier.newLayout = VK_IMAGE_LAYOUT_GENERAL;
                imageMemoryBarrier.image = rayTraceImage;
                imageMemoryBarrier.subresourceRange = subresourceRange;
                imageMemoryBarrier.srcAccessMask = VK_ACCESS_TRANSFER_READ_BIT;
                imageMemoryBarrier.dstAccessMask = 0;
                imageMemoryBarrier.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
                imageMemoryBarrier.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;

                vkCmdPipelineBarrier(cmdRaytrace[x], VK_PIPELINE_STAGE_ALL_COMMANDS_BIT,
                                     VK_PIPELINE_STAGE_ALL_COMMANDS_BIT, 0, 0, NULL, 0, NULL, 1,
                                     &imageMemoryBarrier);
            }

            {
                VkImageSubresourceRange subresourceRange = {};
                subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
                subresourceRange.baseMipLevel = 0;
                subresourceRange.levelCount = 1;
                subresourceRange.baseArrayLayer = 0;
                subresourceRange.layerCount = 1;

                VkImageMemoryBarrier imageMemoryBarrier = {};
                imageMemoryBarrier.sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER;
                imageMemoryBarrier.pNext = NULL;
                imageMemoryBarrier.oldLayout = VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL;
                imageMemoryBarrier.newLayout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;
                imageMemoryBarrier.image = ctx.getSwapImage(x);
                imageMemoryBarrier.subresourceRange = subresourceRange;
                imageMemoryBarrier.srcAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
                imageMemoryBarrier.dstAccessMask = 0;
                imageMemoryBarrier.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
                imageMemoryBarrier.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;

                vkCmdPipelineBarrier(cmdRaytrace[x], VK_PIPELINE_STAGE_ALL_COMMANDS_BIT,
                                     VK_PIPELINE_STAGE_ALL_COMMANDS_BIT, 0, 0, NULL, 0, NULL, 1,
                                     &imageMemoryBarrier);
            }

            chkd(vkEndCommandBuffer(cmdRaytrace[x]));
        }
    }

    void drawRayTraceImage(cgvu::Context &ctx, uint32_t swapchainImageID)
    {
        // Vulkan shortcuts
        VkDevice dev = ctx.handle();
        auto &devInfo = *(VulkanDeviceInfo *)ctx.getDeviceInfo();
        auto commandPool = devInfo.primaryGraphicsQF().pool_static;

        VkSubmitInfo submitInfo{};
        submitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
        submitInfo.commandBufferCount = 1;
        submitInfo.pCommandBuffers = &cmdRaytrace[swapchainImageID];

        chkd(vkQueueSubmit(devInfo.getGraphicsQueue(), 1, &submitInfo, VK_NULL_HANDLE));

        ctx.waitIdle();
    }
};

//////
//
// Application entry point
//

int main(int argc, char *argv[])
{
    // Create application object
    RTXtubesApp app;

    // Hand off control flow
    return app.run(argc, argv);
}

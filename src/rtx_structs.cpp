#include "rtx_structs.h"

#include <cgvu/vk/vulkan_internals.h>

#include <cassert>

uint64_t getBufferDeviceAddress(cgvu::Context &ctx, VkBuffer buffer)
{
    VkBufferDeviceAddressInfo bdai{};
    bdai.sType = VK_STRUCTURE_TYPE_BUFFER_DEVICE_ADDRESS_INFO;
    bdai.buffer = buffer;
    return vkGetBufferDeviceAddress(ctx.handle(), &bdai);
}

RayTracingScratchBuffer createScratchBuffer(cgvu::Context &ctx, VkDeviceSize size)
{
    auto device = ctx.handle();
    auto devInfo = *(VulkanDeviceInfo *)ctx.getDeviceInfo();

    RayTracingScratchBuffer scratchBuffer{};

    VkBufferCreateInfo bufferCreateInfo{};
    bufferCreateInfo.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
    bufferCreateInfo.size = size;
    bufferCreateInfo.usage =
        VK_BUFFER_USAGE_STORAGE_BUFFER_BIT | VK_BUFFER_USAGE_SHADER_DEVICE_ADDRESS_BIT;
    auto result = vkCreateBuffer(device, &bufferCreateInfo, nullptr, &scratchBuffer.handle);
    assert(result == VK_SUCCESS);

    VkMemoryRequirements memoryRequirements{};
    vkGetBufferMemoryRequirements(device, scratchBuffer.handle, &memoryRequirements);

    const auto memoryTypeIndex =
        devInfo.findMemoryType(memoryRequirements, VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT);

    VkMemoryAllocateFlagsInfo memoryAllocateFlagsInfo{};
    memoryAllocateFlagsInfo.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_FLAGS_INFO;
    memoryAllocateFlagsInfo.flags = VK_MEMORY_ALLOCATE_DEVICE_ADDRESS_BIT_KHR;

    VkMemoryAllocateInfo memoryAllocateInfo = {};
    memoryAllocateInfo.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;
    memoryAllocateInfo.pNext = &memoryAllocateFlagsInfo;
    memoryAllocateInfo.allocationSize = memoryRequirements.size;
    memoryAllocateInfo.memoryTypeIndex = memoryTypeIndex;
    result = vkAllocateMemory(device, &memoryAllocateInfo, nullptr, &scratchBuffer.memory);
    assert(VK_SUCCESS == result);
    result = vkBindBufferMemory(device, scratchBuffer.handle, scratchBuffer.memory, 0);
    assert(VK_SUCCESS == result);

    VkBufferDeviceAddressInfoKHR bufferDeviceAddressInfo{};
    bufferDeviceAddressInfo.sType = VK_STRUCTURE_TYPE_BUFFER_DEVICE_ADDRESS_INFO;
    bufferDeviceAddressInfo.buffer = scratchBuffer.handle;
    scratchBuffer.deviceAddress = vkGetBufferDeviceAddress(device, &bufferDeviceAddressInfo);

    return scratchBuffer;
}

void deleteScratchBuffer(cgvu::Context &ctx, RayTracingScratchBuffer &scratchBuffer)
{
    auto device = ctx.handle();

    if (scratchBuffer.memory != VK_NULL_HANDLE)
    {
        vkFreeMemory(device, scratchBuffer.memory, nullptr);
    }
    if (scratchBuffer.handle != VK_NULL_HANDLE)
    {
        vkDestroyBuffer(device, scratchBuffer.handle, nullptr);
    }
}

void createAccelerationStructureBuffer(cgvu::Context &ctx, AccelerationStructure &as,
                                       VkAccelerationStructureBuildSizesInfoKHR buildSizeInfo)
{
    auto device = ctx.handle();
    auto devInfo = *(VulkanDeviceInfo *)ctx.getDeviceInfo();

    VkBufferCreateInfo bufferCreateInfo{};
    bufferCreateInfo.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
    bufferCreateInfo.size = buildSizeInfo.accelerationStructureSize;
    bufferCreateInfo.usage = VK_BUFFER_USAGE_ACCELERATION_STRUCTURE_STORAGE_BIT_KHR |
                             VK_BUFFER_USAGE_SHADER_DEVICE_ADDRESS_BIT;
    auto result = vkCreateBuffer(device, &bufferCreateInfo, nullptr, &as.buffer);
    assert(VK_SUCCESS == result);

    VkMemoryRequirements memoryRequirements{};
    vkGetBufferMemoryRequirements(device, as.buffer, &memoryRequirements);

    VkMemoryAllocateFlagsInfo memoryAllocateFlagsInfo{};
    memoryAllocateFlagsInfo.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_FLAGS_INFO;
    memoryAllocateFlagsInfo.flags = VK_MEMORY_ALLOCATE_DEVICE_ADDRESS_BIT_KHR;

    const auto memoryTypeIndex =
        devInfo.findMemoryType(memoryRequirements, VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT);

    VkMemoryAllocateInfo memoryAllocateInfo{};
    memoryAllocateInfo.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;
    memoryAllocateInfo.pNext = &memoryAllocateFlagsInfo;
    memoryAllocateInfo.allocationSize = memoryRequirements.size;
    memoryAllocateInfo.memoryTypeIndex = memoryTypeIndex;
    result = vkAllocateMemory(device, &memoryAllocateInfo, nullptr, &as.memory);
    assert(VK_SUCCESS == result);
    result = vkBindBufferMemory(device, as.buffer, as.memory, 0);
    assert(VK_SUCCESS == result);
}

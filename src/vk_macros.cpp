#include "vk_macros.h"

#include <cgvu/vk/vulkan_internals.h>

void chkd(VkResult result)
{
    if (VK_SUCCESS != result)
    {
        throw cgvu::err::VulkanException(result, CGVU_LOGMSG("VkResult Check", ""));
    }
}

void createBuffer(cgvu::Context &ctx, VkDeviceSize size, VkBufferUsageFlags usageFlags,
                  VkMemoryPropertyFlags propertyFlags, VkBuffer &buffer,
                  VkDeviceMemory &bufferMemory, VkMemoryAllocateFlagsInfo *allocateFlags)
{
    VkDevice dev = ctx.handle();
    auto &devInfo = *(VulkanDeviceInfo *)ctx.getDeviceInfo();

    VkBufferCreateInfo bufferCreateInfo = {};
    bufferCreateInfo.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
    bufferCreateInfo.size = size;
    bufferCreateInfo.usage = usageFlags;
    bufferCreateInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;

    chkd(vkCreateBuffer(dev, &bufferCreateInfo, NULL, &buffer));

    VkMemoryRequirements memoryRequirements;
    vkGetBufferMemoryRequirements(dev, buffer, &memoryRequirements);

    VkMemoryAllocateInfo memoryAllocateInfo = {};
    memoryAllocateInfo.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;
    memoryAllocateInfo.pNext = allocateFlags;
    memoryAllocateInfo.allocationSize = memoryRequirements.size;

    memoryAllocateInfo.memoryTypeIndex = devInfo.findMemoryType(memoryRequirements, propertyFlags);

    chkd(vkAllocateMemory(dev, &memoryAllocateInfo, NULL, &bufferMemory));

    vkBindBufferMemory(dev, buffer, bufferMemory, 0);
}

void copyBuffer(cgvu::Context &ctx, VkBuffer& srcBuffer, VkBuffer& dstBuffer, VkDeviceSize size)
{
    // Vulkan shortcuts
    VkDevice dev = ctx.handle();
    auto &devInfo = *(VulkanDeviceInfo *)ctx.getDeviceInfo();
    auto commandPool = devInfo.primaryGraphicsQF().pool_static;

    VkCommandBufferAllocateInfo bufferAllocateInfo = {};
    bufferAllocateInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
    bufferAllocateInfo.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
    bufferAllocateInfo.commandPool = commandPool;
    bufferAllocateInfo.commandBufferCount = 1;

    VkCommandBuffer commandBuffer;
    vkAllocateCommandBuffers(dev, &bufferAllocateInfo, &commandBuffer);

    VkCommandBufferBeginInfo commandBufferBeginInfo = {};
    commandBufferBeginInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
    commandBufferBeginInfo.flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT;

    vkBeginCommandBuffer(commandBuffer, &commandBufferBeginInfo);
    VkBufferCopy bufferCopy = {};
    bufferCopy.size = size;
    vkCmdCopyBuffer(commandBuffer, srcBuffer, dstBuffer, 1, &bufferCopy);
    vkEndCommandBuffer(commandBuffer);

    VkSubmitInfo submitInfo = {};
    submitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
    submitInfo.commandBufferCount = 1;
    submitInfo.pCommandBuffers = &commandBuffer;

    vkQueueSubmit(devInfo.getGraphicsQueue(), 1, &submitInfo, VK_NULL_HANDLE);
    vkQueueWaitIdle(devInfo.getGraphicsQueue());

    vkFreeCommandBuffers(dev, commandPool, 1, &commandBuffer);
}

void createImage(cgvu::Context &ctx, uint32_t width, uint32_t height, VkFormat format,
                 VkImageTiling tiling, VkImageUsageFlags usageFlags,
                 VkMemoryPropertyFlags propertyFlags, VkImage &image, VkDeviceMemory &imageMemory,
                 VkMemoryAllocateFlagsInfo *allocateFlags)
{
    // Vulkan shortcuts
    VkDevice dev = ctx.handle();
    auto &devInfo = *(VulkanDeviceInfo *)ctx.getDeviceInfo();

    VkImageCreateInfo imageCreateInfo = {};
    imageCreateInfo.sType = VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO;
    imageCreateInfo.imageType = VK_IMAGE_TYPE_2D;
    imageCreateInfo.extent.width = width;
    imageCreateInfo.extent.height = height;
    imageCreateInfo.extent.depth = 1;
    imageCreateInfo.mipLevels = 1;
    imageCreateInfo.arrayLayers = 1;
    imageCreateInfo.format = format;
    imageCreateInfo.tiling = tiling;
    imageCreateInfo.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
    imageCreateInfo.usage = usageFlags;
    imageCreateInfo.samples = VK_SAMPLE_COUNT_1_BIT;
    imageCreateInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;

    chkd(vkCreateImage(dev, &imageCreateInfo, nullptr, &image));

    VkMemoryRequirements memoryRequirements;
    vkGetImageMemoryRequirements(dev, image, &memoryRequirements);

    VkMemoryAllocateInfo memoryAllocateInfo = {};
    memoryAllocateInfo.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;
    memoryAllocateInfo.pNext = allocateFlags;
    memoryAllocateInfo.allocationSize = memoryRequirements.size;

    memoryAllocateInfo.memoryTypeIndex = devInfo.findMemoryType(memoryRequirements, propertyFlags);

    chkd(vkAllocateMemory(dev, &memoryAllocateInfo, nullptr, &imageMemory));

    vkBindImageMemory(dev, image, imageMemory, 0);
}

#ifndef __BEZDAT_H__
#define __BEZDAT_H__


//////
//
// Includes
//

// C++ STL
#include <iostream>

// GLM library
#include <glm/glm.hpp>

#include "hspline.h"


//////
//
// Function definitions
//

/** @brief Pure placeholder. Does nothing. */
inline void herpDiDerp (void) {}



//////
//
// Classes
//

/** @brief Provides read and write capabilites for Hermite splines in .bezdat format. */
template <class FltType>
class BezdatHandler
{

public:

	////
	// Exported types

	/** @brief Real number type. */
	typedef FltType Real;

	/** @brief 2D-Vector type. */
	typedef glm::vec<2, Real> Vec2;

	/** @brief 3D-Vector type. */
	typedef glm::vec<3, Real> Vec3;

	/** @brief 4D-Vector type. */
	typedef glm::vec<4, Real> Vec4;

	/** @brief 4x4 Matrix type. */
	typedef glm::mat<4, 4, Real> Mat4;


private:

	////
	// Data members

	/** @brief Implementation handle. */
	void *pimpl;


	////
	// Object construction / destruction

	/** @brief The default constructor. */
	BezdatHandler();


public:

	////
	// Object construction / destruction

	/** @brief Virtual base destructor. Causes vtable creation. */
	virtual ~BezdatHandler();


	////
	// Methods

	/** @brief References the singleton instance of the handler. */
	static const BezdatHandler& obtainRef (void);

	/** @brief References the singleton instance of the handler as a pointer. */
	static const BezdatHandler* obtainPtr (void);

	/**
	 * @brief
	 *		Parse the given stream containing the .bezdat file contents and output the
	 *		resulting Hermite spline.
	 */
	HermiteSpline<Real> read (std::istream &contents) const;

	/**
	 * @brief
	 *		Write the contents of a .bezdat file according to the provided Hermite spline
	 *		into the given stream.
	 */
	void write (std::ostream &contents, const HermiteSpline<Real> &spline) const;
};


#endif // ifndef __BEZDAT_H__

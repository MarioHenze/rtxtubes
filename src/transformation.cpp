#include "transformation.h"

#include <algorithm>
#include <functional>

#include "curveutils.h"

namespace {
	using namespace glm;
	
	// Componentwise and
	bvec2 and (bvec2 a, bvec2 b) { return bvec2(a.x && b.x, a.y && b.y); }

	// Componentwise and
	bvec3 and (bvec3 a, bvec3 b) {
		return bvec3(a.x && b.x, a.y && b.y, a.z && b.z);
	}

	// Componentwise and
	bvec4 and (bvec4 a, bvec4 b) {
		return bvec4(a.x && b.x, a.y && b.y, a.z && b.z, a.w && b.w);
	}

	// Checks if a given value is inside the given interval
	bool isBetween(float t, float tmin, float tmax) {
		return t >= tmin && t <= tmax;
	}

	// Checks if a given value is between 0 and 1
	bool isBetween01(float t) { return t >= 0. && t <= 1.; }

	bvec2 isBetween01(vec2 t) {
		return and(greaterThanEqual(t, vec2(0.)), lessThanEqual(t, vec2(1.)));
	}

	bvec3 isBetween01(vec3 t) {
		return and(greaterThanEqual(t, vec3(0.)), lessThanEqual(t, vec3(1.)));
	}

	bvec4 isBetween01(vec4 t) {
		return and(greaterThanEqual(t, vec4(0.)), lessThanEqual(t, vec4(1.)));
	}

	// Checks if a given value is outside the given interval
	bool isOutside(float t, float tmin, float tmax) {
		return isnan(t) || isnan(tmin) || isnan(tmax) || t < tmin || t > tmax;
	}

	// Checks if a given value is outside of 0 and 1
	bool isOutside01(float t) { return isnan(t) || t < 0. || t > 1.; }

	// Returns an orthogonal vector to the given v
	// Always works if the input is non-zero.
	// Doesn�t require the input to be normalised.
	// Doesn�t normalise the output. */
	glm::vec3 orthogonal(glm::vec3 v) {
		return abs(v.x) > abs(v.z) ? glm::vec3(-v.y, v.x, 0.0) : glm::vec3(0.0, -v.z, v.y);
	}

	float getComponentExtrema(QuadraticCurve1<float> component_curve, std::function<float(float, float)> comparison)
	{
		// component_curve contains the spline component in monomial form

		// The quadratic monomial has the form
		// ax^2 + bx + c
		const float a = component_curve[0];
		const float b = component_curve[1];
		const float c = component_curve[2];

		// Derivation 2ax + b = 0 is spline extrema
		// ==> x = -b / 2a
		const float extremal_t = -b / (2 * a);
		const bool t_invalid = isnan(extremal_t) || isOutside01(extremal_t);
		const float extremal_qt =
			t_invalid ? _posInf<float> : evalMonomial(component_curve, extremal_t);

		// Evaluate on spline start and end
		const float start = evalMonomial(component_curve, 0.f);
		const float end = evalMonomial(component_curve, 1.f);

		return comparison(comparison(start, end), extremal_qt);
	}

	float getComponentMinimum(glm::vec3 component_curve)
	{
		return getComponentExtrema(component_curve, [](float a, float b) { return std::min(a, b); });
	}

	float getComponentMaximum(glm::vec3 component_curve)
	{
		return getComponentExtrema(component_curve, [](float a, float b) { return std::max(a, b); });
	}
}

vec3 getCurveMinimum(mat3 curve)
{
	return { getComponentMinimum(curve[0]), getComponentMinimum(curve[1]), getComponentMinimum(curve[2]) };
}

vec3 getCurveMaximum(mat3 curve)
{
	return { getComponentMaximum(curve[0]), getComponentMaximum(curve[1]), getComponentMaximum(curve[2]) };
}

mat3 findBasis(mat3 bezier_spline)
{
	using namespace glm;
	
	const vec3 x_hat = normalize(bezier_spline[2] - bezier_spline[0]);
	const vec3 y_hat = normalize(orthogonal(x_hat));
	const vec3 z_hat = cross(x_hat, y_hat);

	return mat3(x_hat, y_hat, z_hat);
}

mat2x3 findExtremas(mat3 posSpline, vec3 radSpline) {
	const vec3 monomial_radSpline = toMonomial(radSpline);
	const mat3 radmat =
		mat3(vec3(monomial_radSpline[0]), vec3(monomial_radSpline[1]),
			vec3(monomial_radSpline[2]));

	const mat3 qplus = toMonomial(posSpline) + radmat;
	const mat3 qminus = toMonomial(posSpline) - radmat;

	const vec3 pmin = getCurveMinimum(qminus);
	const vec3 pmax = getCurveMaximum(qplus);

	return mat2x3(pmin, pmax);
}

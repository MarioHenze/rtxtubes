#pragma once

#include <utility>

#include <glm/glm.hpp>

#include "hspline.h"

namespace {
	using namespace glm;
}

vec3 getCurveMinimum(mat3 component_curve);
vec3 getCurveMaximum(mat3 component_curve);

// Subdivides a given cubic bezier spline into two C^1 continous quadratic bezier splines.
template<typename CubicBezier, typename QuadraticBezier, typename Element>
std::pair<QuadraticBezier, QuadraticBezier> quadraticSubdivide(CubicBezier cubic_bezier_spline) {
	// TODO: check compatibility/dimensions of matrices/vectors/scalars

	// Formulate quadratic bezier subsegments A and B
	// Endpoints are the same as the original segment
	const Element a1 = cubic_bezier_spline[0];
	const Element b3 = cubic_bezier_spline[3];

	// Respective middle points are determined by translating endpoints
	// along node tangents a specific distance c
	const Element c = Element(_1o3<float>);
	const Element a_tangent = 3.f * (cubic_bezier_spline[1] - cubic_bezier_spline[0]);
	const Element b_tangent = 3.f * (cubic_bezier_spline[3] - cubic_bezier_spline[2]);
	const Element a2 = a1 + c * a_tangent;
	const Element b2 = b3 - c * b_tangent;

	// Connect both subsegments
	const Element a3 = mix(a2, b2, .5);
	const Element b1 = a3;

	// Package both quadratic segments into easier type
	return std::make_pair(QuadraticBezier(a1, a2, a3), QuadraticBezier(b1, b2, b3));
}

mat3 findBasis(mat3 bezier_spline);

mat2x3 findExtremas(mat3 posSpline, vec3 radSpline);

/**
 * @brief tesselateSplineQuadOBBs generates two overlapping OBBs for the given
		  cubic spline
 */
std::array<float, 12> getQuadraticOBBs(HermiteSpline<float> spline);
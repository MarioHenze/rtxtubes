glslc.exe --target-env=vulkan1.2 -O -o hsplinetube_rt.rgen.spv .\hsplinetube_rt.rgen
glslc.exe --target-env=vulkan1.2 -O -o hsplinetube_rt.rmiss.spv .\hsplinetube_rt.rmiss
glslc.exe --target-env=vulkan1.2 -O -o hsplinetube_rt.rint.spv .\hsplinetube_rt.rint
glslc.exe --target-env=vulkan1.2 -O -o hsplinetube_rt.rchit.spv .\hsplinetube_rt.rchit
PAUSE
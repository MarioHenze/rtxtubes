#version 460
#extension GL_KHR_vulkan_glsl : enable
#extension GL_EXT_ray_tracing : require
#extension GL_GOOGLE_include_directive : enable

struct QuadSpline {
	mat3 posSpline;
	mat3x4 colSpline;
	vec3 rad_spline;
};

layout(location = 0) rayPayloadInEXT Payload {
	//vec3 rayDirection;
	vec4 directColor;
} payload;
// hit.x == t; hit.y == f(t) == l
hitAttributeEXT vec2 hit;

layout(set = 0, binding = 0) uniform accelerationStructureEXT tlas;
layout(set = 0, binding = 1, rgba8) uniform image2D image;
layout(set = 0, binding = 2) uniform CGVuTransforms {
	mat4 modelview;
	mat4 proj;
	mat4 normalMat;
} cgvu_trans;
layout(std430, set = 0, binding = 3) readonly restrict buffer QSpline {
	QuadSpline qSplines[];
};

#include "hsplinetube_common.glsl"

void main()
{
	QuadSpline qs = qSplines[gl_PrimitiveID];

	payload.directColor = evalBezier(qs.colSpline, hit.x);
}
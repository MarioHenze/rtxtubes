#version 450
#extension GL_KHR_vulkan_glsl : enable
#extension GL_GOOGLE_include_directive : enable
#extension GL_EXT_debug_printf : enable

////
// Shader config and constants

// Configuration defines
#define NOOP_FRAGMENT_PROCESSING 0

// Uniforms
layout(binding = 0) uniform CGVuTransforms {
	mat4 modelview;
	mat4 proj;
	mat4 normalMat;
} cgvu_trans;

layout(binding = 1) uniform CGVuMaterial {
    vec4 albedo;
    float specularity;
    float shininess;
} cgvu_mat;

layout(binding = 2) uniform CGVuGlobalLight {
	vec3 pos;   // in eye space
	vec3 color;
} cgvu_light;

// Inputs
layout(location = 0) in vec3 in_pos;
layout(location = 1) in vec4 in_color;
layout(location = 2) in flat mat3 in_PosSpline;
layout(location = 5) in flat vec3 in_RadSpline;
layout(location = 6) in flat mat3 in_ColSpline;

// Outputs
layout(location = 0) out vec4 out_color;
layout(depth_greater) out float gl_FragDepth;

// Includes
#include "hsplinetube_common.glsl"

////
// Functions

float[5] compose_gt(vec3 rt, vec3 pyt, vec3 pzt) {
    const float[5] rt2 = squareQuadraticFunction(rt);
    const float[5] pyt2 = squareQuadraticFunction(pyt);
    const float[5] pzt2 = squareQuadraticFunction(pzt);

    float[5] ret;
    for (int i = 0; i < 5; i++) {
        ret[i] = rt2[i] - pyt2[i] - pzt2[i];
    }
    return ret;
}

bool gt_real_at_t(float[5] gt, float t) { return evalMonomial(gt, t) > 0.; }

float eval_ft(vec3 ht, float[5] gt, float t) {
    return evalMonomial(ht, t) - sqrt(evalMonomial(gt, t));
}

float eval_ft_deriv(vec2 ht_deriv, float[5] gt, vec4 gt_deriv, float t) {
    const float eval_htd = evalMonomial(ht_deriv, t);
    const float eval_gtd = evalMonomial(gt_deriv, t);
    const float eval_gt = evalMonomial(gt, t);

    return eval_htd - (eval_gtd * .5 * inversesqrt(eval_gt));
}

float bisect_ft(vec3 ht, float[5] gt, float lower, float upper) {
    if (eval_ft(ht, gt, lower) > 0. && eval_ft(ht, gt, upper) < 0.) {
        const float temp = lower;
        lower = upper;
        upper = temp;
    }

    for (int i = 0; i < 10; i++) {
        const float middle = mix(lower, upper, .5);
        const float middle_value = eval_ft(ht, gt, middle);

        if (middle_value < 0.)
            lower = middle;
        else
            upper = middle;
    }

    return mix(lower, upper, .5);
}

float bisect_ft_deriv(vec2 ht_deriv, float[5] gt, vec4 gt_deriv, float lower,
                      float upper) {
    if (eval_ft_deriv(ht_deriv, gt, gt_deriv, lower) > 0. &&
        eval_ft_deriv(ht_deriv, gt, gt_deriv, upper) < 0.) {
        const float temp = lower;
        lower = upper;
        upper = temp;
    }

    for (int i = 0; i < 10; i++) {
        const float middle = mix(lower, upper, .5);
        const float middle_value =
            eval_ft_deriv(ht_deriv, gt, gt_deriv, middle);
        
        if (middle_value < 0.)
            lower = middle;
        else
            upper = middle;
    }

    return mix(lower, upper, .5);
}

vec2 minimize_ft(vec3 ht, float[5] gt, vec4 ft_deriv_roots) {
    float t_global_min = 0.;
    float ft_global_min = _posInf;
	
	if (gt_real_at_t(gt, 0.)) {
		ft_global_min = eval_ft(ht, gt, 0.);
	}

	if (gt_real_at_t(gt, 1.)) {
		const float ft = eval_ft(ht, gt, 1.);
		if (ft < ft_global_min) {
			t_global_min = 1.;
            ft_global_min = ft;
		}
	}

    for (int i = 1; i <= int(ft_deriv_roots[0]); i++) {
        const float t = ft_deriv_roots[i];
        const float ft = eval_ft(ht, gt, t);
        // Compare and t_global_min min t according to ft evaluation
        if (ft <= ft_global_min) {
            t_global_min = t;
            ft_global_min = ft;
        }
    }
    return vec2(t_global_min, ft_global_min);
}

mat3 calc_ray_base() {
    const vec3 x_hat = normalize(in_pos);
    const vec3 y_hat = normalize(orthogonal(x_hat));
    const vec3 z_hat = normalize(cross(x_hat, y_hat));

    return mat3(x_hat, y_hat, z_hat);
}

// Shader entry point
void main() {
#if (NOOP_FRAGMENT_PROCESSING)
    discard;
#else
    const mat3 rb = inverse(calc_ray_base());

    const mat3 mon_pos = toMonomial(rb * in_PosSpline);
    const vec3 mon_rad = toMonomial(in_RadSpline);

    const vec3 ht = getComponentFkt(mon_pos, 0);
    const vec2 ht_deriv = deriveMonomial(ht);

    const vec3 pyt = getComponentFkt(mon_pos, 1);
    const vec3 pzt = getComponentFkt(mon_pos, 2);

    const float[5] gt = compose_gt(mon_rad, pyt, pzt);
    const vec4 gt_deriv = deriveMonomial(gt);

    const float[5] gt_roots = findRoot(gt);
    const int num_gt_roots = int(gt_roots[0]);

    vec4 ft_deriv_roots = vec4(0., invalid_root, invalid_root, invalid_root);
    if (num_gt_roots >= 1 && gt_real_at_t(gt, 0.)) {
        const float t =
            bisect_ft_deriv(ht_deriv, gt, gt_deriv, 0., gt_roots[1]);
        if (isBetween01(t)) {
            ft_deriv_roots[0]++;
            ft_deriv_roots[1] = t;
        }
    }
    // Only with 2 or more gt roots the inbetween test is necessary
    for (int i = 1; i < num_gt_roots; i++) {
        const float t_a = gt_roots[i];
        const float t_b = gt_roots[i + 1];
        const float t_inbetween = mix(t_a, t_b, .5);
        if (evalMonomial(gt, t_inbetween) > 0.) {
            const float t = bisect_ft_deriv(ht_deriv, gt, gt_deriv, t_a, t_b);
            ft_deriv_roots[0]++;
            ft_deriv_roots[int(ft_deriv_roots[0])] = t;
        }
    }
    if (num_gt_roots >= 1 && gt_real_at_t(gt, 1.)) {
        const float t_n = gt_roots[num_gt_roots];
        const float t = bisect_ft_deriv(ht_deriv, gt, gt_deriv, t_n, 1.);
        if (isBetween01(t)) {
	        ft_deriv_roots[0]++;
	        ft_deriv_roots[int(ft_deriv_roots[0])] = t;
	    }
    }

    //If g(t) has only roots outside [0, 1] i.e. punching through both ball 
    //ends, make an attempt to find f'(t) roots while hoping for the best ...
    if (gt_real_at_t(gt, 0.) && gt_real_at_t(gt, 1.))
    {
        const float t = bisect_ft_deriv(ht_deriv, gt, gt_deriv, 0., 1.);
        if (isBetween01(t)) {
	        ft_deriv_roots[0]++;
	        ft_deriv_roots[int(ft_deriv_roots[0])] = t;
	    }
    }

    // This makes only sense if there are any ft_derived roots
    // hit.x == t; hit.y == f(t) == l
    vec2 hit = minimize_ft(ht, gt, ft_deriv_roots);

    const vec3 hit_pos_eye = normalize(in_pos) * hit.y;
    const vec4 v_eye = vec4(hit_pos_eye, 1.);
    const vec4 depth = cgvu_trans.proj * v_eye;
    gl_FragDepth = depth.z / depth.w;

    // Final color from material
    vec4 color = vec4(evalBezier(in_ColSpline, hit.x), 1.) * cgvu_mat.albedo;

    // Lighting
    // - init
    vec3 normal =
        normalize(hit.y * normalize(hit_pos_eye) - evalBezier(in_PosSpline, hit.x));
    vec3 dirToLight = normalize(cgvu_light.pos - hit_pos_eye);
    // - diffuse term (lambert)
    vec3 diffuse =
        color.rgb * cgvu_light.color * max(dot(dirToLight, normal), 0);
    // - specular term (blinn-phong)
    vec3 halfVec = normalize(normalize(-hit_pos_eye) + dirToLight);
    vec3 specular = color.rgb * cgvu_mat.specularity * cgvu_light.color *
                    pow(max(dot(halfVec, normal), 0), cgvu_mat.shininess);

    // Final color
    out_color = vec4(diffuse + specular, 1.);
    // out_color = abs(in_ray_direction);
#endif
}

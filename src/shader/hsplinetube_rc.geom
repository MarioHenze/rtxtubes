#version 460
#extension GL_KHR_vulkan_glsl : enable
#extension GL_GOOGLE_include_directive : enable
#extension GL_EXT_debug_printf : enable

////
// Shader config and constants

// Geometry shader configuration
layout (lines) in;
// 2 * 14 Vertices for the two segment bounding boxes
layout (triangle_strip, max_vertices = 28) out;

// Uniforms
layout(binding = 0) uniform CGVuTransforms {
	mat4 modelview;
	mat4 proj;
	mat4 normalMat;
} cgvu_trans;

// Inputs
layout(location = 0) in vec4 in_tan[];
layout(location = 1) in vec4 in_col[];
layout(location = 2) in vec4 in_dcol[];
layout(location = 3) in vec2 in_rad[];

// Outputs
layout(location = 0) out vec3 v_position;
layout(location = 1) out vec4 v_color;
layout(location = 2) out flat mat3 out_PosSpline;
layout(location = 5) out flat vec3 out_RadSpline;
layout(location = 6) out flat mat3 out_ColSpline;

// Includes
#include "hsplinetube_common.glsl"

// Globals
#define BBOX_LEFT_DN_FRNT bbox[0][0]
#define BBOX_LEFT_UP_FRNT bbox[0][1]
#define BBOX_RGHT_DN_FRNT bbox[0][2]
#define BBOX_RGHT_UP_FRNT bbox[0][3]
#define BBOX_RGHT_DN_BACK bbox[1][0]
#define BBOX_RGHT_UP_BACK bbox[1][1]
#define BBOX_LEFT_DN_BACK bbox[1][2]
#define BBOX_LEFT_UP_BACK bbox[1][3]
const mat4 unitCube[2] = {
	{vec4(0, 0, 0, 1),
	 vec4(0, 1, 0, 1),
	 vec4(1, 0, 0, 1),
	 vec4(1, 1, 0, 1)},
	{vec4(1, 0, 1, 1),
	 vec4(1, 1, 1, 1),
	 vec4(0, 0, 1, 1),
	 vec4(0, 1, 1, 1)}
};

////
// Functions

float getCurveMinimum(vec3 component_curve) {
    // component_curve contains the spline component in monomial form

    // The quadratic monomial has the form
    // ax^2 + bx + c
    const float a = component_curve[0];
    const float b = component_curve[1];
    const float c = component_curve[2];

    // Derivation 2ax + b = 0 is spline extrema
    // ==> x = -b / 2a
    const float extremal_t = -b / (2 * a);
    const bool t_invalid = isnan(extremal_t) || isOutside01(extremal_t);
    const float extremal_qt =
        t_invalid ? _posInf : evalMonomial(component_curve, extremal_t);

    // Evaluate on spline start and end
    const float start = evalMonomial(component_curve, 0.);
    const float end = evalMonomial(component_curve, 1.);

    return min(min(start, end), extremal_qt);
}

vec3 getCurveMinimum(mat3 curve) {
    vec3 ret = vec3(0.);

    for (int component = 0; component < 3; component++) {
        ret[component] = getCurveMinimum(getComponentFkt(curve, component));
    }

    return ret;
}

float getCurveMaximum(vec3 component_curve) {
    // component_curve contains the spline component in monomial form

    // The quadratic monomial has the form
    // ax^2 + bx + c
    const float a = component_curve[0];
    const float b = component_curve[1];
    const float c = component_curve[2];

    // Derivation 2ax + b = 0 is spline extrema
    // ==> x = -b / 2a
    const float extremal_t = -b / (2 * a);
    const bool t_invalid = isnan(extremal_t) || isOutside01(extremal_t);
    const float extremal_qt =
        t_invalid ? _negInf : evalMonomial(component_curve, extremal_t);

    // Evaluate on spline start and end
    const float start = evalMonomial(component_curve, 0.);
    const float end = evalMonomial(component_curve, 1.);

    return max(max(start, end), extremal_qt);
}

vec3 getCurveMaximum(mat3 curve) {
    vec3 ret = vec3(0.);

    for (int component = 0; component < 3; component++) {
        ret[component] = getCurveMaximum(getComponentFkt(curve, component));
    }

    return ret;
}

// Outputs the segments static data (position nodes, radius nodes, ...) into the
// respective out variables.
void set_segment_statics(mat3 posSpline, vec3 radSpline, mat3x4 colSpline) {
    const mat4 MV = cgvu_trans.modelview;

    const vec4 s = MV * vec4(posSpline[0], 1.);
    const vec3 posSpline_start = s.xyz / s.w; 
    const vec4 c = MV * vec4(posSpline[1], 1.);
    const vec3 posSpline_cp = c.xyz / c.w;
    const vec4 e = MV * vec4(posSpline[2], 1.);
    const vec3 posSpline_end = e.xyz / e.w; 

    out_PosSpline = mat3(posSpline_start, posSpline_cp, posSpline_end);
    out_RadSpline = radSpline;
    out_ColSpline = mat3(colSpline);
}

mat3 findBasis(mat3 bezierSpline) {
    const vec3 x_hat = normalize(bezierSpline[2] - bezierSpline[0]);

    // Project tangent t onto a plane with normal x_hat
    /*const vec3 t = bezierSpline[1] - bezierSpline[0];
    const vec3 v = t - x_hat * dot(t, x_hat);

    const float sideways_sqrLen = dot(v, v);
    const bool isDegenerate = (sideways_sqrLen == 0.);

    const vec3 y_hat =
            isDegenerate ?
            normalize(orthogonal(x_hat)) :
            normalize(v);*/

    const vec3 y_hat = normalize(orthogonal(x_hat));

    const vec3 z_hat = cross(x_hat, y_hat);

    return mat3(x_hat, y_hat, z_hat);
}

// Subdivides a given cubic bezier spline for a scalar valued attribute into
// two C^1 continous quadratic bezier splines.
void quadraticSubdivide(vec4 cubicBezierSpline, out vec3 subSplineA,
                        out vec3 subSplineB) {
    // Formulate quadratic bezier subsegments A and B
    // Endpoints are the same as the original segment
    const float a1 = cubicBezierSpline[0];
    const float b3 = cubicBezierSpline[3];

    // Respective middle points are determined by translating endpoints
    // along node tangents a specific distance c
    const float c = _1o3;
    const float a_tangent = 3. * (cubicBezierSpline[1] - cubicBezierSpline[0]);
    const float b_tangent = 3. * (cubicBezierSpline[3] - cubicBezierSpline[2]);
    const float a2 = a1 + c * a_tangent;
    const float b2 = b3 - c * b_tangent;

    // Connect both subsegments
    const float a3 = mix(a2, b2, .5);
    const float b1 = a3;

    // Package both quadratic segments into easier type
    subSplineA = vec3(a1, a2, a3);
    subSplineB = vec3(b1, b2, b3);
}

// Subdivides a given cubic bezier spline for a 3D valued attribute into
// two C^1 continous quadratic bezier splines.
void quadraticSubdivide(mat4x3 cubicBezierSpline, out mat3 subSplineA,
                        out mat3 subSplineB) {
    // Formulate quadratic bezier subsegments A and B
    // Endpoints are the same as the original segment
    const vec3 a1 = cubicBezierSpline[0];
    const vec3 b3 = cubicBezierSpline[3];

    // Respective middle points are determined by translating endpoints
    // along node tangents a specific distance c
    const float c = _1o3;
    const vec3 a_tangent = 3. * (cubicBezierSpline[1] - cubicBezierSpline[0]);
    const vec3 b_tangent = 3. * (cubicBezierSpline[3] - cubicBezierSpline[2]);
    const vec3 a2 = a1 + c * a_tangent;
    const vec3 b2 = b3 - c * b_tangent;

    // Connect both subsegments
    const vec3 a3 = mix(a2, b2, .5);
    const vec3 b1 = a3;

    // Package both quadratic segments into easier type
    subSplineA = mat3(a1, a2, a3);
    subSplineB = mat3(b1, b2, b3);
}

// Subdivides a given cubic bezier spline for a 4D valued attribute into
// two C^1 continous quadratic bezier splines.
void quadraticSubdivide(mat4 cubicBezierSpline, out mat3x4 subSplineA,
                        out mat3x4 subSplineB) {
	// Formulate quadratic bezier subsegments A and B
    // Endpoints are the same as the original segment
    const vec4 a1 = cubicBezierSpline[0];
    const vec4 b3 = cubicBezierSpline[3];

    // Respective middle points are determined by translating endpoints
    // along node tangents a specific distance c
    const float c = _1o3;
    const vec4 a_tangent = 3. * (cubicBezierSpline[1] - cubicBezierSpline[0]);
    const vec4 b_tangent = 3. * (cubicBezierSpline[3] - cubicBezierSpline[2]);
    const vec4 a2 = a1 + c * a_tangent;
    const vec4 b2 = b3 - c * b_tangent;

    // Connect both subsegments
    const vec4 a3 = mix(a2, b2, .5);
    const vec4 b1 = a3;

    // Package both quadratic segments into easier type
    subSplineA = mat3x4(a1, a2, a3);
    subSplineB = mat3x4(b1, b2, b3);
}

mat2x3 findExtremas(mat3 posSpline, vec3 radSpline) {
    const vec3 monomial_radSpline = toMonomial(radSpline);
    const mat3 radmat =
        mat3(vec3(monomial_radSpline[0]), vec3(monomial_radSpline[1]),
             vec3(monomial_radSpline[2]));

    const mat3 qplus = toMonomial(posSpline) + radmat;
    const mat3 qminus = toMonomial(posSpline) - radmat;

    const vec3 pmin = getCurveMinimum(qminus);
    const vec3 pmax = getCurveMaximum(qplus);

    return mat2x3(pmin, pmax);
}

// Shader entry point
void main() {
    mat4x3 cubicBezierSpline_pos =
        toBezier(gl_in[0].gl_Position.xyz / gl_in[0].gl_Position.w,
                 in_tan[0].xyz,
                 gl_in[1].gl_Position.xyz / gl_in[1].gl_Position.w,
                 in_tan[1].xyz);
    mat3 subSplines[2];
    quadraticSubdivide(cubicBezierSpline_pos, subSplines[0], subSplines[1]);

    vec4 cubicBezierSpline_rad =
        toBezier(in_rad[0].r, in_rad[0].g, in_rad[1].r, in_rad[1].g);
    vec3 radSplines[2];
    quadraticSubdivide(cubicBezierSpline_rad, radSplines[0], radSplines[1]);

    mat4 cubicBezierSpline_col =
        toBezier(in_col[0], in_dcol[0], in_col[1], in_dcol[1]);
    mat3x4 colSplines[2];
    quadraticSubdivide(cubicBezierSpline_col, colSplines[0], colSplines[1]);

    for (int i = 0; i < 2; i++) {
        // Find an aligning basis for the sub segments
        const mat3 basis = findBasis(subSplines[i]);

        // Construct rotation matrix for oriented space
        // rotation mat inverse =^ transpose
        const mat3 R = transpose(basis);

        // Find the silhouette extends by determining the spline extremas.
        const mat2x3 extremas = findExtremas(R * subSplines[i], radSplines[i]);

        // extent = pmax - pmin
        const vec3 pmin = extremas[0];
        const vec3 pmax = extremas[1];
        const vec3 ext = pmax - pmin;

        // Construct scaling/translation matrix for final oriented bounding box
        const mat4 ST = {vec4(ext.x, 0, 0, 0), vec4(0, ext.y, 0, 0),
                         vec4(0, 0, ext.z, 0), vec4(pmin.x, pmin.y, pmin.z, 1)};

        const mat4 M = mat4(basis) * ST;

        // Bounding box vertices
        const mat4 bbox[2] = {cgvu_trans.modelview * M * unitCube[0],
                              cgvu_trans.modelview * M * unitCube[1]};

        // Emit box strip
        // - 14 strip vertices, stripification according to Evans et al.
        v_position = BBOX_LEFT_UP_FRNT.xyz;
        gl_Position = cgvu_trans.proj * BBOX_LEFT_UP_FRNT;
        v_color = in_col[0];
        set_segment_statics(subSplines[i], radSplines[i], colSplines[i]);
        EmitVertex();
        v_position = BBOX_RGHT_UP_FRNT.xyz;
        gl_Position = cgvu_trans.proj * BBOX_RGHT_UP_FRNT;
        v_color = in_col[0];
        set_segment_statics(subSplines[i], radSplines[i], colSplines[i]);
        EmitVertex();
        v_position = BBOX_LEFT_DN_FRNT.xyz;
        gl_Position = cgvu_trans.proj * BBOX_LEFT_DN_FRNT;
        v_color = in_col[0];
        set_segment_statics(subSplines[i], radSplines[i], colSplines[i]);
        EmitVertex();
        v_position = BBOX_RGHT_DN_FRNT.xyz;
        gl_Position = cgvu_trans.proj * BBOX_RGHT_DN_FRNT;
        v_color = in_col[0];
        set_segment_statics(subSplines[i], radSplines[i], colSplines[i]);
        EmitVertex();
        v_position = BBOX_RGHT_DN_BACK.xyz;
        gl_Position = cgvu_trans.proj * BBOX_RGHT_DN_BACK;
        v_color = in_col[1];
        set_segment_statics(subSplines[i], radSplines[i], colSplines[i]);
        EmitVertex();
        v_position = BBOX_RGHT_UP_FRNT.xyz;
        gl_Position = cgvu_trans.proj * BBOX_RGHT_UP_FRNT;
        v_color = in_col[0];
        set_segment_statics(subSplines[i], radSplines[i], colSplines[i]);
        EmitVertex();
        v_position = BBOX_RGHT_UP_BACK.xyz;
        gl_Position = cgvu_trans.proj * BBOX_RGHT_UP_BACK;
        v_color = in_col[1];
        set_segment_statics(subSplines[i], radSplines[i], colSplines[i]);
        EmitVertex();
        v_position = BBOX_LEFT_UP_FRNT.xyz;
        gl_Position = cgvu_trans.proj * BBOX_LEFT_UP_FRNT;
        v_color = in_col[0];
        set_segment_statics(subSplines[i], radSplines[i], colSplines[i]);
        EmitVertex();
        v_position = BBOX_LEFT_UP_BACK.xyz;
        gl_Position = cgvu_trans.proj * BBOX_LEFT_UP_BACK;
        v_color = in_col[1];
        set_segment_statics(subSplines[i], radSplines[i], colSplines[i]);
        EmitVertex();
        v_position = BBOX_LEFT_DN_FRNT.xyz;
        gl_Position = cgvu_trans.proj * BBOX_LEFT_DN_FRNT;
        v_color = in_col[0];
        set_segment_statics(subSplines[i], radSplines[i], colSplines[i]);
        EmitVertex();
        v_position = BBOX_LEFT_DN_BACK.xyz;
        gl_Position = cgvu_trans.proj * BBOX_LEFT_DN_BACK;
        v_color = in_col[1];
        set_segment_statics(subSplines[i], radSplines[i], colSplines[i]);
        EmitVertex();
        v_position = BBOX_RGHT_DN_BACK.xyz;
        gl_Position = cgvu_trans.proj * BBOX_RGHT_DN_BACK;
        v_color = in_col[1];
        set_segment_statics(subSplines[i], radSplines[i], colSplines[i]);
        EmitVertex();
        v_position = BBOX_LEFT_UP_BACK.xyz;
        gl_Position = cgvu_trans.proj * BBOX_LEFT_UP_BACK;
        v_color = in_col[1];
        set_segment_statics(subSplines[i], radSplines[i], colSplines[i]);
        EmitVertex();
        v_position = BBOX_RGHT_UP_BACK.xyz;
        gl_Position = cgvu_trans.proj * BBOX_RGHT_UP_BACK;
        v_color = in_col[1];
        set_segment_statics(subSplines[i], radSplines[i], colSplines[i]);
        EmitVertex();

        // - finalize
        EndPrimitive();
    }
}

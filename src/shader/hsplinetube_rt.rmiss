#version 460
#extension GL_EXT_ray_tracing : require

struct QuadSpline {
	mat3 posSpline;
	mat3x4 colSpline;
	vec3 rad_spline;
};

layout(location = 0) rayPayloadInEXT Payload {
	//vec3 rayDirection;
	vec4 directColor;
} payload;

layout(set = 0, binding = 0) uniform accelerationStructureEXT tlas;
layout(set = 0, binding = 1, rgba8) uniform image2D image;
layout(set = 0, binding = 2) uniform CGVuTransforms {
	mat4 modelview;
	mat4 proj;
	mat4 normalMat;
} cgvu_trans;
layout(std430, set = 0, binding = 3) readonly restrict buffer QSpline {
	QuadSpline qSplines[];
};

void main()
{	
	payload.directColor = vec4(1., 0., 0., 0.);
}
#version 460
#extension GL_EXT_ray_tracing : require
#extension GL_GOOGLE_include_directive : enable
#extension GL_KHR_vulkan_glsl : enable

struct QuadSpline {
	mat3 posSpline;
	mat3x4 colSpline;
	vec3 rad_spline;
};

// hit.x == t; hit.y == f(t) == l
hitAttributeEXT vec2 hit;

layout(set = 0, binding = 0) uniform accelerationStructureEXT tlas;
layout(set = 0, binding = 1, rgba8) uniform image2D image;
layout(set = 0, binding = 2) uniform CGVuTransforms {
	mat4 modelview;
	mat4 proj;
	mat4 normalMat;
} cgvu_trans;
layout(std430, set = 0, binding = 3) readonly restrict buffer QSpline {
	QuadSpline qSplines[];
};

#include "hsplinetube_common.glsl"

////
// Functions

float[5] compose_gt(vec3 rt, vec3 pyt, vec3 pzt) {
    const float[5] rt2 = squareQuadraticFunction(rt);
    const float[5] pyt2 = squareQuadraticFunction(pyt);
    const float[5] pzt2 = squareQuadraticFunction(pzt);

    float[5] ret;
    for (int i = 0; i < 5; i++) {
        ret[i] = rt2[i] - pyt2[i] - pzt2[i];
    }
    return ret;
}

bool gt_real_at_t(float[5] gt, float t) { return evalMonomial(gt, t) >= 0.; }

float eval_ft(vec3 ht, float[5] gt, float t) {
    return evalMonomial(ht, t) - sqrt(max(evalMonomial(gt, t), 0.));
}

float eval_ft_deriv(vec2 ht_deriv, float[5] gt, vec4 gt_deriv, float t) {
    const float eval_htd = evalMonomial(ht_deriv, t);
    const float eval_gtd = evalMonomial(gt_deriv, t);
    const float eval_gt = max(evalMonomial(gt, t), _eps);

    return eval_htd - (eval_gtd * .5 * inversesqrt(eval_gt));
}

float bisect_ft(vec3 ht, float[5] gt, float lower, float upper) {
    if (sign(eval_ft(ht, gt, lower)) == sign(eval_ft(ht, gt, upper))) {
        return invalid_root;
    }

    for (int i = 0; i < 10; i++) {
        const float middle = mix(lower, upper, .5);
        const float middle_value = eval_ft(ht, gt, middle);
        const float lower_value = eval_ft(ht, gt, lower);
        const float upper_value = eval_ft(ht, gt, upper);

        const bool root_lower = sign(middle_value) == sign(upper_value);
        const bool root_upper = sign(lower_value) == sign(middle_value);

        lower = root_upper ? middle : lower;
        upper = root_lower ? middle : upper;
    }

    return mix(lower, upper, .5);
}

float bisect_ft_deriv(vec2 ht_deriv, float[5] gt, vec4 gt_deriv, float lower,
                      float upper) {
    if (sign(eval_ft_deriv(ht_deriv, gt, gt_deriv, lower)) ==
        sign(eval_ft_deriv(ht_deriv, gt, gt_deriv, upper))) {
        return invalid_root;
    }

    for (int i = 0; i < 10; i++) {
        const float middle = mix(lower, upper, .5);
        const float middle_value =
            eval_ft_deriv(ht_deriv, gt, gt_deriv, middle);
        const float lower_value = eval_ft_deriv(ht_deriv, gt, gt_deriv, lower);
        const float upper_value = eval_ft_deriv(ht_deriv, gt, gt_deriv, upper);

        const bool root_lower =
            sign(middle_value) == sign(upper_value) || isnan(upper_value);
        const bool root_upper =
            sign(lower_value) == sign(middle_value) || isnan(lower_value);

        lower = root_upper ? middle : lower;
        upper = root_lower ? middle : upper;
    }

    return mix(lower, upper, .5);
}

vec2 minimize_ft(vec3 ht, float[5] gt, vec4 ft_deriv_roots) {
    float t_global_min = 0.;
    float ft_global_min = _posInf;
	
	if (gt_real_at_t(gt, 0.)) {
		ft_global_min = eval_ft(ht, gt, _eps);
	}

	if (gt_real_at_t(gt, 1.)) {
		const float ft = eval_ft(ht, gt, 1.);
		if (ft <= ft_global_min) {
			t_global_min = 1.;
            ft_global_min = ft;
		}
	}

    for (int i = 1; i <= int(ft_deriv_roots[0]); i++) {
        const float t = ft_deriv_roots[i];
        const float ft = eval_ft(ht, gt, t);
        // Compare and t_global_min min t according to ft evaluation
        if (ft <= ft_global_min) {
            t_global_min = t;
            ft_global_min = ft;
        }
    }
    return vec2(t_global_min, ft_global_min);
}

mat3 calc_ray_base() {
    const vec3 x_hat = normalize(gl_WorldRayDirectionEXT);
    const vec3 y_hat = normalize(orthogonal(x_hat));
    const vec3 z_hat = normalize(cross(x_hat, y_hat));

    return mat3(x_hat, y_hat, z_hat);
}

void main()
{
	const mat3 rb = inverse(calc_ray_base());

    const mat3 mon_pos = toMonomial(rb * qSplines[gl_PrimitiveID].posSpline);
    const vec3 mon_rad = toMonomial(qSplines[gl_PrimitiveID].rad_spline);

    const vec3 ht = getComponentFkt(mon_pos, 0);
    const vec2 ht_deriv = deriveMonomial(ht);

    const vec3 pyt = getComponentFkt(mon_pos, 1);
    const vec3 pzt = getComponentFkt(mon_pos, 2);

    const float[5] gt = compose_gt(mon_rad, pyt, pzt);
    const vec4 gt_deriv = deriveMonomial(gt);
    const float[5] all_gt_roots = findRoot(gt);

    const float[5] gt_roots = filter01Roots(all_gt_roots);
    const int num_gt_roots = int(gt_roots[0]);

    vec4 ft_deriv_roots = vec4(0., invalid_root, invalid_root, invalid_root);
    if (num_gt_roots >= 1 && evalMonomial(gt, 0.) > 0.) {
        const float t =
            bisect_ft_deriv(ht_deriv, gt, gt_deriv, 0., gt_roots[1]);
        if (isBetween01(t)) {
            ft_deriv_roots[0]++;
            ft_deriv_roots[1] = t;
        }
    }
    // Only with 2 or more gt roots the inbetween test is necessary
    for (int i = 1; i < num_gt_roots; i++) {
        const float t_a = gt_roots[i];
        const float t_b = gt_roots[i + 1];
        const float t_inbetween = mix(t_a, t_b, .5);
        if (evalMonomial(gt, t_inbetween) > 0.) {
            const float t = bisect_ft_deriv(ht_deriv, gt, gt_deriv, t_a, t_b);
            ft_deriv_roots[0]++;
            ft_deriv_roots[int(ft_deriv_roots[0])] = t;
        }
    }
    if (num_gt_roots >= 1 && evalMonomial(gt, 1.) > 0.) {
        const float t_n = gt_roots[num_gt_roots];
        const float t = bisect_ft_deriv(ht_deriv, gt, gt_deriv, t_n, 1.);
        if (isBetween01(t)) {
	        ft_deriv_roots[0]++;
	        ft_deriv_roots[int(ft_deriv_roots[0])] = t;
	    }
    }

    //If g(t) has only roots outside [0, 1] i.e. punching through both ball 
    //ends, make an attempt to find f'(t) roots while hoping for the best ...
    if (gt_real_at_t(gt, 0.) && gt_real_at_t(gt, 1.))
    {
    	const float t_n = gt_roots[num_gt_roots];
        const float t = bisect_ft_deriv(ht_deriv, gt, gt_deriv, 0., 1.);
        if (isBetween01(t)) {
	        ft_deriv_roots[0]++;
	        ft_deriv_roots[int(ft_deriv_roots[0])] = t;
	    }
    }
 
    // This makes only sense if there are any ft_derived roots
    // hit.x == t; hit.y == f(t) == l
    vec2 hit = minimize_ft(ht, gt, ft_deriv_roots);

    reportIntersectionEXT(hit.y, 0);
    /*const vec3 hit_pos_eye = normalize(gl_WorldRayDirectionEXT) * hit.y;
    const vec4 v_eye = vec4(hit_pos_eye, 1.);
    const vec4 depth = cgvu_trans.proj * v_eye;*/
}
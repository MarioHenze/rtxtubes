
//////
//
// Globals
//

const float _posInf = 3e+37;
const float _negInf = -3e+37;

// 1 over 3
const float _1o3 = 1.0 / 3.0;
// 2 over 3
const float _2o3 = 2.0 / 3.0;

// Floating-point epsilon (highp, i.e. 2^-23)
const float _eps = 0.00000011920928955078125;
// 4D epsilon vector
const vec4 _eps4 = vec4(_eps);
// 4D epsilon vector
const vec3 _eps3 = vec3(_eps);
// 4D epsilon vector
const vec2 _eps2 = vec2(_eps);

// 4D zero vector
const vec4 _zero4 = vec4(0);
// 3D zero vector
const vec3 _zero3 = vec3(0);
// 2D zero vector
const vec2 _zero2 = vec2(0);

// 4D one vector
const vec4 _one4 = vec4(1);
// 3D one vector
const vec3 _one3 = vec3(1);
// 2D one vector
const vec2 _one2 = vec2(1);

// 4D two vector
const vec4 _two4 = vec4(2);
// 3D two vector
const vec3 _two3 = vec3(2);
// 2D two vector
const vec2 _two2 = vec2(2);

// 4D boolean true vector
const bvec4 _true4 = bvec4(true);
// 3D boolean true vector
const bvec3 _true3 = bvec3(true);
// 2D boolean true vector
const bvec2 _true2 = bvec2(true);

// Cubic Hermite to Bezier basis transform
const mat4 h2b = {vec4(1, 0, 0, 0), vec4(1, _1o3, 0, 0), vec4(0, 0, -_1o3, 1),
                  vec4(0, 0, 0, 1)};
// Cubic Bezier to Hermite basis transform
const mat4 b2h = {vec4(1, 0, 0, 0), vec4(-3, 3, 0, 0), vec4(0, 0, -3, 3),
                  vec4(0, 0, 0, 1)};

// Cubic Bezier to monomial basis transform
const mat4 b2m = {vec4(-1, 3, -3, 1), vec4(3, -6, 3, 0), vec4(-3, 3, 0, 0),
                  vec4(1, 0, 0, 0)};
// Cubic Monomial to Bezier basis transform
const mat4 m2b = {vec4(0, 0, 0, 1), vec4(0, 0, _1o3, 1), vec4(0, _1o3, _2o3, 1),
                  vec4(1, 1, 1, 1)};

// Quadratic Bezier to monomial basis transform
const mat3 b2m_2 = {vec3(1, -2, 1), vec3(-2, 2, 0), vec3(1, 0, 0)};
// Quadratic Monomial to Bezier basis transform
const mat3 m2b_2 = {vec3(0, 0, 1), vec3(0, 0.5, 1), vec3(1, 1, 1)};

// Linear Bezier to monomial basis transform
const mat2 b2m_1 = {vec2(-1, 1), vec2(1, 0)};
// Linear Monomial to Bezier basis transform
const mat2 m2b_1 = {vec2(0, 1), vec2(1, 1)};

// TODO: How to treat intervall borders?
const float root_search_lower_bound = 0.;
const float root_search_upper_bound = 1.;
const float invalid_root = 2.;

////
// Misc utilities

// Copy and swap inplace if x is bigger than y
void cas(inout vec2 v) { v = v.x > v.y ? v.yx : v; }

void sort(inout vec2 v) { cas(v); }

void sort(inout vec3 v) {
    cas(v.yz);
    cas(v.xy);
    cas(v.yz);
}

void sort(inout vec4 v) {
    cas(v.xy);
    cas(v.zw);
    cas(v.yw);
    cas(v.xz);
    cas(v.yz);
}

// Componentwise and
bvec2 and (bvec2 a, bvec2 b) { return bvec2(a.x && b.x, a.y && b.y); }

// Componentwise and
bvec3 and (bvec3 a, bvec3 b) {
    return bvec3(a.x && b.x, a.y && b.y, a.z && b.z);
}

// Componentwise and
bvec4 and (bvec4 a, bvec4 b) {
    return bvec4(a.x && b.x, a.y && b.y, a.z && b.z, a.w && b.w);
}

// Checks if a given value is inside the given interval
bool isBetween(in float t, in float tmin, in float tmax) {
    return t >= tmin && t <= tmax;
}

// Checks if a given value is between 0 and 1
bool isBetween01(in float t) { return t >= 0. && t <= 1.; }

bvec2 isBetween01(in vec2 t) {
    return and(greaterThanEqual(t, vec2(0.)), lessThanEqual(t, vec2(1.)));
}

bvec3 isBetween01(in vec3 t) {
    return and(greaterThanEqual(t, vec3(0.)), lessThanEqual(t, vec3(1.)));
}

bvec4 isBetween01(in vec4 t) {
    return and(greaterThanEqual(t, vec4(0.)), lessThanEqual(t, vec4(1.)));
}

// Checks if a given value is outside the given interval
bool isOutside(in float t, in float tmin, in float tmax) {
    return isnan(t) || isnan(tmin) || isnan(tmax) || t < tmin || t > tmax;
}

// Checks if a given value is outside of 0 and 1
bool isOutside01(in float t) { return isnan(t) || t < 0. || t > 1.; }

// Returns an orthogonal vector to the given v
// Always works if the input is non-zero.
// Doesn’t require the input to be normalised.
// Doesn’t normalise the output. */
vec3 orthogonal(vec3 v) {
    return abs(v.x) > abs(v.z) ? vec3(-v.y, v.x, 0.0) : vec3(0.0, -v.z, v.y);
}

//////
//
// Functions
//

////
// Polynom Arithmetic

float[5] squareQuadraticFunction(vec3 quadMonom) {
    const float a = quadMonom[0];
    const float b = quadMonom[1];
    const float c = quadMonom[2];

    return float[5](a * a, 2. * a * b, 2. * a * c + b * b, 2. * b * c, c * c);
}

vec4 deriveMonomial(float[5] quartMonom) {
    const float a = quartMonom[0];
    const float b = quartMonom[1];
    const float c = quartMonom[2];
    const float d = quartMonom[3];

    return vec4(4. * a, 3. * b, 2. * c, d);
}

vec3 deriveMonomial(vec4 cubicMonom) {
    const float a = cubicMonom[0];
    const float b = cubicMonom[1];
    const float c = cubicMonom[2];

    return vec3(3. * a, 2. * b, c);
}

vec2 deriveMonomial(vec3 quadraticMonom) {
    const float a = quadraticMonom[0];
    const float b = quadraticMonom[1];

    return vec2(2 * a, b);
}

////
// Component function extraction

// Extracts the i-th component function from the given 4D vector-valued cubic
// curve
vec4 getComponentFkt(mat4 curve, int i) {
    return vec4(curve[0][i], curve[1][i], curve[2][i], curve[3][i]);
}

// Extracts the i-th component function from the given 3D vector-valued cubic
// curve
vec4 getComponentFkt(mat4x3 curve, int i) {
    return vec4(curve[0][i], curve[1][i], curve[2][i], curve[3][i]);
}

// Extracts the i-th component function from the given 2D vector-valued cubic
// curve
vec4 getComponentFkt(mat4x2 curve, int i) {
    return vec4(curve[0][i], curve[1][i], curve[2][i], curve[3][i]);
}

// Extracts the i-th component function from the given 4D vector-valued
// quadratic curve
vec3 getComponentFkt(mat3x4 curve, int i) {
    return vec3(curve[0][i], curve[1][i], curve[2][i]);
}

// Extracts the i-th component function from the given 3D vector-valued
// quadratic curve
vec3 getComponentFkt(mat3 curve, int i) {
    return vec3(curve[0][i], curve[1][i], curve[2][i]);
}

// Extracts the i-th component function from the given 2D vector-valued
// quadratic curve
vec3 getComponentFkt(mat3x2 curve, int i) {
    return vec3(curve[0][i], curve[1][i], curve[2][i]);
}

////
// Bezier to Hermite conversion

// Converts a 4D vector-valued cubic Hermite curve to a cubic Bezier curve
mat4 toBezier(in vec4 n0, in vec4 t0, in vec4 n1, in vec4 t1) {
    return mat4(n0, n0 + _1o3 * t0, n1 - _1o3 * t1, n1);
}
// Converts a 4D vector-valued cubic Hermite curve to a cubic Bezier curve,
// writing the control points to the given 4x4 matrix
void toBezier(out mat4 b, in vec4 n0, in vec4 t0, in vec4 n1, in vec4 t1) {
    b[0] = n0;
    b[1] = n0 + _1o3 * t0;
    b[2] = n1 - _1o3 * t1;
    b[3] = n1;
}
// Converts a 4D vector-valued cubic Hermite curve to a cubic Bezier curve
mat4 toBezier(in mat4 h) { return h * h2b; }
// Converts a 4D vector-valued cubic Hermite curve to a cubic Bezier curve,
// writing the control points to the given 4x4 matrix
void toBezier(out mat4 b, in mat4 h) { b = h * h2b; }

// Converts a 3D vector-valued cubic Hermite curve to a cubic Bezier curve
mat4x3 toBezier(in vec3 n0, in vec3 t0, in vec3 n1, in vec3 t1) {
    return mat4x3(n0, n0 + _1o3 * t0, n1 - _1o3 * t1, n1);
}
// Converts a 3D vector-valued cubic Hermite curve to a cubic Bezier curve,
// writing the control points to the given 4x3 matrix
void toBezier(out mat4x3 b, in vec3 n0, in vec3 t0, in vec3 n1, in vec3 t1) {
    b[0] = n0;
    b[1] = n0 + _1o3 * t0;
    b[2] = n1 - _1o3 * t1;
    b[3] = n1;
}
// Converts a 3D vector-valued cubic Hermite curve to a cubic Bezier curve
mat4x3 toBezier(in mat4x3 h) { return h * h2b; }
// Converts a 3D vector-valued cubic Hermite curve to a cubic Bezier curve,
// writing the control points to the given 4x3 matrix
void toBezier(out mat4x3 b, in mat4x3 h) { b = h * h2b; }

// Converts a 2D vector-valued cubic Hermite curve to a cubic Bezier curve
mat4x2 toBezier(in vec2 n0, in vec2 t0, in vec2 n1, in vec2 t1) {
    return mat4x2(n0, n0 + _1o3 * t0, n1 - _1o3 * t1, n1);
}
// Converts a 2D vector-valued cubic Hermite curve to a cubic Bezier curve,
// writing the control points to the given 4x2 matrix
void toBezier(out mat4x2 b, in vec2 n0, in vec2 t0, in vec2 n1, in vec2 t1) {
    b[0] = n0;
    b[1] = n0 + _1o3 * t0;
    b[2] = n1 - _1o3 * t1;
    b[3] = n1;
}
// Converts a 2D vector-valued cubic Hermite curve to a cubic Bezier curve
mat4x2 toBezier(in mat4x2 h) { return h * h2b; }
// Converts 2D a vector-valued cubic Hermite curve to a cubic Bezier curve,
// writing the control points to the given 4x2 matrix
void toBezier(out mat4x2 b, in mat4x2 h) { b = h * h2b; }

// Converts a scalar-valued cubic Hermite curve to a cubic Bezier curve
vec4 toBezier(in float n0, in float t0, in float n1, in float t1) {
    return vec4(n0, n0 + _1o3 * t0, n1 - _1o3 * t1, n1);
}
// Converts a scalar-valued cubic Hermite curve to a cubic Bezier curve, writing
// the control points to the given 4-vector
void toBezier(out vec4 b, in float n0, in float t0, in float n1, in float t1) {
    b[0] = n0;
    b[1] = n0 + _1o3 * t0;
    b[2] = n1 - _1o3 * t1;
    b[3] = n1;
}
// Converts a scalar-vector-valued cubic Hermite curve to a cubic Bezier curve
vec4 toBezier(in vec4 h) { return h * h2b; }
// Converts a scalar-valued cubic Hermite curve to a cubic Bezier curve, writing
// the control points to the given 4-vector
void toBezier(out vec4 b, in vec4 h) { b = h * h2b; }

////
// Bezier to Hermite conversion

// Converts a 4D vector-valued cubic Bezier curve to a cubic Hermite curve
mat4 toHermite(in vec4 b0, in vec4 b1, in vec4 b2, in vec4 b3) {
    return mat4(b0, 3 * (b1 - b0), 3 * (b3 - b2), b3);
}
// Converts a 4D vector-valued cubic Bezier curve to a cubic Hermite curve,
// writing the control points to the given 4x4 matrix
void toHermite(out mat4 h, in vec4 b0, in vec4 b1, in vec4 b2, in vec4 b3) {
    h[0] = b0;
    h[1] = 3 * (b1 - b0);
    h[2] = 3 * (b3 - b2);
    h[3] = b3;
}
// Converts a 4D vector-valued cubic Bezier curve to a cubic Hermite curve
mat4 toHermite(in mat4 b) { return b * b2h; }
// Converts a 4D vector-valued cubic Bezier curve to a cubic Hermite curve,
// writing the control points to the given 4x4 matrix
void toHermite(out mat4 h, in mat4 b) { h = b * b2h; }

// Converts a 3D vector-valued cubic Bezier curve to a cubic Hermite curve
mat4x3 toHermite(in vec3 b0, in vec3 b1, in vec3 b2, in vec3 b3) {
    return mat4x3(b0, 3 * (b1 - b0), 3 * (b3 - b2), b3);
}
// Converts a 3D vector-valued cubic Bezier curve to a cubic Hermite curve,
// writing the control points to the given 4x3 matrix
void toHermite(out mat4x3 h, in vec3 b0, in vec3 b1, in vec3 b2, in vec3 b3) {
    h[0] = b0;
    h[1] = 3 * (b1 - b0);
    h[2] = 3 * (b3 - b2);
    h[3] = b3;
}
// Converts a 3D vector-valued cubic Bezier curve to a cubic Hermite curve
mat4x3 toHermite(in mat4x3 b) { return b * b2h; }
// Converts a 3D vector-valued cubic Bezier curve to a cubic Hermite curve,
// writing the control points to the given 4x3 matrix
void toHermite(out mat4x3 h, in mat4x3 b) { h = b * b2h; }

// Converts a 2D vector-valued cubic Bezier curve to a cubic Hermite curve
mat4x2 toHermite(in vec2 b0, in vec2 b1, in vec2 b2, in vec2 b3) {
    return mat4x2(b0, 3 * (b1 - b0), 3 * (b3 - b2), b3);
}
// Converts a 2D vector-valued cubic Bezier curve to a cubic Hermite curve,
// writing the control points to the given 4x2 matrix
void toHermite(out mat4x2 h, in vec2 b0, in vec2 b1, in vec2 b2, in vec2 b3) {
    h[0] = b0;
    h[1] = 3 * (b1 - b0);
    h[2] = 3 * (b3 - b2);
    h[3] = b3;
}
// Converts a 2D vector-valued cubic Bezier curve to a cubic Hermite curve
mat4x2 toHermite(in mat4x2 b) { return b * b2h; }
// Converts a 2D vector-valued cubic Bezier curve to a cubic Hermite curve,
// writing the control points to the given 4x2 matrix
void toHermite(out mat4x2 h, in mat4x2 b) { h = b * b2h; }

// Converts a scalar-valued cubic Bezier curve to a cubic Hermite curve
vec4 toHermite(in float b0, in float b1, in float b2, in float b3) {
    return vec4(b0, 3 * (b1 - b0), 3 * (b3 - b2), b3);
}
// Converts a scalar-valued cubic Bezier curve to a cubic Hermite curve, writing
// the control points to the given 4-vector
void toHermite(out vec4 h, in float b0, in float b1, in float b2, in float b3) {
    h[0] = b0;
    h[1] = 3 * (b1 - b0);
    h[2] = 3 * (b3 - b2);
    h[3] = b3;
}
// Converts a scalar-valued cubic Bezier curve to a cubic Hermite curve
vec4 toHermite(in vec4 b) { return b * b2h; }
// Converts a scalar-valued cubic Bezier curve to a cubic Hermite curve, writing
// the control points to the given 4-vector
void toHermite(out vec4 h, in vec4 b) { h = b * b2h; }

////
// Bezier to Monomial conversion

// Converts a 4D vector-valued cubic Bezier curve to canonical polynomial form
mat4 toMonomial(in vec4 b0, in vec4 b1, in vec4 b2, in vec4 b3) {
    return mat4(-b0 + 3 * b1 - 3 * b2 + b3, 3 * b0 - 6 * b1 + 3 * b2,
                -3 * b0 + 3 * b1, b0);
}
// Converts a 4D vector-valued cubic Bezier curve to canonical polynomial form,
// writing the control points to the given 4x4 matrix
void toMonomial(out mat4 m, in vec4 b0, in vec4 b1, in vec4 b2, in vec4 b3) {
    m[0] = -b0 + 3 * b1 - 3 * b2 + b3;
    m[1] = 3 * b0 - 6 * b1 + 3 * b2;
    m[2] = -3 * b0 + 3 * b1;
    m[3] = b0;
}
// Converts a 4D vector-valued cubic Bezier curve to canonical polynomial form
mat4 toMonomial(in mat4 b) { return b * b2m; }
// Converts a 4D vector-valued cubic Bezier curve to canonical polynomial form,
// writing the control points to the given 4x4 matrix
void toMonomial(out mat4 m, in mat4 b) { m = b * b2m; }

// Converts a 3D vector-valued cubic Bezier curve to canonical polynomial form
mat4x3 toMonomial(in vec3 b0, in vec3 b1, in vec3 b2, in vec3 b3) {
    return mat4x3(-b0 + 3 * b1 - 3 * b2 + b3, 3 * b0 - 6 * b1 + 3 * b2,
                  -3 * b0 + 3 * b1, b0);
}
// Converts a 3D vector-valued cubic Bezier curve to canonical polynomial form,
// writing the control points to the given 4x3 matrix
void toMonomial(out mat4x3 m, in vec3 b0, in vec3 b1, in vec3 b2, in vec3 b3) {
    m[0] = -b0 + 3 * b1 - 3 * b2 + b3;
    m[1] = 3 * b0 - 6 * b1 + 3 * b2;
    m[2] = -3 * b0 + 3 * b1;
    m[3] = b0;
}
// Converts a 3D vector-valued cubic Bezier curve to canonical polynomial form
mat4x3 toMonomial(in mat4x3 b) { return b * b2m; }
// Converts a 3D vector-valued cubic Bezier curve to canonical polynomial form,
// writing the control points to the given 4x3 matrix
void toMonomial(out mat4x3 m, in mat4x3 b) { m = b * b2m; }

// Converts a 2D vector-valued cubic Bezier curve to canonical polynomial form
mat4x2 toMonomial(in vec2 b0, in vec2 b1, in vec2 b2, in vec2 b3) {
    return mat4x2(-b0 + 3 * b1 - 3 * b2 + b3, 3 * b0 - 6 * b1 + 3 * b2,
                  -3 * b0 + 3 * b1, b0);
}
// Converts a 2D vector-valued cubic Bezier curve to canonical polynomial form,
// writing the control points to the given 4x2 matrix
void toMonomial(out mat4x2 m, in vec2 b0, in vec2 b1, in vec2 b2, in vec2 b3) {
    m[0] = -b0 + 3 * b1 - 3 * b2 + b3;
    m[1] = 3 * b0 - 6 * b1 + 3 * b2;
    m[2] = -3 * b0 + 3 * b1;
    m[3] = b0;
}
// Converts a 2D vector-valued cubic Bezier curve to canonical polynomial form
mat4x2 toMonomial(in mat4x2 b) { return b * b2m; }
// Converts a 2D vector-valued cubic Bezier curve to canonical polynomial form,
// writing the control points to the given 4x2 matrix
void toMonomial(out mat4x2 m, in mat4x2 b) { m = b * b2m; }

// Converts a scalar-valued cubic Bezier curve to canonical polynomial form
vec4 toMonomial(in float b0, in float b1, in float b2, in float b3) {
    return vec4(-b0 + 3 * b1 - 3 * b2 + b3, 3 * b0 - 6 * b1 + 3 * b2,
                -3 * b0 + 3 * b1, b0);
}
// Converts a scalar-valued cubic Bezier curve to canonical polynomial form,
// writing the control points to the given 4-vector
void toMonomial(out vec4 m, in float b0, in float b1, in float b2,
                in float b3) {
    m[0] = -b0 + 3 * b1 - 3 * b2 + b3;
    m[1] = 3 * b0 - 6 * b1 + 3 * b2;
    m[2] = -3 * b0 + 3 * b1;
    m[3] = b0;
}
// Converts a scalar-valued cubic Bezier curve to canonical polynomial form
vec4 toMonomial(in vec4 b) { return b * b2m; }
// Converts a scalar-valued cubic Bezier curve to canonical polynomial form,
// writing the control points to the given 4-vector
void toMonomial(out vec4 m, in vec4 b) { m = b * b2m; }

// Converts a 4D vector-valued quadratic Bezier curve to canonical polynomial
// form
mat3x4 toMonomial(in vec4 b0, in vec4 b1, in vec4 b2) {
    return mat3x4(b0 - 2 * b1 + b2, -2 * b0 + 2 * b1, b0);
}
// Converts a 4D vector-valued quadratic Bezier curve to canonical polynomial
// form, writing the control points to the given 3x4 matrix
void toMonomial(out mat3x4 m, in vec4 b0, in vec4 b1, in vec4 b2) {
    m[0] = b0 - 2 * b1 + b2;
    m[1] = -2 * b0 + 2 * b1;
    m[2] = b0;
}
// Converts a 4D vector-valued quadratic Bezier curve to canonical polynomial
// form
mat3x4 toMonomial(in mat3x4 b) { return b * b2m_2; }
// Converts a 4D vector-valued quadratic Bezier curve to canonical polynomial
// form, writing the control points to the given 3x4 matrix
void toMonomial(out mat3x4 m, in mat3x4 b) { m = b * b2m_2; }

// Converts a 3D vector-valued quadratic Bezier curve to canonical polynomial
// form
mat3 toMonomial(in vec3 b0, in vec3 b1, in vec3 b2) {
    return mat3(b0 - 2 * b1 + b2, -2 * b0 + 2 * b1, b0);
}
// Converts a 3D vector-valued quadratic Bezier curve to canonical polynomial
// form, writing the control points to the given 3x4 matrix
void toMonomial(out mat3 m, in vec3 b0, in vec3 b1, in vec3 b2) {
    m[0] = b0 - 2 * b1 + b2;
    m[1] = -2 * b0 + 2 * b1;
    m[2] = b0;
}
// Converts a 3D vector-valued quadratic Bezier curve to canonical polynomial
// form
mat3 toMonomial(in mat3 b) { return b * b2m_2; }
// Converts a 3D vector-valued quadratic Bezier curve to canonical polynomial
// form, writing the control points to the given 3x4 matrix
void toMonomial(out mat3 m, in mat3 b) { m = b * b2m_2; }

// Converts a 2D vector-valued quadratic Bezier curve to canonical polynomial
// form
mat3x2 toMonomial(in vec2 b0, in vec2 b1, in vec2 b2) {
    return mat3x2(b0 - 2 * b1 + b2, -2 * b0 + 2 * b1, b0);
}
// Converts a 2D vector-valued quadratic Bezier curve to canonical polynomial
// form, writing the control points to the given 3x4 matrix
void toMonomial(out mat3x2 m, in vec2 b0, in vec2 b1, in vec2 b2) {
    m[0] = b0 - 2 * b1 + b2;
    m[1] = -2 * b0 + 2 * b1;
    m[2] = b0;
}
// Converts a 2D vector-valued quadratic Bezier curve to canonical polynomial
// form
mat3x2 toMonomial(in mat3x2 b) { return b * b2m_2; }
// Converts a 2D vector-valued quadratic Bezier curve to canonical polynomial
// form, writing the control points to the given 3x4 matrix
void toMonomial(out mat3x2 m, in mat3x2 b) { m = b * b2m_2; }

// Converts a scalar-valued quadratic Bezier curve to canonical polynomial form
vec3 toMonomial(in float b0, in float b1, in float b2) {
    return vec3(b0 - 2 * b1 + b2, -2 * b0 + 2 * b1, b0);
}
// Converts a scalar-valued quadratic Bezier curve to canonical polynomial form,
// writing the control points to the given 3x4 matrix
void toMonomial(out vec3 m, in float b0, in float b1, in float b2) {
    m[0] = b0 - 2 * b1 + b2;
    m[1] = -2 * b0 + 2 * b1;
    m[2] = b0;
}
// Converts a scalar-valued quadratic Bezier curve to canonical polynomial form
vec3 toMonomial(in vec3 b) { return b * b2m_2; }
// Converts a scalar-valued quadratic Bezier curve to canonical polynomial form,
// writing the control points to the given 3x4 matrix
void toMonomial(out vec3 m, in vec3 b) { m = b * b2m_2; }

////
// Bezier curve derivatives

// Derives the given 4D vector-valued cubic Bezier curve, returning the control
// points of the resulting quadratic Bezier curve
mat3x4 deriveBezier(in mat4 b) {
    return mat3x4(3 * (b[1] - b[0]), 3 * (b[2] - b[1]), 3 * (b[3] - b[2]));
}

// Derives the given 3D vector-valued cubic Bezier curve, returning the control
// points of the resulting quadratic Bezier curve
mat3 deriveBezier(in mat4x3 b) {
    return mat3(3 * (b[1] - b[0]), 3 * (b[2] - b[1]), 3 * (b[3] - b[2]));
}

// Derives the given 2D vector-valued cubic Bezier curve, returning the control
// points of the resulting quadratic Bezier curve
mat3x2 deriveBezier(in mat4x2 b) {
    return mat3x2(3 * (b[1] - b[0]), 3 * (b[2] - b[1]), 3 * (b[3] - b[2]));
}

// Derives the given scalar-valued cubic Bezier curve, returning the control
// points of the resulting quadratic Bezier curve
vec3 deriveBezier(in vec4 b) {
    return vec3(3 * (b[1] - b[0]), 3 * (b[2] - b[1]), 3 * (b[3] - b[2]));
}

// Derives the given 4D vector-valued quadratic Bezier curve, returning the
// control points of the resulting linear Bezier curve (i.e. the endpoints of a
// line)
mat2x4 deriveBezier(in mat3x4 b) {
    return mat2x4(2 * (b[1] - b[0]), 2 * (b[2] - b[1]));
}

// Derives the given 3D vector-valued quadratic Bezier curve, returning the
// control points of the resulting linear Bezier curve (i.e. the endpoints of a
// line)
mat2x3 deriveBezier(in mat3 b) {
    return mat2x3(2 * (b[1] - b[0]), 2 * (b[2] - b[1]));
}

// Derives the given 2D vector-valued quadratic Bezier curve, returning the
// control points of the resulting linear Bezier curve (i.e. the endpoints of a
// line)
mat2 deriveBezier(in mat3x2 b) {
    return mat2(2 * (b[1] - b[0]), 2 * (b[2] - b[1]));
}

// Derives the given scalar-valued quadratic Bezier curve, returning the control
// points of the resulting linear Bezier curve (i.e. the endpoints of a line)
vec2 deriveBezier(in vec3 b) {
    return vec2(2 * (b[1] - b[0]), 2 * (b[2] - b[1]));
}

////
// Bezier curve evaluation

// Evaluates 4D vector-valued cubic Bezier curve at given t=0..1
vec4 evalBezier(in mat4 b, in float t) {
    mat3x4 v = {mix(b[0], b[1], t), mix(b[1], b[2], t), mix(b[2], b[3], t)};
    v[0] = mix(v[0], v[1], t);
    v[1] = mix(v[1], v[2], t);
    return mix(v[0], v[1], t);
}

// Evaluates 3D vector-valued cubic Bezier curve at given t=0..1
vec3 evalBezier(in mat4x3 b, in float t) {
    mat3x3 v = {mix(b[0], b[1], t), mix(b[1], b[2], t), mix(b[2], b[3], t)};
    v[0] = mix(v[0], v[1], t);
    v[1] = mix(v[1], v[2], t);
    return mix(v[0], v[1], t);
}

// Evaluates 2D vector-valued cubic Bezier curve at given t=0..1
vec2 evalBezier(in mat4x2 b, in float t) {
    mat3x2 v = {mix(b[0], b[1], t), mix(b[1], b[2], t), mix(b[2], b[3], t)};
    v[0] = mix(v[0], v[1], t);
    v[1] = mix(v[1], v[2], t);
    return mix(v[0], v[1], t);
}

// Evaluates scalar-valued cubic Bezier curve at given t=0..1
float evalBezier(in vec4 b, in float t) {
    vec3 v = {mix(b[0], b[1], t), mix(b[1], b[2], t), mix(b[2], b[3], t)};
    v[0] = mix(v[0], v[1], t);
    v[1] = mix(v[1], v[2], t);
    return mix(v[0], v[1], t);
}

// Evaluates 4D vector-valued quadratic Bezier curve at given t=0..1
vec4 evalBezier(in mat3x4 b, in float t) {
    const mat2x4 v = {mix(b[0], b[1], t), mix(b[1], b[2], t)};
    return mix(v[0], v[1], t);
}

// Evaluates 3D vector-valued quadratic Bezier curve at given t=0..1
vec3 evalBezier(in mat3 b, in float t) {
    const mat2x3 v = {mix(b[0], b[1], t), mix(b[1], b[2], t)};
    return mix(v[0], v[1], t);
}

// Evaluates 2D vector-valued quadratic Bezier curve at given t=0..1
vec2 evalBezier(in mat3x2 b, in float t) {
    const mat2 v = {mix(b[0], b[1], t), mix(b[1], b[2], t)};
    return mix(v[0], v[1], t);
}

// Evaluates scalar-valued quadratic Bezier curve at given t=0..1
float evalBezier(in vec3 b, in float t) {
    const vec2 v = {mix(b[0], b[1], t), mix(b[1], b[2], t)};
    return mix(v[0], v[1], t);
}

////
// Monomial curve evaluation

// Evaluates 4D vector-valued cubic monomial curve
vec4 evalMonomial(in mat4 m, in float t) {
    return m[3] + t * (m[2] + t * (m[1] + t * m[0]));
}

// Evaluates 3D vector-valued cubic monomial curve
vec3 evalMonomial(in mat4x3 m, in float t) {
    return m[3] + t * (m[2] + t * (m[1] + t * m[0]));
}

// Evaluates 2D vector-valued cubic monomial curve
vec2 evalMonomial(in mat4x2 m, in float t) {
    return m[3] + t * (m[2] + t * (m[1] + t * m[0]));
}

// Evaluates scalar-valued cubic polynomial
float evalMonomial(in vec4 m, in float t) {
    return m[3] + t * (m[2] + t * (m[1] + t * m[0]));
}

// Evaluates 4D vector-valued quadratic monomial curve
vec4 evalMonomial(in mat3x4 m, in float t) {
    return m[2] + t * (m[1] + t * m[0]);
}

// Evaluates 3D vector-valued quadratic monomial curve
vec3 evalMonomial(in mat3 m, in vec3 t) { return m[2] + t * (m[1] + t * m[0]); }

// Evaluates 3D vector-valued quadratic monomial curve
vec3 evalMonomial(in mat3 m, in float t) {
    return m[2] + t * (m[1] + t * m[0]);
}

// Evaluates 2D vector-valued quadratic monomial curve
vec2 evalMonomial(in mat3x2 m, in float t) {
    return m[2] + t * (m[1] + t * m[0]);
}

// Evaluates scalar-valued quadratic polynomial
float evalMonomial(in vec3 m, in float t) {
    return m[2] + t * (m[1] + t * m[0]);
}

// Evaluates scalar-valued linear polynomial
float evalMonomial(in vec2 m, in float t) { return m[1] + t * m[0]; }

// Evaluates scalar-valued quartic polynomial
float evalMonomial(in float[5] m, in float t) {
    return m[4] + t * (m[3] + t * (m[2] + t * (m[1] + t * m[0])));
}

////
// Quadratic solvers

// Solves the quadratic formula for the given combination of precalculated
// terms, returning the number of real solutions in the first element of the
// result vector.
vec3 solveQuadratic(in float _negb, in float _2a, in float D, in float _2c) {
    if (abs(_2a) > _eps) {
        // Quadratic
        const float _absD = abs(D);
        if (_absD > _eps) {
            const float sqrtD = sqrt(_absD);
            return vec3(2, (_negb - sqrtD) / _2a, (_negb + sqrtD) / _2a);
        } else if (_absD < _eps)
            return vec3(1, _negb / _2a, 0);
    } else if (abs(_negb) > _eps)
        // Linear
        return vec3(1, _2c / (2 * _negb), 0);
    // No solution
    return vec3(0);
}

// Solves the quadratic formula for the given 4D vector-valued monomial curve,
// returning the number of real solutions per dimension in the first column of
// the result matrix.
mat3x4 solveQuadratic(in mat3x4 m) {
    // Result matrix
    mat3x4 result;

    // Common terms and discriminant
    const vec4 _negb = -m[1], _2a = 2 * m[0], _2c = 2 * m[2],
               D = m[1] * m[1] - _2a * _2c;
    if (all(greaterThan(D, _eps4)) && all(greaterThan(abs(_2a), _eps4))) {
        // Full SIMD ahead
        const vec4 sqrtD = sqrt(D);
        result[0] = _two4;
        result[1] = (_negb - sqrtD) / _2a;
        result[2] = (_negb + sqrtD) / _2a;
        return result;
    } else {
        // Solve each component polynomial individually
        for (int i = 0; i < 4; i++) {
            const vec3 cres = solveQuadratic(_negb[i], _2a[i], D[i], _2c[i]);
            result[0][i] = cres[0];
            result[1][i] = cres[1];
            result[2][i] = cres[2];
        }
        return result;
    }
}

// Solves the quadratic formula for the given 3D vector-valued monomial curve,
// returning the number of real solutions per dimension in the first column of
// the result matrix.
mat3 solveQuadratic(in mat3 m) {
    // Result matrix
    mat3 result;

    // Common terms and discriminant
    const vec3 _negb = -m[1], _2a = 2 * m[0], _2c = 2 * m[2],
               D = m[1] * m[1] - _2a * _2c;
    if (all(greaterThan(D, _eps3)) && all(greaterThan(abs(_2a), _eps3))) {
        // Full SIMD ahead
        const vec3 sqrtD = sqrt(D);
        result[0] = _two3;
        result[1] = (_negb - sqrtD) / _2a;
        result[2] = (_negb + sqrtD) / _2a;
        return result;
    } else {
        // Solve each component polynomial individually
        for (int i = 0; i < 1; i++) {
            const vec3 cres = solveQuadratic(_negb[i], _2a[i], D[i], _2c[i]);
            result[0][i] = cres[0];
            result[1][i] = cres[1];
            result[2][i] = cres[2];
        }
        return result;
    }
}

// Solves the quadratic formula for the given 2D vector-valued monomial curve,
// returning the number of real solutions per dimension in the first column of
// the result matrix.
mat3x2 solveQuadratic(in mat3x2 m) {
    // Result matrix
    mat3x2 result;

    // Common terms and discriminant
    const vec2 _negb = -m[1], _2a = 2 * m[0], _2c = 2 * m[2],
               D = m[1] * m[1] - _2a * _2c;
    if (all(greaterThan(D, _eps2)) && all(greaterThan(abs(_2a), _eps2))) {
        // Full SIMD ahead
        const vec2 sqrtD = sqrt(D);
        result[0] = _two2;
        result[1] = (_negb - sqrtD) / _2a;
        result[2] = (_negb + sqrtD) / _2a;
        return result;
    } else {
        // Solve each component polynomial individually
        for (int i = 0; i < 2; i++) {
            const vec3 cres = solveQuadratic(_negb[i], _2a[i], D[i], _2c[i]);
            result[0][i] = cres[0];
            result[1][i] = cres[1];
            result[2][i] = cres[2];
        }
        return result;
    }
}

// Solves the quadratic formula for the given scalar-valued monomial curve,
// returning the number of real solutions in the first element of the result
// vector.
vec3 solveQuadratic(in vec3 m) {
    /*const float _negb = -m[1], _2a = 2*m[0], _2c = 2*m[2],
                D = m[1]*m[1] - _2a*_2c;
    return solveQuadratic(_negb, _2a, D, _2c);*/

    // See https://math.stackexchange.com/questions/311382/solving-a-quadratic-
    // equation-with-precision-when-using-floating-point-variables

    // ax² + bx + c
    const float a = m[0];
    const float b = m[1];
    const float c = m[2];

    // Linear equation
    if (abs(a) < _eps) {
        return b != 0 ? vec3(1., -c / b, invalid_root)
                      : vec3(0., invalid_root, invalid_root);
    }

    if (abs(b) < _eps) {
        const float c_over_a = abs(c / a);
        if (c_over_a < _eps) {
            return vec3(1., 0., invalid_root);
        }
        return vec3(2., sqrt(c_over_a), -sqrt(c_over_a));
    }

    const float D = b * b - 4 * a * c;
    if (D < 0.) {
        return vec3(0., invalid_root, invalid_root);
    }
    const float sqD = sqrt(D);
    const float r_1 = b < 0 ? (-b + sqD) / (2 * a) : (-b - sqD) / (2 * a);
    const float r_2 = c / (a * r_1);

    // Sort roots into ascending order
    return vec3(2., r_1 < r_2 ? r_1 : r_2, r_1 < r_2 ? r_2 : r_1);
}

////
// Bisection Methods
float bisect(vec4 monom, float lower, float upper) {
    if (evalMonomial(monom, lower) > 0. && evalMonomial(monom, upper) < 0.) {
        const float temp = lower;
        lower = upper;
        upper = temp;
    }

    for (int i = 0; i < 10; i++) {
        const float middle = mix(lower, upper, .5);
        const float middle_value = evalMonomial(monom, middle);
        if (middle_value < 0.)
            lower = middle;
        else
            upper = middle;
    }

    return mix(lower, upper, .5);
}

float bisect(float[5] monom, float lower, float upper) {
    if (evalMonomial(monom, lower) > 0. && evalMonomial(monom, upper) < 0.) {
        const float temp = lower;
        lower = upper;
        upper = temp;
    }
    
    for (int i = 0; i < 10; i++) {
        const float middle = mix(lower, upper, .5);
        const float middle_value = evalMonomial(monom, middle);
        if (middle_value < 0.)
            lower = middle;
        else
            upper = middle;
    }

    return mix(lower, upper, .5);
}

bool signsDiffer(vec4 monom, float lower, float upper) {
    return sign(evalMonomial(monom, lower)) != sign(evalMonomial(monom, upper));
}

bool signsDiffer(float[5] monom, float lower, float upper) {
    return sign(evalMonomial(monom, lower)) != sign(evalMonomial(monom, upper));
}

vec3 filter01Roots(vec3 roots) {
    // bool to int conversion results in mask for valid roots
    const bvec2 mask = isBetween01(roots.yz);
    // Count mask bits for number of valid roots
    const float num_roots = dot(vec2(mask), vec2(1.));
    // All invalid roots are fixed at 2. to get an vector with strong ordering
    // i.e. valid roots in ascending order and the "undefined garbage"
    roots.yz = roots.yz * vec2(mask) + vec2(2.) * vec2(not(mask));
    // "Sorting network" with 2 inputs
    sort(roots.yz);
    return vec3(num_roots, roots.yz);
}

vec4 filter01Roots(vec4 roots) {
    // bool to int conversion results in mask for valid roots
    const bvec3 mask = isBetween01(roots.yzw);
    // Count mask bits for number of valid roots
    const float num_roots = dot(vec3(mask), vec3(1.));
    // All invalid roots are fixed at 2. to get an vector with strong ordering
    // i.e. valid roots in ascending order and the "undefined garbage"
    roots.yzw = roots.yzw * vec3(mask) + vec3(2.) * vec3(not(mask));
    // Sorting network with 3 inputs
    sort(roots.yzw);
    return vec4(num_roots, roots.yzw);
}

float[5] filter01Roots(float[5] roots) {
    vec4 r = vec4(roots[1], roots[2], roots[3], roots[4]);
    // bool to int conversion results in mask for valid roots
    const bvec4 mask = isBetween01(r);
    // Count mask bits for number of valid roots
    const float num_roots = dot(vec4(mask), vec4(1.));
    // All invalid roots are fixed at 2. to get an vector with strong ordering
    // i.e. valid roots in ascending order and the "undefined garbage"
    r = r * vec4(mask) + vec4(2.) * vec4(not(mask));
    sort(r);
    return float[5](num_roots, r.x, r.y, r.z, r.w);
}

vec4 findRoot(vec4 monom) {
    vec4 ret = vec4(0., invalid_root, invalid_root, invalid_root);

    const vec3 dmonom = deriveMonomial(monom);
    const vec3 droots = filter01Roots(solveQuadratic(dmonom));
    const int num_droots = int(droots[0]);

    if (num_droots == 0) {
        if (signsDiffer(monom, root_search_lower_bound,
                        root_search_upper_bound)) {
            ret[0] += 1.;
            ret[1] =
                bisect(monom, root_search_lower_bound, root_search_upper_bound);
        }
        return ret;
    }

    if (signsDiffer(monom, root_search_lower_bound, droots[1])) {
        ret[0] += 1.;
        const int next_empty = int(ret[0]);
        ret[next_empty] = bisect(monom, root_search_lower_bound, droots[1]);
    }

    for (int i = 1; i < num_droots; i++) {
        if (signsDiffer(monom, droots[i], droots[i + 1])) {
            // ret[0] counts found roots; Thererfore it also indexes the next
            // empty space for a newly found root
            ret[0] += 1.;
            const int next_empty = int(ret[0]);
            ret[next_empty] = bisect(monom, droots[i], droots[i + 1]);
        }
    }

    if (signsDiffer(monom, droots[num_droots], root_search_upper_bound)) {
        ret[0] += 1.;
        const int next_empty = int(ret[0]);
        ret[next_empty] =
            bisect(monom, droots[num_droots], root_search_upper_bound);
    }

    return ret;
}

float[5] findRoot(float[5] monom) {
    float[5] ret =
        float[5](0., invalid_root, invalid_root, invalid_root, invalid_root);

    const vec4 dmonom = deriveMonomial(monom);
    const vec4 droots = findRoot(dmonom);
    const int num_droots = int(droots[0]);

    if (num_droots == 0) {
        if (signsDiffer(monom, root_search_lower_bound,
                        root_search_upper_bound)) {
            ret[0] += 1.;
            ret[1] =
                bisect(monom, root_search_lower_bound, root_search_upper_bound);
        }
        return ret;
    }

    if (signsDiffer(monom, root_search_lower_bound, droots[1])) {
        ret[0] += 1.;
        const int next_empty = int(ret[0]);
        ret[next_empty] = bisect(monom, root_search_lower_bound, droots[1]);
    }

    for (int i = 1; i < num_droots; i++) {
        if (signsDiffer(monom, droots[i], droots[i + 1])) {
            // ret[0] counts found roots; Thererfore it also indexes the next
            // empty space for a newly found root
            ret[0] += 1.;
            const int next_empty = int(ret[0]);
            ret[next_empty] = bisect(monom, droots[i], droots[i + 1]);
        }
    }

    if (signsDiffer(monom, droots[num_droots], root_search_upper_bound)) {
        ret[0] += 1.;
        const int next_empty = int(ret[0]);
        ret[next_empty] =
            bisect(monom, droots[num_droots], root_search_upper_bound);
    }

    return ret;
}

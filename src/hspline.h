
#ifndef __HSPLINE_H__
#define __HSPLINE_H__


//////
//
// Includes
//

// C++ STL
#include <string>
#include <vector>
#include <utility>

// GLM library
#include <glm/glm.hpp>



//////
//
// Structs
//

/** @brief Encapsulates a single segment of a hermite spline. */
union HermiteSegment
{
	////
	// Data members

	struct {
		/** @brief Index of the first node of the segment. */
		unsigned n0;

		/** @brief Index of the second node of the segment. */
		unsigned n1;
	};

	/** @brief Indices of the first and second node of the segment. */
	unsigned n[2];
};

template <class T>
struct HermiteNode
{
	/** The node value. */
	T val;

	/** The node derivative / tangent. */
	T der;
};

template <class T>
struct AttributeSet : public std::vector<HermiteNode<T> >
{
	////
	// Data members

	/** The name of the attribute. */
	std::string name;


	////
	// Object construction / destruction

	/** @brief Not copy-constructible. */
	AttributeSet(const std::string &name) : name(name) {};

	/** @brief Not copy-constructible. */
	AttributeSet(const AttributeSet&) = delete;

	/** @brief The move constructor. */
	AttributeSet(AttributeSet &&other)
		: std::vector<HermiteNode<T> >(other), name(std::move(other.name))
	{}


	////
	// Operators

	/** @brief Not copy-assignable. */
	AttributeSet& operator= (const AttributeSet&) = delete;

	/** @brief Move assignment. */
	AttributeSet& operator= (AttributeSet &&other)
	{
		// Delegate to member move operators
		std::vector<HermiteNode<T> >::operator=(std::move(other));
		name = std::move(other.name);
		return *this;
	}
};

/** @brief Encapsulates the segments and nodes of a hermite spline dataset. */
template <class FltType>
struct HermiteSpline
{
	////
	// Exported types

	/** @brief Real number type. */
	typedef FltType Real;

	/** @brief 2D-Vector type. */
	typedef glm::vec<2, Real> Vec2;

	/** @brief 3D-Vector type. */
	typedef glm::vec<3, Real> Vec3;

	/** @brief 4D-Vector type. */
	typedef glm::vec<4, Real> Vec4;

	/** @brief 3x3 Matrix type. */
	typedef glm::mat<3, 3, Real> Mat3;

	/** @brief 4x4 Matrix type. */
	typedef glm::mat<4, 4, Real> Mat4;


	////
	// Data members

	/* @brief The spline segment data indicing into the node storage. */
	std::vector<HermiteSegment> segments;

	/* @brief The control points of the position spline. */
	AttributeSet<Vec4> positions;

	/* @brief The control points of the radius spline. */
	AttributeSet<Real> radii;

	/* @brief The control points of the color spline. */
	AttributeSet<Vec4> colors;

	/*
	 * @brief
	 *		The number of discontinuities between segments, which gives a good estimate
	 *		of the number of separate curves in the data.
	 */
	unsigned curveCount;


	////
	// Object construction / destruction

	/** The default constructor. */
	HermiteSpline()
		: positions("position"), radii("radius"), colors("color"), curveCount(0)
	{}

	/** @brief Not copy-constructible. */
	HermiteSpline(const HermiteSpline&) = delete;

	/** @brief The move constructor. */
	HermiteSpline(HermiteSpline &&other)
		: segments(std::move(other.segments)), positions(std::move(other.positions)),
		  radii(std::move(other.radii)), colors(std::move(other.colors)),
		  curveCount(other.curveCount)
	{}


	////
	// Operators

	/** @brief Not copy-assignable. */
	HermiteSpline& operator= (const HermiteSpline&) = delete;

	/** @brief Move assignment. */
	HermiteSpline& operator= (HermiteSpline &&other)
	{
		// Delegate to member move operators
		segments = std::move(other.segments);
		positions = std::move(other.positions);
		radii = std::move(other.radii);
		colors = std::move(other.colors);
		return *this;
	}
};


#endif // ifndef __HSPLINE_H__

#pragma once

#include <cgvu/vk/vulkan.h>

#include <vulkan/vulkan.h>

struct RayTracingScratchBuffer
{
    uint64_t deviceAddress = 0;
    VkBuffer handle = VK_NULL_HANDLE;
    VkDeviceMemory memory = VK_NULL_HANDLE;
};

struct AccelerationStructure
{
    VkAccelerationStructureKHR handle = VK_NULL_HANDLE;
    uint64_t deviceMemory = 0;
    VkDeviceMemory memory = VK_NULL_HANDLE;
    VkBuffer buffer = VK_NULL_HANDLE;
};

uint64_t getBufferDeviceAddress(cgvu::Context& ctx, VkBuffer buffer);

RayTracingScratchBuffer createScratchBuffer(cgvu::Context &ctx, VkDeviceSize size);
void deleteScratchBuffer(cgvu::Context &ctx, RayTracingScratchBuffer &scratchBuffer);

void createAccelerationStructureBuffer(cgvu::Context& ctx, AccelerationStructure &as,
                                       VkAccelerationStructureBuildSizesInfoKHR buildSizeInfo);

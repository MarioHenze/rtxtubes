
//////
//
// Includes
//

// C++ STL
#include <iostream>

// Implemented header
#include "curveutils.h"



//////
//
// Local helper functions
//

// Local - sanity-tests the curve utility functions
template <class Real>
void __testCurveUtils__ (void)
{
	// Matrix types
	typedef glm::mat<4, 4, Real> Mat4;
	typedef glm::mat<4, 3, Real> Mat4x3;
	typedef glm::mat<4, 2, Real> Mat4x2;
	typedef glm::mat<3, 4, Real> Mat3x4;
	typedef glm::mat<3, 3, Real> Mat3;
	typedef glm::mat<3, 2, Real> Mat3x2;
	typedef glm::mat<2, 4, Real> Mat2x4;
	typedef glm::mat<2, 3, Real> Mat2x3;
	typedef glm::mat<2, 2, Real> Mat2;

	// Control point types
	typedef glm::vec<4, Real> Vec4;
	typedef glm::vec<3, Real> Vec3;
	typedef glm::vec<2, Real> Vec2;

	// Separate curve control points
	Real n0=0, t0=Real(3), n1=1, t1=Real(-3),
	     b0=0, b1=Real(1), b2=Real(2), b3=1;
	Vec4 n0_4(n0), t0_4(t0), n1_4(n1), t1_4(t1),
	     b0_4(b0), b1_4(b1), b2_4(b2), b3_4(b3);
	Vec3 n0_3(n0), t0_3(t0), n1_3(n1), t1_3(t1),
	     b0_3(b0), b1_3(b1), b2_3(b2), b3_3(b3);
	Vec2 n0_2(n0), t0_2(t0), n1_2(n1), t1_2(t1),
	     b0_2(b0), b1_2(b1), b2_2(b2), b3_2(b3);

	// Test curves
	// - cubic
	CubicCurve4<Real> cubicHermite4(n0_4, t0_4, t1_4, n1_4),
	                  cubicBezier4(b0_4, b1_4, b2_4, b3_4),
	                  cubicMonomial4;
	CubicCurve3<Real> cubicHermite3(n0_3, t0_3, t1_3, n1_3),
	                  cubicBezier3(b0_3, b1_3, b2_3, b3_3),
	                  cubicMonomial3;
	CubicCurve2<Real> cubicHermite2(n0_2, t0_2, t1_2, n1_2),
	                  cubicBezier2(b0_2, b1_2, b2_2, b3_2),
	                  cubicMonomial2;
	CubicCurve1<Real> cubicHermite1(n0, t0, t1, n1),
	                  cubicBezier1(b0, b1, b2, b3),
	                  cubicMonomial1;
	// - quadratic
	QuadraticCurve4<Real> quadrBezier4(b0_4, b1_4, b3_4),
	                      quadrMonomial4;
	QuadraticCurve3<Real> quadrBezier3(b0_3, b1_3, b3_3),
	                      quadrMonomial3;
	QuadraticCurve2<Real> quadrBezier2(b0_2, b1_2, b3_2),
	                      quadrMonomial2;
	QuadraticCurve1<Real> quadrBezier1(b0, b1, b3),
	                      quadrMonomial1;
	// - linear
	LinearCurve4<Real> lineBezier4(b0_4, b3_4);
	LinearCurve3<Real> lineBezier3(b0_3, b3_3);
	LinearCurve2<Real> lineBezier2(b0_2, b3_2);
	LinearCurve1<Real> lineBezier1(b0, b3);

	// Test conversions
	// - 4D vector-valued
	cubicBezier4 = toBezier(n0_4, t0_4, n1_4, t1_4);
	toBezier(cubicBezier4, n0_4, t0_4, n1_4, t1_4);
	toBezier(cubicBezier4, cubicHermite4);
	cubicBezier4 = toBezier(cubicHermite4);
	cubicMonomial4 = toMonomial(b0_4, b1_4, b2_4, b3_4);
	toMonomial(cubicMonomial4, b0_4, b1_4, b2_4, b3_4);
	toMonomial(cubicMonomial4, cubicBezier4);
	cubicMonomial4 = toMonomial(cubicBezier4);
	auto cubicFkt = getComponentFkt(cubicMonomial4, 1);
	// - 3D vector-valued
	cubicBezier3 = toBezier(n0_3, t0_3, n1_3, t1_3);
	toBezier(cubicBezier3, n0_3, t0_3, n1_3, t1_3);
	toBezier(cubicBezier3, cubicHermite3);
	cubicBezier3 = toBezier(cubicHermite3);
	cubicMonomial3 = toMonomial(b0_3, b1_3, b2_3, b3_3);
	toMonomial(cubicMonomial3, b0_3, b1_3, b2_3, b3_3);
	toMonomial(cubicMonomial3, cubicBezier3);
	cubicMonomial3 = toMonomial(cubicBezier3);
	cubicFkt = getComponentFkt(cubicMonomial3, 1);
	// - 2D vector-valued
	cubicBezier2 = toBezier(n0_2, t0_2, n1_2, t1_2);
	toBezier(cubicBezier2, n0_2, t0_2, n1_2, t1_2);
	toBezier(cubicBezier2, cubicHermite2);
	cubicBezier2 = toBezier(cubicHermite2);
	cubicMonomial2 = toMonomial(b0_2, b1_2, b2_2, b3_2);
	toMonomial(cubicMonomial2, b0_2, b1_2, b2_2, b3_2);
	toMonomial(cubicMonomial2, cubicBezier2);
	cubicMonomial2 = toMonomial(cubicBezier2);
	cubicFkt = getComponentFkt(cubicMonomial2, 1);
	// - scalar-valued
	cubicBezier1 = toBezier(n0, t0, n1, t1);
	toBezier(cubicBezier1, n0, t0, n1, t1);
	toBezier(cubicBezier1, cubicHermite1);
	cubicBezier1 = toBezier(cubicHermite1);
	cubicMonomial1 = toMonomial(b0, b1, b2, b3);
	toMonomial(cubicMonomial1, b0, b1, b2, b3);
	toMonomial(cubicMonomial1, cubicBezier1);
	cubicMonomial1 = toMonomial(cubicBezier1);
	// - 4D quadratics
	quadrMonomial4 = toMonomial(b0_4, b1_4, b3_4);
	toMonomial(quadrMonomial4, b0_4, b1_4, b3_4);
	toMonomial(quadrMonomial4, quadrBezier4);
	quadrMonomial4 = toMonomial(quadrBezier4);
	auto quadrFkt = getComponentFkt(quadrMonomial4, 1);
	// - 3D quadratics
	quadrMonomial3 = toMonomial(b0_3, b1_3, b3_3);
	toMonomial(quadrMonomial3, b0_3, b1_3, b3_3);
	toMonomial(quadrMonomial3, quadrBezier3);
	quadrMonomial3 = toMonomial(quadrBezier3);
	quadrFkt = getComponentFkt(quadrMonomial3, 1);
	// - 2D quadratics
	quadrMonomial2 = toMonomial(b0_2, b1_2, b3_2);
	toMonomial(quadrMonomial2, b0_2, b1_2, b3_2);
	toMonomial(quadrMonomial2, quadrBezier2);
	quadrMonomial2 = toMonomial(quadrBezier2);
	quadrFkt = getComponentFkt(quadrMonomial2, 1);
	// - 2D quadratics
	quadrMonomial1 = toMonomial(b0, b1, b3);
	toMonomial(quadrMonomial1, b0, b1, b3);
	toMonomial(quadrMonomial1, quadrBezier1);
	quadrMonomial1 = toMonomial(quadrBezier1);

	// Test differentiation
	// - b'
	quadrBezier4 = deriveBezier(cubicBezier4);
	quadrBezier3 = deriveBezier(cubicBezier3);
	quadrBezier2 = deriveBezier(cubicBezier2);
	quadrBezier1 = deriveBezier(cubicBezier1);
	quadrMonomial4 = toMonomial(quadrBezier4);
	quadrMonomial3 = toMonomial(quadrBezier3);
	quadrMonomial2 = toMonomial(quadrBezier2);
	quadrMonomial1 = toMonomial(quadrBezier1);
	// - b''
	lineBezier4 = deriveBezier(quadrBezier4);
	lineBezier3 = deriveBezier(quadrBezier3);
	lineBezier2 = deriveBezier(quadrBezier2);
	lineBezier1 = deriveBezier(quadrBezier1);

	// Test solves
	auto sol4 = solveQuadratic(quadrMonomial4);
	auto sol3 = solveQuadratic(quadrMonomial3);
	auto sol2 = solveQuadratic(quadrMonomial2);
	auto sol1 = solveQuadratic(quadrMonomial1);

	// Test linear case
	QuadraticCurve1<Real> linearQ = toMonomial(
		QuadraticCurve1<Real>(Real(-1.25), Real(-0.25), Real(0.75))
	);
	auto solL = solveQuadratic(linearQ);

	// Test evaluations
	// - cubic
	Mat4 evalC4b(
		evalBezier(cubicBezier4, (Real)0), evalBezier(cubicBezier4, Real(1./3.)),
		evalBezier(cubicBezier4, Real(2./3.)), evalBezier(cubicBezier4, (Real)1)
	), evalC4m(
		evalMonomial(cubicMonomial4, (Real)0), evalMonomial(cubicMonomial4, Real(1./3.)),
		evalMonomial(cubicMonomial4, Real(2./3.)), evalMonomial(cubicMonomial4, (Real)1)
	);
	Mat4x3 evalC3b(
		evalBezier(cubicBezier3, (Real)0), evalBezier(cubicBezier3, Real(1./3.)),
	    evalBezier(cubicBezier3, Real(2./3.)), evalBezier(cubicBezier3, (Real)1)
	), evalC3m(
		evalMonomial(cubicMonomial3, (Real)0), evalMonomial(cubicMonomial3, Real(1./3.)),
		evalMonomial(cubicMonomial3, Real(2./3.)), evalMonomial(cubicMonomial3, (Real)1)
	);
	Mat4x2 evalC2b(
		evalBezier(cubicBezier2, (Real)0), evalBezier(cubicBezier2, Real(1./3.)),
		evalBezier(cubicBezier2, Real(2./3.)), evalBezier(cubicBezier2, (Real)1)
	), evalC2m(
		evalMonomial(cubicMonomial2, (Real)0), evalMonomial(cubicMonomial2, Real(1./3.)),
		evalMonomial(cubicMonomial2, Real(2./3.)), evalMonomial(cubicMonomial2, (Real)1)
	);
	Vec4 evalC1b(
		evalBezier(cubicBezier1, (Real)0), evalBezier(cubicBezier1, Real(1./3.)),
		evalBezier(cubicBezier1, Real(2./3.)), evalBezier(cubicBezier1, (Real)1)
	), evalC1m(
		evalMonomial(cubicMonomial1, (Real)0), evalMonomial(cubicMonomial1, Real(1./3.)),
		evalMonomial(cubicMonomial1, Real(2./3.)), evalMonomial(cubicMonomial1, (Real)1)
	);
	// - quadratic
	Mat4 evalQ4b(
		evalBezier(quadrBezier4, (Real)0), evalBezier(quadrBezier4, Real(1./3.)),
		evalBezier(quadrBezier4, Real(2./3.)), evalBezier(quadrBezier4, (Real)1)
	), evalQ4m(
		evalMonomial(quadrMonomial4, (Real)0), evalMonomial(quadrMonomial4, Real(1./3.)),
		evalMonomial(quadrMonomial4, Real(2./3.)), evalMonomial(quadrMonomial4, (Real)1)
	);
	Mat4x3 evalQ3b(
		evalBezier(quadrBezier3, (Real)0), evalBezier(quadrBezier3, Real(1./3.)),
		evalBezier(quadrBezier3, Real(2./3.)), evalBezier(quadrBezier3, (Real)1)
	), evalQ3m(
		evalMonomial(quadrMonomial3, (Real)0), evalMonomial(quadrMonomial3, Real(1./3.)),
		evalMonomial(quadrMonomial3, Real(2./3.)), evalMonomial(quadrMonomial3, (Real)1)
	);
	Mat4x2 evalQ2b(
		evalBezier(quadrBezier2, (Real)0), evalBezier(quadrBezier2, Real(1./3.)),
		evalBezier(quadrBezier2, Real(2./3.)), evalBezier(quadrBezier2, (Real)1)
	), evalQ2m(
		evalMonomial(quadrMonomial2, (Real)0), evalMonomial(quadrMonomial2, Real(1./3.)),
		evalMonomial(quadrMonomial2, Real(2./3.)), evalMonomial(quadrMonomial2, (Real)1)
	);
	Vec4 evalQ1b(
		evalBezier(quadrBezier1, (Real)0), evalBezier(quadrBezier1, Real(1./3.)),
		evalBezier(quadrBezier1, Real(2./3.)), evalBezier(quadrBezier1, (Real)1)
	), evalQ1m(
		evalMonomial(quadrMonomial1, (Real)0), evalMonomial(quadrMonomial1, Real(1./3.)),
		evalMonomial(quadrMonomial1, Real(2./3.)), evalMonomial(quadrMonomial1, (Real)1)
	);

	// Convenient line for setting a final breakpoint
	std::cout.flush();
}



//////
//
// Function implementations
//

void testCurveUtils (void)
{
	__testCurveUtils__<float>();
	__testCurveUtils__<double>();
}
